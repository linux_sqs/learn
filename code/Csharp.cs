using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp1
{
    class expr
    {
        public int expr_val=90;
        public int expr_v
        {
            set
            {
                expr_val = value;
            }
            get
            {
                return expr_val;
            }
        }
        public expr(int asd)
        {
            expr_val = asd;
        }
        static void Main(string[] args)
        {
            Console.Out.WriteLine("主函数被调用");
            Console.WriteLine("当前时间： {0}",System.DateTime.Now.ToString("dd 日 hh:mm:sss"));

            expr asd_sen = new expr('2');
            Console.WriteLine(asd_sen.expr_val);
            compute_pi();
            csharp_fuxi();
            Console.ReadKey();
        }
        static void compute_pi()
        {
            //计算pi ：莱布尼茨方法
            bool is_plus = true;
            long jingdu = 1000000; //定义精度
            double pi_of_me = 0.0;
            long jump_two=1;
            for (int i = 1; i<jingdu;i++)
            {
                if(is_plus==true)
                {
                    pi_of_me += 4.0 / jump_two;
                }
                else
                {
                    pi_of_me -= 4.0 / jump_two;//4.0
                }
                is_plus = !is_plus;
                jump_two+=2;
            }
            Console.WriteLine(pi_of_me);
            //计算pi: Nilakantha 级数
            long start = 3;
            is_plus = true;
            pi_of_me=3;
            for (int i = 0; i < jingdu;i++ )
            {
                if(is_plus==true)
                {
                    pi_of_me+=4.0/(start*(start-1)*(start+1));
                }
                else
                {
                    pi_of_me-=4.0/(start*(start-1)*(start+1));
                }
                is_plus = !is_plus;
                start += 2;
            }
            Console.WriteLine(pi_of_me);
        }
        static void csharp_fuxi()
        {
            //C#考前复习
            decimal asd = 123234;
            Console.WriteLine(asd);
        }
        
    }
}
//闰年：1、能被4且不能被100整除。2、能被400整除。
/*C#程序开发参考手册 王小科
 * 相关内容在.net文档中都能查到 索引中输入关键字即可
 * 关键字：abstract 、base as is throw
 * 类：diapose convert console math random regex
 * namespace(搜 命名空间) readline typeof goto  split trim
 * static：编译时分派内存，文件内部可见
*/
static void compute_pi()
        {
            //计算pi ：莱布尼茨方法
            bool is_plus = true;
            long jingdu = 1000000; //定义精度
            double pi_of_me = 0.0;
            long jump_two=1;
            for (int i = 1; i<jingdu;i++)
            {
                if(is_plus==true)
                {
                    pi_of_me += 4.0 / jump_two;
                }
                else
                {
                    pi_of_me -= 4.0 / jump_two;//4.0
                }
                is_plus = !is_plus;
                jump_two+=2;
            }
            Console.WriteLine(pi_of_me);
            //计算pi: Nilakantha 级数
            long start = 3;
            is_plus = true;
            pi_of_me=3;
            for (int i = 0; i < jingdu;i++ )
            {
                if(is_plus==true)
                {
                    pi_of_me+=4.0/(start*(start-1)*(start+1));
                }
                else
                {
                    pi_of_me-=4.0/(start*(start-1)*(start+1));
                }
                is_plus = !is_plus;
                start += 2;
            }
            Console.WriteLine(pi_of_me);
        }
        
        
        
        
        
        
        
        
               
namespace csharp2
{
    public interface booklibel
    {
        void showlibel();//声明方法，实现接口的类中定义
    }
    public class book
    {
        private string bookname;//private不能直接由对象访问
        public int nummber;     //public可以
        protected float prices;       

        public book()
        {
            Console.WriteLine("调用默认构造函数");
        }
        ~book()
        {
            Console.WriteLine("析构函数被调用");
            Console.ReadKey();
        }
        public book(string name="no_name",int num=0,float jia=0)
        {
            bookname=name;
            nummber=num;
            prices=jia;
            Console.WriteLine("调用带参构造函数");
        }
        public float setprices(float pri)
        {
            prices = pri;
            return prices;
        }
    }

    class mathbook:book //C#不支持多重继承，接口代替
    {
        UInt16 pagenum;
    }


    class Program   //类的默认为internal，成员默认访问权限为private
    {
        enum days
        {
            sun, mon, tue, wed, thu, fri, sat
        };
        static void Main(string[] args)
        {
            Console.Out.WriteLine("hello world");
            int firstday = (int)days.fri;
            Console.WriteLine("monday is{0}", firstday);

            string name = "瓦尔登湖";
            float fudian = (float)0.23;
            book qt5 = new book();
            book qt4 = new book(null,123,fudian);

            //throw (new ApplicationException("Zero Temperature found"));

            Console.ReadKey();
        }
    }
}
/*多态性：静态多态性：编译时：函数重载，运算符重载
        动态多态性：运行时：abstract抽象类（虚方法virtual）
 * 封闭类：sealed 修饰的类
 * 预处理指令：#line #error #warning #region #if #else #define #elif #endif
            #endregion
 * 异常处理：try catch finally throw
            system.*Exception
 */