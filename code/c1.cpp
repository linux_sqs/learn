/*
向量的使用方式

*/
#include "stdafx.h"	//为什么要写这个文件？???还得在第一行？（标记文件默认包含）
#include "vec.h"

void use_vec()
{

	//vector 向量的使用
	/*三种初始化方法：
		1、name（） 3（一个）
		2、name（长度，默认值）几个3
		3、name（同类型引用）
		4、name（iter_first,iter_end）一段
	*/
	vector<int> asd(20, 1), v2;
	v2 = asd;	//copy
	vector<int>::iterator vec_iter1 = v2.begin(),vec_iter2=v2.end();//() not lack
	v2[7] = 8;  //赋值
	vector<int> v3(vec_iter1,vec_iter2);	//初始化
	cout <<"v3[10]为："<< v3.at(10) <<endl;	//at ==[]
	cout<<"2的首"<<v2.back() <<"尾 "<<v2.front()<<"大小"<<v2.size()<<endl;
	cout<<"v2的大小："<<v2.capacity()<<endl;
	v3.clear();
	v3 = v2;
	v3.erase(v3.begin());//删除某个元素
	v3.insert(v3.begin(),2);
	cout << v3.max_size() << "是v3的最大长度。" << endl;
	v3._Pop_back_n(2);
	v3.pop_back();
	v3.push_back(4);
	v3.swap(v2);
//判断非空
	if (v3.empty())
	{
		cout << "v3 is empty" << endl;
	}
	else
	{
		cout << "v3 is not empty" << endl;
	}
//2.判断两个向量是否相同
	if (v2 == asd)
	{
		cout << "两个向量相同" << endl;
		//长度相同，对应位置的元素也相同。
	}
	if (v2 != asd)
	{
		cout << "两个向量不同" << endl;
	}
	cout << endl;
//1.show the content of vector
	unsigned int i = 0;
	while (i < asd.size())
	{
		cout << asd[i++] << " ";
	}
	cout << endl;
	for (i = 0; i < v2.size(); i++)
	{
		cout << v2[i++] << " ";
	}
	cout << endl;
	i = 0;
	do
	{
		cout << v3.at(i++) << " ";
	} 
	while (i<v3.size());
}

/*   
v2 是咋了？？？

*/



// stdafx.cpp : 只包括标准包含文件的源文件
// rongqi_cpp.pch 将作为预编译头
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"

// TODO:  在 STDAFX.H 中
// 引用任何所需的附加头文件，而不是在此文件中引用
/*
链表的使用
	17-3-20 晚
*/
#include <list>

void use_list()
{
	list<int> li;
	li.assign(10,23);  //赋值
	cout << "尾元素为：" << li.back()<<endl;
	//li.clear();   //清空列表
	li.front() = 17;  //首项值
	li.back() = 17;
	cout << "列表最大为：" << li.max_size()<<endl;
	cout << "列表大小：" << li.size() << endl;

	list<int> la(20, 7);
	//li.swap(la);  //交换
	//li.merge(la);
	//li.pop_back(); //尾部弹出
	//li.pop_front();
	li.push_front(8); //头插
	li.push_back(8);   //尾插

	//li.erase(li.begin());
	//li.insert(li.begin(),3,2);//begin返回迭代器
	//li.remove(23);  //按值删除
	li.resize(20, 12);
	li.reverse();  //倒转所有元素
	li.splice(--li.end(),la);
//迭代器的输出使用
	int i = 1;
	//li.unique();  //去重
	//li.sort();  //升序
	list<int>::iterator li_iter;
	for (li_iter = li.begin(); li_iter != li.end(); li_iter++,i++)
	{
		//li.rend();????
		//li.remove_if(*li_iter==8);?????
		//*li_iter = i;
		cout << ++*li_iter << " ";	
	}
//判断null
	cout << endl;
	if (li.empty())
	{
		cout << "该列表为空" << endl;
	}
	else
	{
		cout << "该列表不为空" << endl;
	}

//stop the terminal
	int key;
	cin >> key;
}




//extern void use_vec();//即使你包含了他的头文件，在调用前也得声明。shit

/*
visual C++ 的源文件调用、管理方式：

		1、包含的头文件和函数声明放在.h文件中
		2、函数的具体定义放在.cpp文件中。
		3、.h文件用来实现项目上层管理（软件工程架构）（接口）
		4、.cpp文件用来实现项目下层操作（函数实现）（接口实现）
		5、联系到主文件时：只包含头文件即可
		6、stdafx.h 和stdafx.cpp 、和projectname.cpp为例。
*/

//几种C结构辨析

//enum_switch();
void enum_switch()
{
	//枚举结构的使用
	enum food{
		apple = 0,pine,tea,water,fruit
	};
	food cc= water;
	//switch结构的使用
	switch (cc)
	{
	case apple:
		std::cout << "apple" << std::endl;
		break;
	case pine:
		std::cout << "pine" << std::endl;
		break;
	default:
		std::cout << "no food." << std::endl;
	}
	//结构体的使用   1
	typedef struct body{
		char name[10];
		float  weight;
		double high;
		int age;
	}person;//此处为类型
	person jonson;
	strcpy_s(jonson.name,"qiongsen");
	jonson.weight = 0.8;
	jonson.high = 185;
	jonson.age = 25;
	std::cout << jonson.name << '-' << jonson.age << std::endl;
	//结构体的使用   2
	struct body1{
		char name[10];
		float  weight;
		double high;
		int age;
	}person1;//此处为对象
	person1.age = 25;
	strcpy_s(person1.name, "qiongsen");
	std::cout << person1.name << '-' << person1.age << std::endl;
	//联合体的使用
	union book
	{
		char* CPP;
		char* java;
		char* Csharp;
		char* C;
	};
	//单双指针和字符指针字符变量的区别。
	char* app = "w";
	//char  app1 = "w";  const char* 不能转换为 char
	int i = (1, 2);//,运算结果因编译器而不同，此时为2
	std::cout << "i= (1, 2) 的值为" << i << std::endl;
	//nullptr
	int* cpp = nullptr;
	std::cout <<"nullptr is:"<< cpp;
}

//C和C++的字符串存储验证

//str_char();
void str_char()
{
	using namespace std;
	char asd[] = "java";
	char asd1[] = "java";
	string str = "cpp";
	string str1 = "cpp";
	char* chr = "python";
	char* chr1 = "python";
	const char* ch = "golang";
	const char* ch1 = "golang";

	cout << (asd == asd1) << " asd == asd1" << "\t" << &asd << " " << &asd1 << " " << asd << " " << asd1 << endl;
	cout << (str == str1) << " str == str1" << "\t" << &str << " " << &str1 << " " << str << " " << str1 << endl;
	cout << (chr == chr1) << " chr == chr1" << "\t" << &chr << " " << &chr1 << " " << chr << " " << chr1 << endl;
	cout << (ch == ch1) << " ch == ch1" << "\t" << &ch << ' ' << &ch1 << " " << ch << " " << ch1 << endl;
	//只有数组名字比较的是地址 其他的都是内容
	//所有的存储地址都不相同
}

int _tmain(int argc, _TCHAR* argv[])
{
//list的使用
	use_list();
//vector 的使用
	//use_vec();
//wait a input
	int key = 0;
	cin >> key;
	return 0;
}
