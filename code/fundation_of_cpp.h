/*
2017-4-18  qs night 
	lib
*/
#include<iostream>
#include<fstream>
#include<string>
using namespace std;
void readfile()
{
	//ifstream tmpfile("foursign.txt");//不允许时使用不完整的文件类型 未包含fstream 文件
	fstream tmpfile2;
	tmpfile2.open("foursign.txt",ios_base::in);
	//wchar_t* str=new wchar_t[30]();
	if (tmpfile2.is_open()){
		/*tmpfile2.getline(str, 20);
		cout << str << endl;		
		cout << tmpfile2.get() << endl;*/
		//tmpfile2.read(str,20);		
		string str1;
		getline(tmpfile2,str1);
		cout << str1 << endl;

		ofstream tmp2("temp.txt", ios::out);
		tmp2 << str1;
		cout << "opend 好 yes." << endl;
	}
	else
	{
		cout << "opend 好 no" << endl;
	}
	tmpfile2.close();
	
}
//#include<iostream.h> vs不行
#include<iostream>
#include<string>
#define PI 3.1415926
using namespace std;
void test()
{
	cout << "test the project." << endl;
	int b(2);//=号之外的另一种初始化方式。
	cout << b << endl;
	string little = "woshishui,从哪来？到那去？";
	//string big = 'asd,no,yes?'; 单引号，单字符
	cout << little;
	const int big = 0x4b;
	cout << big;
	wchar_t line[] = L"远离啊，就是你呀。";
	cout << line << PI;
	getline(cin, little);
	cout << little;

	int n = 10;
loop:
	cout << n << ", ";
	n--;
	if (n>0) goto loop;
	cout << "FIRE!";
}
//template:
template<typename T>
void show_type(T para)
{
	cout << para << "的类型大小是" << sizeof(T) << endl;
}
template<class T>
T show_content(T parm)
{
	parm += 20;
	return parm;
}
void use_template()
{
	double bookprice = 13.20;
	show_type(bookprice);
	show_type(3);
	cout << show_content(bookprice) << endl;
	cout << show_content(3) << endl;
}
/*能模版 class么?如果可以的话，自动化编程就剩逻辑控制了。
	模版实现了类型可替换、如何实现关键字可替换？？？？
	模版参数值：template<class T,int num>
	模版特殊化：template <> class pair <int> {｝

*/
#include<vector>
#include<typeinfo>
void type_info1()
{
	int num = 1278;
	vector<int> tizi;
	cout << "num的类型名是：" << typeid(num).name() << endl;
	cout << "tizi的类型名是：" << typeid(tizi).name() << endl;
}
//预处理
#undef n_gram
#define n_gram 2
#define max_num1(x,y) x>y?x:y

#if MAX_WIDTH>200
#undef MAX_WIDTH
#define MAX_WIDTH 200

#elif MAX_WIDTH<50
#undef MAX_WIDTH
#define MAX_WIDTH 50

#else
#undef MAX_WIDTH
#define MAX_WIDTH 100
#endif

#line 123 "文件first.h"
// 123 下一行的新行数 “要文件名”
void test_preporesor()
{
	int maxnum = max_num1(123, 567);
	cout << n_gram << "__" << maxnum << endl;
	cout << MAX_WIDTH << endl;
}
#ifndef __cplusplus
#error need a interpretation。
#endif
//#pragma 对编译器进行配置，参数不支持则忽略。
void point_and_ref()
{
	//测试指针和引用
	int i = 33;
	int *pointer = &i;
	int &ref = i;
	cout << "指针：" << *pointer << "__" << pointer << endl;
	cout << "引用" << ref << "___" << &ref << endl;
	cout << &i << "是i的地址。";
	/* 
	*p 等于ref、pointer等于&ref
	*/
}
//递归深度查看
int find_deep_digui(long long& i)
{
	if (i < 1000000000000){
		++i;//万亿
	}
	else
	{
		return 0;
	}
	if (i % 1000000000 == 0){
		cout << endl;//十亿
		cout << i << ' ';
	}
	find_deep_digui(i);
}//基本认为是无限（到了230亿）


//readfile();  //文件读取
//test();     //类型
//use_template();  //模版
//type_info1();
//test_preporesor();
//point_and_ref();
//long long i = 1;
//find_deep_digui(i);  //查看递归深度









void enum_switch()
{
	//枚举结构的使用
	enum food{
		apple = 0,pine,tea,water,fruit
	};
	food cc= water;
	//switch结构的使用
	switch (cc)
	{
	case apple:
		std::cout << "apple" << std::endl;
		break;
	case pine:
		std::cout << "pine" << std::endl;
		break;
	default:
		std::cout << "no food." << std::endl;
	}
	//结构体的使用   1
	typedef struct body{
		char name[10];
		float  weight;
		double high;
		int age;
	}person;//此处为类型
	person jonson;
	strcpy_s(jonson.name,"qiongsen");
	jonson.weight = 99.8;
	jonson.high = 185;
	jonson.age = 25;
	std::cout << jonson.name << '-' << jonson.age << std::endl;
	//结构体的使用   2
	struct body1{
		char name[10];
		float  weight;
		double high;
		int age;
	}person1;//此处为对象
	person1.age = 25;
	strcpy_s(person1.name, "qiongsen");
	std::cout << person1.name << '-' << person1.age << std::endl;
	//联合体的使用
	union book
	{
		char* CPP;
		char* java;
		char* Csharp;
		char* C;
	};
	//单双指针和字符指针字符变量的区别。
	char* app = "w";
	//char  app1 = "w";  const char* 不能转换为 char
	int i = (1, 2);//,运算结果因编译器而不同，此时为2
	std::cout << "i= (1, 2) 的值为" << i << std::endl;
	//nullptr
	int* cpp = nullptr;
	std::cout <<"nullptr is:"<< cpp;
}










#include<iostream>
#include<cmath>
using namespace std;

const double PI = 3.14159265358979323846;

struct Point
{
	double x;
	double y;
};
double get_prob(double a,double b,double c=5.00000)
{
	double cosx = (pow(a, 2) + pow(b, 2) - pow(c, 2)) / (2.00000*a * b);
	//cout << cosx << endl;
	double prob = acos(cosx);
	//cout << prob << endl;
	return prob/(PI*2.0000);
}
double get_edge_a(Point P)
{
	return sqrt(pow(P.x, 2) + pow(4.00000 - P.y, 2));//4.0的精度可能不够
}
double get_edge_b(Point P)
{
	return sqrt(pow(3.0000 - P.x,2)+pow(P.y,2));
}
void ant_in_triangle()
{
	//直角三角形（3,4,5）中的蚂蚁在任意位置，可能直线走任何方向
	//求 蚂蚁从最长边走出的概率(精确九位小数)
	// 使用所有概率的均值计算总概率。？
	Point P;
	double a, b,prob=0.00000;
	double width = 0.00000001;
	long long times = 0; //统计计算的总点数

	for (double i = width;i<3.0000;i+=width){
		for (double j = width;j<(4.0000-(4.0000/3.0000)*i);j+=width){  //误差发生在4/3
			times++;
			P.x = i;
			P.y = j;
			a = get_edge_a(P);
			b = get_edge_b(P);
			prob+=get_prob(a, b);
		}
		cout << prob << ',';
		break;
	}
	cout << endl << "统计计算的总点数" << times << endl;
	cout << "平均概率值为：" << (prob/times)*100.0 <<",,,"<< prob << endl;
	//33.6608
}









