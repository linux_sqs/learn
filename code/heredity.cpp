/*遗传算法
1.使用年龄（死亡概率）进行个体声明终结，进行代际迭代
*/
#include<stdlib.h>
#include<time.h>
void show(vector<int> &space)
{
	//define that you want see
	int i = 0;
	vector<int>::iterator it;
	for (it = space.begin(); it < space.end(); it++,i++)
	{
		cout << ((*it == 0) ? ' ' : 'O') << ' ';
		if (i >= 30){
			cout << endl;
			i = -1;
		}
	}
}
void save_param()
{
	//save the content you need.
}
void make_live(vector<int> &space)
{
	//param init
	srand(time(NULL));
	vector<int>::iterator it;
	for (it = space.begin(); it < space.end(); it++)
	{
		*it = rand() % 2;
	}
}
void human_and_things(vector<int> &space)
{
	//生与死
	for (unsigned int i = 2; i < space.size();i++)
	{
		if (space[i] == space[i - 1] && space[i - 1] == space[i - 2])
		{
			if (space[i] == 1){
				space[i-2] = 0;
			}
			if (space[i] == 0){
				space[i-2] = 1;
			}
		}
	}
}
void heredity_algri(int max_age = 1000000)
{
	int sumyears = 0;
start:
	//参数初始化
	int year = 0;
	char continued = 'y';
	vector<int> space(902,0);
	make_live(space);
	//世界开始
	while (true)
	{
		//定义每年要做的事
		human_and_things(space);
		if (year >= max_age){  //代数控制
			break;
		}
		year++;
	}
	//show the state
	show(space);
	//deal the end thing

	cout << "次数：" << (sumyears+=max_age) << "continue?";
	cin >> continued;
	if (continued == 'y'){
		system("cls");
		goto start;
	}
	else{
		cout << "mori is coming!" << endl;
		save_param();
	}
}
/*
1.每完成一部分。编译一次。（框架、函数、算法、etc。）
*/
