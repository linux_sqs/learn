#
# 本文为python学习初期的相关笔记和练习
#
#	存在多个main,不要直接执行
#
#	asen 2019-3
float（-1.0） is -1.0   #False
float（-1） is -1.0     #False
float（-1） # -1.0  ???
type（-1.0）<class 'float'>

-3 is ~2 #True
-3 == ~2

True is not False  #True
not False is True  #True

3**-2 == （3**2）**-1  #True
3**-2 == 3**2**-1  #False  不是左结合嘛？
3**2 ==3**2**1  #True

int.bit_length(123)  #  7










abs()		#绝对值
dict()	   #字典类
help()	  #内置帮助调用
min()		#最小值
setattr()  #设置对象参数
all(iter) #迭代器所有元素为真或空iter
dir(obj)   #目录下的名字列表,obj对象拥有的内容
hex()  #16进制转换
next()   #从 迭代器 （生成器）中检索下一个项目
slice()#切片
any(iter)#迭代器存在一个元素为真
divmod(a,b)  #除法a//b
id()    #返回对象的标识（整数）[编号或者内存位置]
object()#返回一个新的无特征的对象
sorted()  #通用排序
ascii()   #类似repr()，返回一个字符串
enumerate(iterable, start=0)   #返回枚举类型 给序列添加索引
input()   #标准输入
oct()#8进制
staticmethod(function)   #返回function的一个静态方法。
bin()  #int->string(二进制)
eval(expression, globals=None, locals=None)   #求值asd=123 eval（‘asd’）  》123
int()  #整数化
open()#打开文件
str(object)#返回一个串
bool()#布尔检测：‘’为false
exec()#执行py代码
isinstance(object, classinfo)  #object是clsaainfo的一个实例
ord()  #unicodestring-> int
sum() #求和
bytearray()#
filter()#？
issubclass(object, classinfo)#是子类
pow()#乘方
super()#返回一个代理对象，它委托方法给父类或者type的同级类
bytes()
float()#返回浮点数
iter()#迭代器对象
print()#输出
tuple()#
callable()#是否可调用
format()#格式化
len()#长度
property()#属性：函数做属性
type(object)#返回object类型
chr()#unicode string
frozenset()#返回frozenset对象
list()#
range()#数列
vars()#返回一个模块、字典、类、实例或者其它任何一个对象的dict属性
calssmethod()#包装成类方法
getattr()#返回对象的属性
locals()#更新和返回表示当前局部符号表的字典
repr()   #返回某个对象可打印形式的字符串表示
zip()#创建一个迭代器，聚合来自每个迭代器的元素。
compile(source, filename, mode, flags=0, dont_inherit=False, optimize=-1)#编译
globals()#返回表示当前全局符号表的字典
map()#映射
reversed()  #返回一个反向迭代器
__import__(name, globals=None, locals=None, fromlist=(), level=0)#不常用
complex()#转化为复数
hasattr(object, name)#确认对象的属性
max()#最大值
round()#返回一个浮点型 近似值
delattr(object, name)#删除对象的属性（name）
hash()#返回对象的hash值
memoryview()#返回给定参数的“内存视图”
set()#返回一个新的set 对象
#**********************************************内置常量
False True
None NotImplemented
Ellipsis #与...相同。特殊值，主要与用户定义的容器数据类型的扩展切片语法结合使用。
__debug__ #如果 Python 没有以 -O 选项启动，则该常量为真。
#由site模块添加
quit()
exit()
copyright
licence
credits
#**********************************************内置类型










#!/usr/bin python3
#qs 2017/2/6
import os		#操作系统关联函数包
print([d for d in os.listdir('.')])  #当前目录下的文件
print(os.getcwd())
print(os.path)
print("os的名字：",os.name)
#print(dir(os));

#日常文件和目录管理任务
import shutil
#shutil.copyfile(‘文件1’，‘文件2’)
#shutil.move("目录一","目录二")
#help(os)
import glob
#print(glob.glob('*.py'))
import sys
#print(sys.argv)#命令行参数 更多参考getopt()和optparse()
sys.stderr.write('Warning, xxxxxxx')
import webbrowser
#webbrowser.open('www.cplusplus.com')

import math
#数学库
print(math.cos(math.pi/4.0))
print(math.log(1024,2))
import random
print(random.choice(['apple','pear','banana']))
print(random.randrange(6))
#import urllib2   urls接收数据
#import smtplib   发送电子邮件

#时间管理
from datetime import date
now=date.today()
print(now)
birthday=date(1964,7,31)
age=now-birthday
print(age.days)

#数据压缩 data compression lib:zlib\gzip\bz2\zipfile\tarfile
import zlib
s='witch which has which witches wrist watch'
print(len(s))
#t=zlib.compress(s)
#print(len(t))
#print(decompress(t))

#********************性能度量 lib：timeit********************************
import time
start=time.clock()
n=0
for i in range(1000000):
    n=n+i
finish=time.clock()
print('一般不太准的时间：',finish-start)
#print(time.localtime())
#--------------------------
def func_time():
    n=1
    for i in range(1000000):
        n=n+i
from timeit import timeit as timeit
print(timeit('math.sqrt(100)','import math'))
print('timeit in func:',timeit('y=map(lambda x:x**10,range(32))'))
print('timeit in sentence:',timeit('x=1'))
import timeit
t2=timeit.Timer('x=range(10000)')
print('timeit normal:',t2.timeit())
#--------------------------
#import time
start=time.perf_counter() #墙上时间
end=time.process_time()  #进程时间
print('较准确的运行时间：',end-start)
#-----------------------------
import time
from functools import wraps
def timethis(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        start=time.perf_counter()
        r=func(*args,**kwargs)
        end=time.perf_counter()
        print('{}.{}:{}'.format(func.__module__,func.__name__,end-start))
        return r
    return wrapper
@timethis
def countdown(n):
    while n>0:
        n-=1
print(countdown(10000))
'''墙上时间：
系统时间(wall clock time, elapsed time). 
是指一段程序从运行到终止，系统时钟走过的时间。
一般来说，系统时间都是要大于CPU时间的。通常这类时间可以由系统提供，
在C++/Windows中，可以由<time.h>提供。注意得到的时间精度是和系统有关系的
'''
#异常 ***********************************************************
'''
try:
    print('java is not enough to you.')
    #raise IOError
except IOError as e:
    print('have sick',e)
import warnings
warnings.warn('调用警告信息',RuntimeWarning)  
import pdb  
def func(n):
    pdb.set_trace() #运行到此 进行调试
    return n+10
func('hello')
'''
#优化-------------------
'''全局范围的代码比函数中的要慢(使用函数)
    消除属性访问（不用.操作）
    访问局部变量要比全局变量快
    避免不必要的抽象：装饰器、属性、描述符都会使代码运行速度变慢(5倍)
    使用内建的容器，
    避免不必要的数据结构和【拷贝动作】
循环里面不用
    .调用、装饰器函数、全局变量、深拷贝
最先降低算法复杂度，然后优化循环内部

'''
import math
def compute_roots(nums):
    result=[]
    sqrt=math.sqrt  #5.704  ->  4.663
    result_append=result.append  #去掉.操作   5.583->4.997
    for n in nums:
        result_append(sqrt(n))
    return result
@timethis
def call_func():
    #test
    nums=range(100000)
    for n in range(100):
        r=compute_roots(nums)
print(call_func())
#测试 lib：doctest  unittest************************************
print('\n ****at this**** \n')
#国际化 gettext locale codecs*******************************************






#****************************************************************

def glossary():
    ''' 类型转换'''
    print(int(3.145))
    print(3+4.5) #自动类型转换
    print(int('30')+3)

def utf_8():
    ''' test python word encoding.'''
    with open("lizi.txt",'r+',encoding='utf-8') as lz:
        sentence=lz.readline()
        print(sentence)
        for word in sentence:
            print('word:',word)

import logging
def use_log():
    '''使用日志模块
        level :过滤掉低于次水平的日志信息
        更改配置后需重启控制台 没有制定文件则输出到控制台

    '''
    logging.basicConfig(filename='test_log.log',level=logging.DEBUG,format='%(levelname)s - %(asctime)s - %(message)s')

    logging.debug('\tdebug 信息 严重级别最低。\n')
    logging.info("\t信息 一般。\n")
    logging.warning('\t提醒 信息\n')
    logging.error('\t信息 错误。\n')
    logging.critical('\t错误信息 严重级别最高\n')

#glossary()
#utf_8()
#use_log()

import math
def used1():
    '''17-12-1  python 内置类型 over
    '''
    x=7
    y=3
    print('乘方：',x**y)
    print('四舍五入：',round(1.4) ,round(1.5) )
    print('截取：',math.trunc(1.9),math.trunc(1.1) )
    print('取底：',math.floor(1.9),math.floor(1.1)  )
    print('取顶：',math.ceil(1.3),math.ceil(1.8) )

    print('数字的大端字节编码：',(1025).to_bytes(2,byteorder='big',signed=True))
    print('数字的小端字节编码：',(1025).to_bytes(2,byteorder='little',signed=True))
    print('2.0 是整型？',(2.0).is_integer() )
    print(len(bin(1024) ),bin(1024) )
    print('hello'.count('l') ,'gg' in 'eggsuprice')

#used1()

# qs 2017/2/9 家中

#re ********************正则表达式模块*****************************
import re
def re_used():
	print(re.findall(r'\bf[a-z]*','which foot or hand fell fastest'))
	print('tea for too'.replace('too','two'))

#re_used()

#*****************多线程&&多进程**************************************
import os
def fork_used():
	pid=os.fork()
	if pid==0:
			print('child procc : %s.'%os.getpid() );   #无逗号
	else:
			 print('I (%s) just created a child process (%s).' % (os.getpid(), pid));
#fork_used()

#pool进程池 multiprocessing模块
from multiprocessing import Pool
import os, time, random

def long_time_task(name):
    print('Run task %s (%s)...' % (name, os.getpid()))
    start = time.time()
    time.sleep(random.random() * 3)
    end = time.time()
    print('Task %s runs %0.3f seconds.' % (name, (end - start)))

def main():
    print('Parent process %s.' % os.getpid())
    p = Pool(4)
    for i in range(5):
        p.apply_async(long_time_task, args=(i,))
    print('Waiting for all subprocesses done...')
    p.close()
    p.join()
    print('All subprocesses done.')

#main()

import subprocess
def subp_used():
	print('$ nslookup baidu')
	r=subprocess.call(['nslookup','www.baidu.com']);
	print('exit code:',r)
#sub_used()
'''
#进程间通信是通过Queue、Pipes等实现的
#多线程 _thread 和threading 模块
#Python虽然不能利用多线程实现多核任务，但可以通过多进程实现多核任务。
#threading.lock() 加线程锁 threadlocal  threading.local()解决同一线程中，函数间参数传递问题。
#多进程缺点：开销大，数量有限 多线程：速度稍快，一关全关。
#C 计算任务 耗cpu ，python io任务		(单进程的异步编程模型称为协程)
#分布式进程：from multiprocessing.managers import BaseManager
'''





#2017-12-18 morning library secondfloor
#deque的使用 栈和队列
from collections import deque


#2017-12-15 morning library secondfloor
import sys
import os
def os_used():
    '''阅读os的文档记录
    '''
    print(os.name)
    #raise os.error
    print(sys.platform)
    print(sys.getfilesystemencoding() )
    #print(os.environ['path'])
    print(os.getcwd() )
    #print(os.getcwdb() )
    print(os.fspath('E:\cpython\17-1'))
    #print(os.get_exec_path() )
    print(os.getlogin())
    print(os.getpid())
    print(os.getppid() )
    print(os.supports_bytes_environ)
    print('常量：',os.O_RDONLY)
    print(os.O_WRONLY)
    print(os.O_RDWR)
    print(os.O_APPEND)
    print(os.O_CREAT)
    print(os.O_EXCL)
    print(os.O_TRUNC)
    print('win32 avaiable:',os.O_BINARY)
    print(os.O_NOINHERIT)
    print(os.O_SHORT_LIVED)
    print(os.O_TEMPORARY)
    print(os.O_RANDOM)
    print(os.O_SEQUENTIAL)
    print(os.O_TEXT)
    
    #print(os.get_terminal_size )
    print(os.F_OK,' ',os.R_OK)
    print(os.W_OK,' ',os.X_OK)
    
    #os.remove(filename)
    #os.removedirs()
    #os.rename()
    #os.rmdir()
    path=os.getcwd()
    with os.scandir(path) as it:
        for entity in it:
            print(entity.name,entity.inode() )
    print(os.stat('lang.py'))
    print(os.supports_effective_ids)
    print(os.urandom(10))
os_used()

#2017-12-2 night library second floor
from math import exp,log,log2,log10,sqrt,tanh,erf
from decimal import Decimal
def math():
    '''  '''
    print(exp(2) )
    print(log(2),log(8,2) )
    print(log2(1024) )
    print(log10(1000) )
    print(pow(2,3) ) #2的3次方
    print(sqrt(10) )
    print(tanh(0.33))
    print('1/2:',erf(14) )
    
    print(Decimal(-7) )
    print(float(0.1)*10 ,0.1*103)
    print(Decimal(0.1*10),0.1*10)
#math()
#2017-10-18
#qs
def math_use():
    import math
    print(math.e)
    print(math.pi)
    print(math.tau)
    print(math.inf)
    print(math.nan)
    
    import random
    persons=['qs','zq','wcl','scl','zts']
    random.shuffle(persons)
    print(random.sample(persons,3))
    print(random.choice(persons) )
    print(random.randint(20,30))
    print(random.randrange(30))
    
    print(list(enumerate(persons)))
    #list() 和[] 还是不同的
#math_use() 

import re
def use_re():
    str='9ab dogcde fg,aesdogcw df&a*c24nddog'
    mached=re.search('dog',str)  #match 匹配起始位置
    print(mached.group(0))

    mached1=re.match('(\w+) (\w+)',str)
    print(mached1.groups(),mached1.group(1))

    pattren=re.compile('[0-9]')  #多次使用时，效率更高
    result=pattren.match(str)
    print(result.group())
    print(result.groups())
    result=pattren.search(str)  #全匹配
    print(result.group(0))

    print(re.split('x*', 'axbc'))

    result=re.findall('[0-9]',str)  #查找所有匹配项
    print(result)

    str1=re.sub('[0-9]','+',str)    #替换所有匹配项
    print(str1)    
#use_re() 
def manage_os():
    import glob
    path='C:/users/asen/'
    print(glob.glob(path+'*'))   
    import os
    print(os.listdir(path))
    print(os.getcwd())
    import shutil  #批量赋值和删除文件
    shutil.copy('DCT.m','b.txt')  #将A 中的内容复制到B
    print(os.getpid())   
#manage_os()  

   
#input('enter over:')


'''
1、使用装饰器。

'''
def asd(func):
    def warper():
        func();
        print("zhuang shi qi,is showing.");
    return warper
    #typeerror:nonetype object is not callable.(返回值出错：多加括号)
@asd  #qwe=asd(qwe)
def qwe():
    print('in the qwe().');
qwe();



'''     python标准库
    库包含内置函数和异常 --无需import导入
    手册组织方式：由内到外。
    17-2-16
'''
#first part builtin type(内置类型)
#主要的内置类型有数字、序列、映射、类、实例和异常。
#真值测试为假：None false 空 任何类型的零。（类型==形式？）
#bool的异常之处：
print(11 and 10,end='\t');#return 10
print(0 and 1,end='\t')#return 0 //若x 为 false, 则结果为 x, 否则结果为 y
print(0 or 11)# return 11
#is 对象id  is not????
# number :          x//y ：商向下取整
print(isinstance(0,bool));
print(2**3);
temp_num = -453;
print(bin(temp_num),'二进制整数所需位数：',temp_num.bit_length());
#to is ->,from is <-
print((50.2).is_integer(),'qs'in 'qsq');#元素在序列中
print(list(range(10,-30,-2)));

'''定长队列：q=deque(maxlen=4)
q.append(2)  q.pop() 尾部弹出
'''
from datetime import date
now=date.today()
print(now)
birthday=date(1992,8,16)
btwn=now-birthday
print(btwn)


#!/usr/bin python3
#qs 2017/2/6
import os
#操作系统关联函数包
print(os.getcwd())
print(os.path)
print("os的名字：",os.name)
#print(dir(os));
#日常文件和目录管理任务
import shutil
#shutil.copyfile(‘文件1’，‘文件2’)
#shutil.move("目录一","目录二")
#help(os)
import glob
#print(glob.glob('*.py'))
import sys
#print(sys.argv)#命令行参数 更多参考getopt()和optparse()
sys.stderr.write('Warning, xxxxxxx')
import re   #正则表达式
print(re.findall(r'\bf[a-z]*','which foot or hand fell fastest'))
print('tea for too'.replace('too','two'))
import math
#数学库
print(math.cos(math.pi/4.0))
print(math.log(1024,2))
import random
print(random.choice(['apple','pear','banana']))
print(random.randrange(6))
#import urllib2   urls接收数据
#import smtplib   发送电子邮件
#时间管理
from datetime import date
now=date.today()
print(now)
birthday=date(1964,7,31)
age=now-birthday
print(age.days)
#数据压缩 data compression lib:zlib\gzip\bz2\zipfile\tarfile
import zlib
s='witch which has which witches wrist watch'
print(len(s))
t=zlib.compress(s)
print(len(t))
print(decompress(t))
#性能度量 lib：timeit
#测试 lib：doctest  unittest
#国际化 gettext locale codecs


#!/usr/bin/env python3
# -*-  coding: utf-8 -*-

'learn to use python language'

__author__='qs'

# python python2.7
# python3 python3.x  //local machine is 3.5.2
# run way :python3  *.py

# (#!/usr/bin/env python3) in the firstline 
# then chmod a+x filename.py

#高级特性********************************************************
L=list(range(100))
print(L[90:100])
print(L[:10])
print(L[-10:-1])
print(L[:10:3])  #第三个数为间隔
print([x * x for x in range(1, 11) if x % 2 == 0] ) #列表生成式
print([m + n for m in 'ABC' for n in 'XYZ'])
#生成器generator   yield替换print （需要时求值）？？？？ next（）
#迭代器
#function********************************************************
#递归函数
def taijie_problem(n) :
		'''台阶问题，最多一次跨三级，跨n级台阶的组合数'''
		if n<1 :
				print("error : n is lower to 1.");
		elif n==1 :
				return 1;
		elif n==2 :
				return 2;
		elif n==3 :
				return 4;
		else:
				return taijie_problem(n-1)+taijie_problem(n-2)+taijie_problem(n-3);
n=input("输入台阶数：")  #英文引号
print("%d级台阶的组合数为%d"%(int(n),taijie_problem(int(n))))
#***************
def top(*asd):  #可变参数。**asd 关键字参数 dict类型 调用时带key
		sum=0
		for i in asd:
				sum=sum+i*i;
		return sum
print(top(1,2,3,7,8,9))
#return x,y,z   //返回tuple
def nop():
		pass
def abs_qs(x):
		if x>=0:
				return x;
		else:
				return -x;
		if x==0:
				return None;  #或return ==None
print(abs_qs(-7));
#*************************fundmentation of python3*********************
#and or not True False None!=0 name is variable
print("10//3=",10//3)
print(ord("A"),chr(66),'\u4e2d')
print( 'Hi, %s, you have $%d.' % ('Michael', 1000000))
sum=0
for x in [1,2,3,4] :
		sum=sum + x
print("sum is:",sum)
print(list(range(10)))
n=99
while n>0 :
		sum=sum+n;
		n=n-2;
print(sum);
dict={'asd':12,'sdf':34,'234':345}
print(dict['asd'])
s=set([1,1,2,3,4,5,5,7])
print(s)
#***************math problem ?
value=input("cin a number:")
value=int(value)
if value >= 0:
	print(value)
else:
	print(-value)
#*******************************
exit()   #out the terminal

 #list tuple dict set的区别
 
 #list =[12,'asd']，list[-1]为倒第一个，尾部：append('jack') pop(),内容可变
# tuple=(3.14,'qwe'),内容不变（可包含list改变）
 #dict={'asd':123,'qwe':234,'zxc':345}   索引结构
#s=set([1,2,3])  无重复 add() remove()''' 





#************************************************************************
#2017 圣诞节 图书馆
# 练习多进程模块的使用
#python 是基于多线程的并发 和 基于多进程的并行
from multiprocessing import Pool
from time import sleep

def multi_func(num):
	'''计算平方值
	'''
	return abs(num*num/17)

def pool_func():
	'''测试pool
	'''
	result=[]
	value=0
	with Pool(4) as p:
		data=[i for i in range(40000000) ]
		#print(data)
		result=p.map(multi_func,data)
	for i in result:
		value+=i
		if value>=10000:
			value=value-1000
	print(value)
	
def single_process():
	'''单进程和pool_func进行对比
		单进程会在不同的核心之间切换
	'''
	result=[]
	value=0
	data=[i for i in range(40000000) ]
	result=list(map(multi_func,data))
	for i in result:
		value+=i
		if value>=10000:
			value=value-1000
	print(value)
	
#***************************************************
from multiprocessing import Process  #书上例题1
import os
def info(title):
	print(title)
	print('module_name:',__name__)
	print('father_id:',os.getppid() )
	print('id:',os.getpid() )
	
def func(name):
	info('function func()')
	print('hello:',name)
	
#***************************************************例2
from multiprocessing import Pipe
def func_pipe(connect):
	connect.send(['湖南',True,18])
	content=connect.recv()
	print('从父进程收到：',content)
	connect.close()
	
#***************************************************例3
from multiprocessing import Manager
def manager_func(d,list1):
	d[1]='10'
	d['10']=1
	d[0.23]=None
	list1.reverse()
	
def test():
	'''多进程 模块 测试函数
		#***************************************
		pool_func()
		sleep(5)
		single_process()
		#****************************************例1
		info('main line')  #显示父进程相关信息
		p=Process(target=func,args=('bob',))
		p.start()
		p.join()
		#***************************************例2
		father_con,kid_con=Pipe()
		p=Process(target=func_pipe,args=(kid_con,))
		p.start()
		print('从子进程收到：',father_con.recv() )
		father_con.send(['harbin',False,33])
		p.join()
		#***************************************例3
	'''
	with Manager() as manager:
		d=manager.dict()
		list1=manager.list(range(10) )
		p=Process(target=manager_func,args=(d,list1))
		p.start()
		p.join()
		print(d)
		print(list1)

if __name__=='__main__':
	'''对于pool 这个主函数声明必须存在
	'''
	test()

'''***************************************************
	好久以前
	模块 相关内容
    Python会在以下路径中搜索它想要寻找的模块：

    程序所在的文件夹
    标准库的安装路径
    操作系统环境变量PYTHONPATH所包含的路径

'''
from clas import bird_use 
#clas 为本文件所在目录的一个python文件 格式如下：

#  import filename 
# from filename import function/class 
#bird_use()

#按序参数传递（正常情况）
#  关键字传递（任意序）
def attrit(a,b):
    print('结果为：', a+b)
    
def att_any(*name): #作为元组传入
    #print('任意参数类型：',type(name))
    print('任意参数为：',name)
    
def att_any1(**argv):
    print('包裹关键字传递：',argv)
    
def used():
    '''
        基本原则是，先位置，再关键字，再包裹位置，再包裹关键字
    '''
    as0=attrit(12,5)
    as1=attrit(b=3,a=5)
    
    att_any(1,2,3)   #变长参数传递
    att_any(1,3,4,5,6,7,'s')
    
    att_any1(a=1,b=3,d='s')  #关键字变长参数传递
    att_any1(n=9,m='sd')
    
    args=(1,5)
    attrit(*args) #变参函数简介调用
    att_any(*args)
    
    dict={'a':1,'b':23,'c':71}
    att_any1(**dict) #关键字变参间接调用
    
#used()   
#**************************************************************
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 09:42:55 2017

@author: asen

@function : using sort function
"""
def sort1():
    '''list1.sort()   只能用于list
    '''
    print("list function 1:")
    list1=[99,4,6,79,9,23,8,2,50,7]
    print(sorted(list1),list1)  #原表不变
    list1.sort()
    print(list1) #原表改变 
    print(list(reversed(list1) ) )
   
class student:
    def __init__(self,name,grade,age):
        self.name=name
        self.grade=grade
        self.age=age
    def __repr__(self):
        return repr((self.name,self.grade,self.age) )

student_obj=[
        student('watson',68,18),
        student('konson',97,17),
        student('first_jon',90,24),
        student('jonson',90,24),
        student('loatao',100,13)
        ]

from operator import itemgetter,attrgetter
def sort2():
    '''默认的排序方式是稳定的 first_jon'和jonson
    '''
    print("元组作为列表元素：")
    array=[('我',1),('里',4),('哪',3),('呀',5),('在',2)]
    print(sorted(array,key=lambda x:x[1]))
    print(sorted(array,key=itemgetter(1) ))
    print(sorted(array,key=itemgetter(1),reverse=True))
    
    print(sorted(student_obj,key=lambda student:student.age) )
    print(sorted(student_obj,key=attrgetter('grade') ) )
    print(sorted(student_obj,key=attrgetter('age'),reverse=True))
    
    print(' Decorate-Sort-Undecorate:')
    decorated = [(student.grade, i, student) for i, student in enumerate(student_obj)]
    decorated.sort()  #student.grade 排序项
    print([student for grade, i, student in decorated] )
    
def sort3():   
    print("字典排序：")
    dict1={'你':1,'在':2,'谁':6,'等':3,'着':5,'待':4}
    print(sorted(dict1,reverse=True) )
    #print(sorted(dict1.keys() ) )  #默认情况
    #print(sorted(dict1.values() ))
    #print(sorted(dict1.items() ) )
    # assert dict1 == {}  断言的使用方式：AssertionError抛出
    
    dict2={1:'禁',4:'城',2:'固',6:'里',5:'堡',3:'在',7:'了'}
    print(sorted(dict2) )
    #print(sorted(dict2.keys() ) )
    #print(sorted(dict2.values(), ))
    #print(sorted(dict2.items() ) )
    #print(sorted(dict2.items(),key=lambda x:x[0] ) ) 
    #等价于上一条语句
    
    #按值  给键排序
    print(sorted(dict1,key=dict1.__getitem__) )  # ****
    #按键 給值排序
    
    print([b for (a,b) in sorted(dict2.items() )])
    
if __name__ =='__main__':
    #sort1()
    #sort2()
    sort3()

#==============================================



#coding=utf-8
#该脚本用于python中的小知识点的总结

def common():
	print(isinstance(3,int)) #3是int型的实例
	print(vars(int))  #查看对象的字典表示
	
def evalused():
	#eval（）：表达式求值,与repr()互为逆操作
	a='[1,2,3,4]'
	b=eval(a)
	print(a,type(a),b,type(b))
	c=repr(b)
	print(c,type(c))


#qs 17/2/9 家中
'常见功能'
from turtle import *
color('black', 'blue')
begin_fill()
while True:
    forward(300)
    left(170)
    if abs(pos()) < 1:
        break
end_fill()
done()

'''
from datetime import datetime
now=datetime.now()
print('现在时间是：',now)
print(now.strftime('%a, %b %d %H:%M'))
print(now.timestamp()) #1970/1/1 到现在的秒数
t=0
print(datetime.fromtimestamp(t))
cday = datetime.strptime('2017-6-1 18:19:59', '%Y-%m-%d %H:%M:%S')
print(cday)
#namedtuple deque(双向链表) defaultdict() ordereddict( in collections )dict无序
#counter in collections  import base64 struct hashlib itertools
#from contextlib import contextmanager   urllib(url解析)

#图形界面 qt gtk tkinter wxwidgets
#数据库：
#MySQL，大家都在用，一般错不了；
#PostgreSQL，学术气息有点重，其实挺不错，但知名度没有MySQL高；
#sqlite，嵌入式数据库，适合桌面和移动应用

#web django

#异步io
#协程 asyncio aiohttp (install)
'''





#**************************************************************
def h():
    print('爱爱死你咯i急哦')
    m=yield 3
    print(m)
    d=yield 100
    print('we are together.')
    print('m,d:',m,d)
    yield 1000

c=h()
m=next(c)
d=c.send('wow!')
print(d)
next(c)

#12-2-2017 morning 《快速介绍python》
def sec1_python():
    ''' 给列表建立一个索引从零开始
    '''
    alist=['how','dare','you','can','do','this','things','?','jhons']
    elist=enumerate(alist)
    print([x for x in elist])
    
    # * 号的意义
    print('上海'*3)
    print([1,3,1]*3)
    print(('1',8)*3)
    print(3*3)

    print(list((['p','g','m'],[1,2,3,4,5])) )
    a,b=3,3+4  #同时赋值
    print(a,b)
    print(1<2<3==0)
    
    print('set操作:')
    s={1,2,3}
    t={2,3,4}
    print(s|t)  #并集
    print(s&t)  #交集
    print(s^t)  #并-交 【各自独有】
    print(s-t)  #s中去掉t中的
    print(s>={1})
    print(s<={1,2,3,4})
    print(s.add(5),s )
    print(s.remove(5),s )  #不存在报 keyerror
    print(s.discard(5),s )  #不报错
    print(s.pop() ,s)
    print(s.clear() )

        
    
#sec1_python()
#**********异常处理**********************
def test_exp():
    try:
        #raise StopIteration  #主动跑出异常
        m=1/0
    except NameError:
        print(' name error')
    except ZeroDivisionError:
        print('/0 error ')
    except StopIteration:    
        print('StopIteration have.')
    finally:
        print('over it .')
#test_exp()  

#动态类型(dynamic typing)是Python另一个重要的核心概念
def dyc_type():
    asd=123435
    print(asd)
    asd='asd is me.'
    print(asd)
    asd=[3.24,'q','qw',123324]
    print(asd)
    
    '''
        函数的参数传递，本质上传递的是引用
        由于python动态类型，
    '''
    a=10
    print('a的初始位置1：\t',a,id(a))
    a=9
    print('a的初始位置2：\t',a,id(a))
    a+=9
    print('a的初始位置3：\t',a,id(a))
    def prt(a):
        print('函数内的值1：\t',a,id(a))
        a=24
        print('函数内的值2：\t',a,id(a))
    prt(a)
    print('函数外的值1：\t',a,id(a))
    a=111
    print('函数外的值2：\t',a,id(a))
    
#dyc_type()   
 
#声明式语言包括数据库查询语言（SQL，XQuery），
#正则表达式，逻辑编程，函数式编程和组态管理系统。
def  magic_method():
    print('a'+'b','as'.__add__('df'))
    lt=[1,2,3]
    print(len(lt),lt.__len__())
    print(lt[1],lt.__getitem__(1))
    #任何一个有__call__()特殊方法的对象都被当作是函数
    class sample:
        def __call__(self,a):
            return a+3
    add=sample()
    print(add(4))        
        
#magic_method()  
#上下文管理器 with 。。 as ..
#上下文管理器有隶属于它的程序块
  
#装饰器
def decrote(func):
    def new():
        print('装饰器添加的内容。')
        return func()
    return new
@decrote   
def food():
    print('this food is delisous.')

#food()
def alloct():
    # True
    a = 1
    b = 1
    print(a is b)

    # True
    a = "good"
    b = "good"
    print(a is b)

    # true
    a = "very good morning"
    b = "very good morning"
    print(a is b)

    # False
    a = [1,2,3]
    b = []
    c=a
    print(a is b)
    
    import sys
    print(sys.getrefcount(c))
    del a
    print(sys.getrefcount(c))
    
    #Python不能进行其它的任务
    #频繁的垃圾回收将大大降低Python的工作效率
    import gc
    print(gc.get_threshold())
    gc.collect()
    #Python同时采用了分代(generation)回收的策略
    #存活时间越久的对象，越不可能在后面的程序中变成垃圾
    #gc.set_threshold(700, 10, 5)  #设置阈值
    #Python作为一种动态类型的语言，其对象和引用分离
    
    a=[1,21,3,1,3,1,2,4]
    print('存在？',1 in a,'次数：',a.count(1))
    print('元素4的索引',a.index(4))
    
    #查询对象所属类 及名称
    print(a.__class__)
    print(a.__class__.__name__)
    print(list.__base__)  #查询父类
    
    print(0b101100) #二进制表示
    print(0o20)  #八进制表示
    print(0x2a)  #十六进制表示
    
    print(sys.path)
    print(divmod(11,3))
    #print(unichr(100)) #unicode 对应的字符????
    print(globals())  #全局命名空间
    print(locals())   #局部命名空间
#alloct()   
input()
'''
    Python的哲学是 一个问题只有一个最好的解决方法。
    这些标准库为许多问题提供了一个标准的解决方案
''' 



#二次回顾练习程序 2016/8/29/22:16
#false=0=空元组
#数据库和XML是两种流行的复杂数据存取手段。
#**************************************************选择循环迭代
import random
id=input("python project.")
rand1=random.randint(1,100)
if rand1<=50 or rand1>0:
    print(rand1)
elif rand1<80 and rand1>50:
    print(rand1+1)
else:
    print(rand1-2)
bsd="中国\t"
i=0
if not i:print("if not's using way.")
while i<3:
    print(bsd,"bsd")
    i=i+1
#输出所有，输入的字符.range(start,end,计数单位)
#常量 全部大写
for art in id:
    print(art)
for i in range(3):
    print(i,"jie")
if "asd" in id:
    print("asd is in the id")
CCD="ZHESHI CHANGLIANG"
print(CCD)
#切片 元组******************************************************
print(id[3:])
元祖=("liuye","逼格","阿凯",
      "dalin","阿冰","asen",789,123.234)
print(元祖[2],元祖,元祖[3:5],"you have",len(元祖),"'s number")
#元组具有不变性，+即可连接元组(只能连接相同的序列。)。小括号。
#列表list具有可变性,中括号哦。元组比列表快。
list1=[1,2,3,4,56]
print(list1[1:4])
list1[2]=2
list1.append(123)#在列表尾部添加方法。
list1.sort()
list1.remove(2)
print(list1,list1.count(2))
#字典（键值key-value,成对存在,key不变，value可变）
dictionary={1:"this is the first string",
            2:"you are right!!!!!!!!!!",
            3:"i trust you indeed.....",
            4:"brain is the important."}
del dictionary[1]
print(dictionary.items(),"\n",dictionary.keys())
#***************************************************************函数
#函数定义def function name :\n\t function context
#文档字符串 函数内第一行 “”“。。。。”“”三重引号
def func(book="c++"):#括号,默认参数，形参
    """my first python function in 2016/8/31/"""
    print("this is a function",book)
    return "ok"
asd=func("yes")#调用方式，实参
print(asd)
#文件读写操作****************************************************/
text_file=open("readwrite.txt","r")#read
print(text_file.read())
text_file.close()

text_file=open("readwrite.txt","w")#清空已有,write
text_file.write("jiushiniya,,,...")
text_file.close()

text_file=open("readwrite.txt","a+")#读取和添加操作add+read
text_file.write("爱尽快恢复撒饭的话")
print(text_file.read())
text_file.close()
#复杂数据存入文件,w是覆盖，r不存在报错，+读写，a添加######################
#pickle腌制，后只能存入二进制文件
import pickle,shelve
p_file=open("p_file.dat","ab")#wb,rb,+....
pickle.dump(list1,p_file)
p_file.close()
p_file=open("p_file.dat","rb")
list1=pickle.load(p_file)
print(list1)
p_file.close()
#shelve 将多个列表存入单个文件中
she=shelve.open("pickles_shelve.dat")
#第二个参数省略
#默认为c：读写(不存在则创建)，n：创建新文件，r：read已存在的文件，w：建立新文件并写入
she["a"]=["adfsdfsd"," sdjjsadovj","撒旦那附近；"]
she["b"]=["sfoiiojion","jkshiouhfnv","dfgedfg"]
she.sync()

print(she["a"],"\n",she["b"])
she.close()
########################exception##############################异常处理
try:
    bbc=int(input("qing shu ru:"))
except ValueError as e:
    print("shuzhi zhuanhuan error\n",e)
except:
    print("这里有一个异常发生！！！")
else:
    print("无任何异常")
#与外界有交互的地方都应该 进行异常处理
#制定具体异常类型是一种良好的编程习惯。（解释器中查看一场类型）
#e为异常参数。描述异常的证实消息。
#**********************************************************
#输入。
input("\nI'm right here waiting for you ...\n")




#==============================================




'''
    单行文本与多行文本的区分
    enumerate的使用方式
'''


def syntex_used():
	'''语法检验
	
	'''
	word='enough.'
	print("java is not {}".format(word))
	print('java is not',word)
	#单行文本与多行文本
	string1=('this is a questions' 
			'ok this you' 
			'are you ok?'
			'yes you are.' 
			'nothing write you.')  #单行文本
			
	string2='''This is a string with embedded newli,
This is a string with embedded newli,\
This is a string with embedded newli,
This is a string with embedded newli 
		'''   #多行文本  \后不能有任何东西
	print(string1)
	print(string2)
	
	#enumerate的使用
	input=['dura','F0','asd','asdq','asd5','qweqw']
	example1={f:i for f,i in zip(input,range(len(input) ))}
	example2=dict(zip(input,range(len(input) )))  #注意括号匹配
	example3=list(enumerate(input) )
	example4=dict(enumerate(input) )
	example5={f:i for i,f in enumerate(input)}
	print(example1)
	print(example2)
	print(example3)
	print(example4)
	print(example5)
	
#syntex_used()

#****************1 *************************************
def func1():
	print('hello world!')
	#这是2016、4、22、18:23#
	help('print')
	print("你是谁？")
	print(''' "快告诉我你是谁？".\t[330].haha!''') 
	print('what\'s your name?\'')
	print(r'这是自然字符串!')  #R,r
	print(u'unicode string,\
		  not english language')
	#没有char类型，单引号和双引号相同，用自然字符串处理正则表达式。
	#变量以字母、_开始，区分大小写。
	#Python把在程序中用到的任何东西都称为 对象
	i=3
	print(i) 
	i=i+7;print(i)        #一句一行，多句用;  
	s=' 原来就是你 '
	print(s)              #不需要声明数据类型
	#同一层次的语句必须有相同的缩进
	#在每个缩进层次使用 单个制表符 或 两个或四个空格 三选一
	#运算符 **幂 //商的整数 ^按位异或 ~按位翻转
	# ==对象相等 bool:not and or 运算符优先级：括号
	#同级：左结合
	exit(0) 
	
func1()
#*****************2*******************************
def func2():
	i=10
	if i<20:
		print(i+11)
	if i==10:
		print(i)
	elif i<10:
		print(i-1)
	else:
		print('asd')
	#条件以：结尾;无switch语句
	ture=1    
	if ture:
		print('---------正确输出------')
	true=True
	while i>=0:
		if i>=1:
			print(i )
			i=i-1
		else:
			i=i-2
			print(i)
			break  #跳出循环
	else:
		print('条件不满足时执行.else exe once time')
	for i in range(1,10):  #冒号
		print('变量' ,i)
	else:
		print('for循环结束！')
	#range（起始数，终止数，步长【默认1】）显示数<终止数。
	TRUE=i=1       #须提前定义
	while TRUE:
		print(i)
		i=i+1
		if i>5:
			TRUE=0
			#break  #缩进。
	else:
		print('可省略')
	#continue 本重循环结束
	a,b,c=1,2,3;
	print("同时赋值",a,b,c);

#func2()
#***************3******************************
def func3():
	#函数
	def function( a,b=9):#默认参数:形参赋值。
		if a==b:
			print('==')
		elif a<b:
			print('min=',a)
		else:
			print('min=',b)
		return 0
	function(10,77)
	i=12;j=21
	function(i,j)
	#所有变量的作用域是它们被定义的
	#块，从它们的名称被定义的那点开始.默认为局部变量。
	#global x，y,z 引入外部变量。
	#带默认参数的形参必须在无默认参数的形参之后。
	#形参之间无序，
	function(b=4,a=3)   #关键参数：加名字
	
	def some():
		pass
	some()  #无回显
	#函数默认return none

	#文档字符串 docstring适用于模块和类
	#惯例 首行：大写字母开始，句号结尾。第二行：空第三行开始。
	def callc(a,b):
		''' You are right!lady.

			when i was a boy ,too young too simple!
			
			lookafter the day:   nice 
		'''
		a=int(a)
		b=int(b)
		a=a+b
		print('jieguo=',a)
		return 0
	callc(4,8)
	#callc(__doc)#wrong!!?
	#help(callc)[:10]
	print('q\n')#quit
	
	#模块：包含所有已定义的变量和函数的文件。
	import sys
	print('the cmd line arguments are:')
	for i in sys.argv :
		print(i)
	#print('\n\nthe python path is',sys.path,'\n')
	#字节编译 .pyc 结尾，字节码加快运行速度
	#from sys import * 导入所有系统模块。
	#查看模块调用方式
	if __name__=='__main__':
		print('该程序自我调用.')
	else:
		print('从其他模块导入。')
	def sayhi():
		print('''hi,创建模块程序 ''')
		version='0.1'
		
		import sys
		#print(dir(sys))
		#print(dir(__doc__))
		print('..........')
		#print(dir())
	sayhi()
	#exit(1)
	
#func3()
#************4****************************************
def func4():
	#数据结构：列表 元组 字典。
	#列表：可变数据类型：增删改查
	#print(help(int))
	shop=['apple','pear','bitch']
	print('我有',len(shop),'要买')
	print('项目是：')
	for item in shop:
		print(item,end=' ')        #自动打印换行符
	shop.append('rice')
	shop.sort()
	
	old=shop[0]
	del shop[0]  #删除首项
	print('我要买',old)
	print('我要买',shop)
	
	#help(list)      #查看列表帮助
	#元组：不可变。
	zu=('123','asd','456','qwe')
	print('元组',zu,len(zu))
	new_zu=('zxc',zu)
	print('新元组',new_zu,len(new_zu))
	name='sqs'
	age=23
	print('我叫',name,',今年',age,'岁')
	
	#字典：无序.键值对
	bat={'1':'akai','2':'aliu','3':'abing'}
	n_bat={1:'4399',2:'7431',3:'7788'}
	print(bat['2'])
	print(zu[1:3])  #输出1’2
	#help(dict)
	
	#序列：列表、元组、字符串都是序列，
	#序列特性:索引操作符（一个）、切片操作符（一部分）
	string='qwertyuiop'
	print(string[:])#输出全部
	print(string[:-1] ) #除去最后一项
	#引用： 当你创建一个对象30并给它赋一个变量i的时候，这个变量i仅仅引用对象30，而不是表示这个
	#对象30本身！变量名i指向你计算机中存储对象30的内存。这被称作名称到对象的绑定
	i=30;
	#exit()
#func4()

#*************5*************************
def func5():
	#类：类型【int】。对象：类的实例【i】
	#help(int)
	#python所有的量都是对象。
	#域：属于类或对象的变量。类的方法：属于类的函数。用class创建类
	#域和方法合称为属性。域：1.实例变量（属于类的对象）2.类变量（属于类本身）
	#类的方法只比普通函数多第一个参数，自动赋值：对象本身（惯例：self）
	#即使类的方法不带参数，也回自动添加self。功能部分
	class per:
		pass
	p=per()
	print(p)
	
	class son:
		def hello(self):     #self can't lost#
			print('hello,how are you?')
			print('my name is:%s'%self.name)
		def __init__(self,name):
			self.name=name
	s=son('sqs')    #传递参数
	s.hello()
	
	#__init__ 初始化方法，建立对象时调用。
	#类和对象的数据部分。它们只是与类和对象的名称空间 绑定 的普通变量
	#类的变量由类的所有对象共享。
	#python 所有的类成员均为public，私有变量：双下划线开始：__privite
	#继承：代码重用的方法之一。
	class super:
		'''父类定义'''
		def __init__(self,name,age):
			self.name=name
			self.age=age
			#print('supername:',self.name ,'superage:',self.age)
		def tell(self):
			print('asdasdasd',name,age)

			
	class son1(super):            #父类传递
		def __init__(self,name,age,tall):
			super.__init__(self,name,age)#父类调用，非自动创建
			self.tall=tall
			print(name,age,tall)
		def tell(self):
			super.tell(self)             #父类调用，非自动创建
			print(self.tall)

			
	class son2(super):
		def __init__(self,name,age,face):
			super.__init__(self,name,age)
			self.face=face
			print(name,age,face)
		def tell(self):
			super.tell(self)
			print(self.face)
	a=son1('son1:asd',29,172)
	b=son2('son2:qwe',23,72)
	#父类传递有多个时：多重继承。

	today=input('输入一行：')  #从键盘读取。 
	print('result is:',today)
	
	#help(input)
#func5()

#*************6*******************************
def func6():
	#help()  打开帮助系统
	#存储器pickle和 (cpickle快1000倍）
	#将对象进行文件保存
	import pickle as pi
	
	shop=['apple','mango','tomato']
	shop+=[i for i in range(20)]
	f=open('shopfile','wb')
	pi.dump(shop,f)
	f.close()
	
	f=open('shopfile','rb')
	stored=pi.load(f)
	print(stored)
	
#func6()

#***************7************************
def func7():
	asd=[1,2,3,5,66,1,0];
	asd.insert(2,4);
	asd.sort();
	asd.reverse();
	print(asd);
	
	#both of the first one.
	print(asd.index(1));#1's index.
	print(asd.count(1));#1 's times.
	
	asd.pop();#no argu is pop the last one.
	asd.pop(0);
	print(asd);
	print([(x,x**3,x/4) for x in range(1,6)]);
	
#func7()
#*****************8*********************
def func8():
	a=-90;
	b=abs(a)-44;#
	
	print(bool(b));#二值。
	
	print(ascii(b));
	
	b=bin(64);#二进制
	
	d=oct(66);
	c=chr(66);
	print(b,c,d);
	
	i=200;
	print(i,':',chr(i),end='');#字符
	
	print('浮点数：',float(64/7));#浮点
	
	print('2000',"转换为十六进制为：",hex(2000));#十六进制。
	
	print('id',id(i));
	
	s=input('输入一行：');
	d=int(a+0.6);#转换为int
	print(s,d,len(s),max(abs(a),a+4));#长度
	
	import array;
	a=array.array('b',[5,3,1]);
	
	z=memoryview(a)
	print(z,z.tolist(),str(d),type(a));
	
	x=[1,2,3,4];
	y=[6,7,8,9];
	print(list(zip(x,y)));
#func8()
#*****************9**********************
def func9():
	#异常&错误
	#python区分大小写。
	s=input('请输入：')
	print(s)
	
	import sys
	try:
		a=input('请输入：')
	except EOFError:      #可以加（）包含多个异常。
		print('there is a eof error!!')
		#sys.exit()
	except:
		print('some wrong have occurred!!')
		print('have done.')
	#except： 处理所有异常。

	#raise 引发异常
	class short(Exception):
		def __init__(self,length,atleast):
			Exception.__init__(self)
			self.length=length
			self.atleast=atleast
	try:
		s=input('请输入>3：')
		if len(s)<3:
			raise short(len(s),3)
	except EOFError:
		print('eoferror')      #1
	except short(3,2):    #???不允许从父类继承？？
		print('double',s.length,s.atleast)
	else:
		print('no exception was raised')#1
	
	#inherit 继承 finally最终处理，关闭文件
	import time
	try:
		f= open('document_get.py','rt',encoding='utf-8') 
		while True:
			lin=f.readline()
			if len(lin)<=10:
				break
			time.sleep(1)
			print(lin)
			
	finally:
		f.close()
		print('文件已经关闭。')
		
	#列表综合
	list1=[2,3,4,5,6]
	list2=[2*i for i in list1 if i>2]
	print(list2)
	#1
	#函数接受元组加参数*srgs，字典**args
	#lambda创建新的函数对象，并且在运行时返回它们
	def repeat(n):
		return lambda s:s*n
	tw=repeat(5)
	print(tw('asdy '))
	print(tw(5))
	
#func9()
'''
	comment注释indentation缩进instance实例
	list comprehension 列表综合。module模块
	derived class导出类literal constant字面意义上的常量
	method方法 operand操作数
	escape sequence转义符operator运算符
	parameter形参pickle存储器sequence序列
	statement语句shebang line组织行tuple元组variable变量
'''
#*******************10*********************************
def func10():
	data=["琼森",20,185,("2016-11-24","morning")]   #[]
	name,age,height,dated=data
	print(name,age,height,dated)
	_,_,_,dtime=data #任意变量名占位
	name,*num=data
	#参数多和少都会出错，等量多重赋值
	p=(4,5,7)
	a,b,c=p
	print(a,b,c)
	s="hello"
	a,b,c,d,e=s
	print(a,b,c,d,e)
	
	from collections import deque 
	q=deque()#(maxlen=3) 没有最大长度就是任意长度
	q.append(12)
	q.append(22)
	q.append(32)
	q.append(42)
	q.pop() #尾部弹出 栈
	q.popleft() #头部弹出 队列
	print(q)
	
	import heapq
	num=[12,34,-23,45,67,789,2,5,5,78,0]
	print(heapq.nlargest(3, num))
	print(heapq.nsmallest(3, num))
	heapq.heapify(num)#heap[0] 最小
	a=slice(1,100,3)
	print(a.start,a.stop,a.step,a)
	#**************文件操作*************
	with open('document_get.py','rt',encoding='utf-8') as file1:
		data=file1.read()
		#print(data)
		#file1.close() with中不需要close()
		
	import os
	if not os.path.exists('记事.txt'):
		with open('记事.txt','at') as file2:
			file2.write('\n end.')
			print(file2)
		#缩进对齐 ！！！
	else:
		print('it\'s fail to open the file')
	#*************返回目录中所有文件。
	print('目录内容：',os.listdir('D:/')[:5] )
	
	import os.path
	print('是文件吗？',os.path.isfile ('d:/'))
	
	import time
	print('Return the time of last modification of path:',time.ctime(os.path.getmtime('d:/anaconda')))
	
	import sys
	print('文件系统编码方式：',sys.getfilesystemencoding())
	
	#*********function*****
	
	add=lambda x, y: x+y  #运行时绑定
	print('lambda 数字计算',add(23,32))
	print('[泛型]字符串计算',add('asd',' is me'))

	import gc
	gc.collect()    #手动垃圾处理。
	
#func10()
#***********模块************
#建立模块：在各级目录下建立 __init__.py 空文件。
#在init .py 中，将2 个文件粘合在一起：
# __init__.py
#from .a import A
#from .b import B
#创建一个.pth 文件，将目录列举出来,放在某个Python 的site-packages 目录

#11/12  18page

#***********************11************************









#方法解析顺序mro列表采用c3线性化处理：python的继承实现
class basic:
	def __init_(self):
		print('basic')

class A(basic):
	def __init_(self):
		super.__init_()
		print('A')
class B(basic):
	def __init_(self):
		super.__init_()
		print('B')
class C(A,B):
	def __init_(self):
		self.asd='' 
		print('C')
		super().__init_()

def usedmro():
	print(C.__mro__)
	print(A.__mro__)
	print(basic.__mro__)
	print(dir(basic))  #默认继承object类
	basic.color='red'  #运行时添加属性 这都可以！！
	print(basic.color)
	c1=C()
	
	c1.name='ques'  #运行时添加属性
	print(c1.name)
	c1.name
usedmro()


'''******************************************************
    学习 python3 面向对象编程 OOP
        17-10
    经验：
        所有内容都放函数里：函数化（看起来更清晰明了）
    如何实现：【所见即所得】
'''
class vehicale:
    def __init__(self,num_wheels): #初始化对象时调用
        self.num_wheels=num_wheels #对象属性
    def num_wheels(self):
        return self.num_wheels
    def set_num_wheels(self,num):
        self.num_wheels=num
def vehicale_use():       
    chang_an=vehicale(4)  #对象定义
    print('轮子数：',chang_an.num_wheels)
    
    chang_an.num_wheels=6
    print('轮子数更新：',chang_an.num_wheels)
    
    chang_an.set_num_wheels(11)
    print('轮子数更新：',chang_an.num_wheels)   
#vehicale_use()


class vehicales:
    def __init__(self,num_wheels):
        self.num_wheels=num_wheels
    @property
    def num_wheels(self):
        return self.num_wheels
    #@num_wheels.setter
    def num_wheels(self,nums):
        self.num_wheels=nums
    def mak_noise(self):
        print('VNNNNNNNNNNNMKHJHYU')
def veh_use():       
    chang_an2=vehicales(4)
    print('轮子数：',chang_an2.num_wheels)
    chang_an2.num_wheels=6
    print('轮子数更新：',chang_an2.num_wheels)        
    chang_an2.mak_noise()
#veh_use()


class person:
    lastname='fangfang'  #类属性
    _food='fruits'     #私有变量 ：不能在类外直接赋值
    def __init__(self,firstname,java):
        self.firstname=firstname  #公开实例变量
        self._place='yetnan'   #私有实例变量
        self._java=java     #self代表了根据类定义而创建的对象
    def _show(self):
        print('应该在类内部使用的私有方法。')
def person_use():   
    tk=person('tk','just-so-so')
    print(tk.firstname)
    print(tk.lastname)

    tk._place='india'
    print(tk._place) #india   ????
    tk._food='banana'
    print(tk._food)
    tk._java='good'   #私有变量怎么更新了??????
    print(tk._java)

    tk._show()
#person_use()


class bird(object):
    have_feather=True
    way_of_reproduction='egg'
    def move(self,dx,dy):
        position=[0,0]
        position[0] += dx
        position[1] += dy 
        return position
class chicken(bird):
    way_of_move='walk'   #类属性 类属性值的改变会影响所有的对象。
    possible_in_kfc=True
class oriole(bird):
    way_of_move='fly'
    possible_in_kfc=False
def bird_use():
    ''' 用继承来说明父类-子类关系。子类自动具有父类的所有属性。
        通过继承制度，我们可以减少程序中的重复信息和重复语句.
        面向对象提高了程序的可重复使用性
    '''
    print('继承：')
    summer1=chicken()
    print('summer1:',summer1.have_feather,summer1.move(4,5))

    summer=bird()
    print('after move:',summer.move(5,8))
    print('列出类属性：',bird.__dict__)
    print('列出对象属性：',summer.__dict__)
class me():
    def __init__(self):
        self.age=20
    #特殊属性定义&&使用
    def getadult(self):
        if self.age<18:
            return False
        else:
            return True
    adult = property(getadult) 
    #当我们查询一个属性时，如果通过__dict__方法无法找到该属性，
    #那么Python会调用对象的__getattr__方法，来即时生成该属性
    def __getattr__(self,male):
        return True
def use_me():
    me1 = me()
    print('特殊属性：',me1.adult)
    print('未知属性：',me1.male)
        
if __name__=='__main__': 
    #必须放到最下边。
    #vehicale_use()
    #veh_use()
    #person_use()
    print('/*  - - - - --  -- - - -*/')
    print('/*  - - - - --  -- - - -*/')
    print('/*  - - - - --  -- - - -*/')
    print('/*  - - - - --  -- - - -*/\n')
    #bird_use()
    use_me()


#********************************************************************


#*************************************基本对象
#attribute特性instantiate实例化,类名约定首字母大写
#constructor 构造器,函数默认是public私有特性以__开头

class Lei(object):
    """python class"""
    canshu1=123
    __canshu2="asdasd"
    def __init__(self):
        Lei.__private1()#私有方法调用,无括号
        print("构造器 构建完成。")
    def say(self,ids):#默认第一参数。
        print("class function called!\n",self,ids)
    def __str__(self):
        return "fanhui lei"
    def __private1():
        print("私有方法调用成功。")
    @property
    def name(self):
        return self.__canshu2
    #静态方法的参数可以有，一般无
    @staticmethod
    def fangfa():
        print(ip1.__canshu2)

ip1=Lei()
ip2=Lei()
ip1.say("OK")
ip2.say("yes")
print(ip1)#call __str__()
print(ip1.name)
print("\t*********************")
#*************************************多对象关系
#接发消息，a调用b的方法。
#多态：以相同方式处理不同事物的方法。
class aaa(object):
    def asd(self,q):
        q.qwe()
        print("asd function!")
class bbb(object):
    did=100
    def __init__(self,do=99):
        self.did=do#self可省略
    def qwe(self):
        print("qwe function")
    def jicheng(self):
        print("继承实现了!")
    def call_p(self):
        print("调用父类成功")
a=aaa()
b=bbb(66)
a.asd(b)
#继承：由抽象到具体的过程。（由模糊到清晰）
class ccc(bbb):
    def zxc(self):
        super().call_p()
    def qwe(self):
        print("重写b的方法。完成")
c=ccc()
c.jicheng()
c.qwe()
c.zxc()
#模块：1、实现代码复用2、按逻辑拆分，易于管理3、分享精神
#模块导入：import 文件名（无后缀）两文件在同一目录下。
#__name__==__main__ （作为文件执行）










#!code=utf-8

'''性能分析：profiling
    两类：基于事件的性能分析 event-based profiling（最后的选择，准确）
          统计式性能分析   statictical profiling 
            以固定的的时间间隔对程序计数器（program counter）进行抽样统计。【分析的数据更少，对性能的影响更小】
    
'''
#事件跟踪方法  弊端：输出的信息太多
import profile
import sys

def profiler(frame,event,arg):
    print('profiler:%r %r' %(event,arg))

#sys.setprofile(profiler)

def fib(n):
    if n==0:
        return 0
    elif n==1 :
        return 1
    else:
        return fib(n-1)+fib(n-2)

def fib_seq(n):
    seq=[]
    if n>0:
        seq.extend(fib_seq(n-1))
    seq.append(fib(n))
    return seq
#print(fib_seq(2))

#--------------------------------

'''
思路：
    发现问题，（出现故障，不和客户要求）
    找到问题实质，（测试，推断，检测）
    解决问题：代码优化，工具优化

性能分析需要花时间，只有在程序出错的时候采用。
    压力测试；保证性能（寻找性能边界）
    产品负载过重时内存泄漏就会发生
    不存在透明的性能分析器
    io关联的代码 很难优化
指标：
    时间、内存消耗
最佳实践：
    建立回归测试套件、代码结构（函数封装）、耐性
    多收集信息、数据预处理、数据可视化
检测性能瓶颈：
    检测时间、空间消耗
代码优化
    选择数据结构和算法，多进程（并行化）
'''

#@1 python性能分析工具：cprofiling
import cProfile
import re

def food():
    position=1
    while position<1000:
        re.compile("foo|bar")
        position+=1
#cProfile.run('food()')

#@2
def runre():
    import re
    re.compile("foo|bar")
def use():    
    prof=cProfile.Profile()
    prof.enable()
    runre()
    prof.create_stats()
    prof.print_stats()
#use()

#@3: python -m cProfile FP.py -o FP.profile
profile.run('print(fib_seq(20))')

#---------线性性能分析 生成报告文件-----------------------
#pip install line_profiler
# kernprof -l name.py
from functools import reduce 
#@profile
def test_profile():
    java=range(1,3000,7)
    java=list(map(lambda x : x+floor(random()*10),java) )
    value=reduce(lambda x,y:x+y,java)
    print('result :',value)
#test_profile()
#--------------------------------------

#借助多线程 ，多进程 充分利用硬件性能 python2中
import _thread
import time

def print_time(name,delay):
    count=0
    while count<10:
        time.sleep(delay)
        count+=1
        print("%s:%s " % (name,time.ctime(time.time()) ) )
def thread_start():
    try:
        _thread.start_new_thread(print_time,("thread-1",1))
        _thread.start_new_thread(print_time,("thread-2",1))
    except:
        print("error start thred")
    time.sleep(20)
thread_start()

'''借助工具  进行中下层的优化

    pypy : jit技术 ：缓存hot块 编译后的代码
        上层不变下层优化
    cython：编译为C
        从上到下更改（新语言）
    numba、pandas：扩展和库优化
    
'''














# -*- coding: utf-8 -*-
"""
Spyder Editor
<<机器学习算法原理与编程实践>>郑捷
《numpy学习指南》IVAN idris
This is a script file.using numpy

    2017-5-22 night qs 
"""
import numpy as np
import matplotlib.pyplot as plt

firstone=np.mat('1 2 3;4 5 6;7 8 9')
secondone=np.arange(20)*7      
thirdone=firstone*3
myrand=np.random.rand(3,3)*10
#显示图形
fun=np.poly1d(np.array([1,2,3,4]).astype(float))
x=np.linspace(-10,10,30)
y=fun(x)
plt.plot(x,y)
plt.xlabel('x')
plt.ylabel('y(x)')
plt.show()

#命令行输出
print(np.eye(3))
print('随机初始化矩阵：\n',myrand)
print('转置矩阵：\n',firstone.T)
print('逆矩阵：\n',firstone.I)
print('原矩阵:\n',firstone)
print('secondone:',secondone) 
print('除法：\n',np.divide(thirdone,firstone))
print('模运算：',secondone[1:10]%4,secondone[1:10])
print('求逆：\n',np.linalg.inv(firstone))
print('特征值：\n',np.linalg.eigvals(firstone))
print('特征向量：\n',np.linalg.eig(firstone))
print('秩：\n',np.linalg.matrix_rank(firstone))
#基本运算演示：
myzero=np.zeros([4,5])
myone=np.ones([4,5])
print(myzero,'\n',myone)
print('+ :\n',myzero+myone)
print('-:\n',myzero-myone)
print('加和演示：',sum(myone))
'''
multiply(mutrx1,mutrx2)  矩阵各元素乘积
power(mutrx1,2)  每个元素的二次幂
mutrx1*mutrx2   矩阵相乘
mutrx1.copy（）  矩阵的复制
mutrx1[0] 第一行
mutrx.T[0]  第一列
[m,n]=shape(mutrx1)  m:行数 N：列数

'''


"""
Created on Tue Jun 13 08:39:19 2017

@author: asen

练习使用numpy
"""
import numpy as np
import matplotlib.pyplot as mlt

array1=np.arange(-4,4)
print("余数：\n",np.remainder(array1,2))
print("模：\n",np.mod(array1,2))
print("模%：\n",array1 % 2)
print("fmod:\n",np.fmod(array1,2))

#lissajous curve 利萨如曲线
a=float(5)
b=float(5)
t=np.linspace(-np.pi,np.pi,201)
x=np.sin(a*t+np.pi/2)
y=np.sin(b*t)
mlt.plot(x,y)
mlt.show()
#正太分布
N=10000
normal_val=np.random.normal(size=N)
dummy,bins,dummy=mlt.hist(normal_val,np.sqrt(N),
                          normed=True,lw=1)
sigma=1
mu=0
mlt.plot(bins,1/(sigma*np.sqrt(2*np.pi))
        *np.exp(-(bins-mu)**2/(2*sigma**2)),lw=2)
mlt.show()
#巴特利特窗
window=np.bartlett(10)
mlt.plot(window)
mlt.show()
#汉明窗
window=np.hamming(42)
mlt.plot(window)
mlt.show()
#凯泽窗
window=np.kaiser(42,10)
mlt.plot(window)
mlt.show()
#零阶贝塞尔函数
x=np.linspace(0,4,100)
vals=np.i0(x)
mlt.plot(x,vals)
mlt.show()
#sinc函数
xx=np.outer(x,x)
vals=np.sinc(xx)
mlt.imshow(vals)
mlt.show()
'''
测试：numpy.testing
'''
fig=mlt.figure()
ax=fig.add_subplot(111)
u=np.linspace(-1,1,100)
x,y=np.meshgrid(u,u)
z=x**2+y**2
ax.contourf(x,y,z)
mlt.show()









'''
python cookbook
    qs 7-6/2017
	1、数据结构与算法
	将结果放到容器对象中，再用迭代器遍历（由于惰性计算，占用内存较小）
	python不擅长递归
	ordereddict 有序字典类：双向链表 比普通字典大两倍。
	zip（）函数过的数据只能用一次。
'''
#变量前加*   代表任意个变量的列表lib/collections
items=[0,1,2,3,4,5,6,7,8,8,8,9,9]
*rand_value,last_value=items
print('变量表:',rand_value)
slc_a=slice(2,4)
print(items[slc_a])
items[slc_a]=[12,34]
print(items,slc_a.start,slc_a.stop,slc_a.step)
from collections import Counter#计数器
list_count=Counter(items)
top_three=list_count.most_common(3)
print('高频词:',top_three,list_count[12])
from  operator import itemgetter#提取项
rows=[
	{'name':'qs ','id':20143955},
	{'name':'wang ','id':20139522},
	{'name':'basd ','id':20149527},
	{'name':'asd ','id':20143888},
	{'name':'rty ','id':20143908}]
byname=sorted(rows,key=itemgetter('name'))
byid=sorted(rows,key=itemgetter('id'))
print('两种排序方式:',byname,byid)
print('max and min :',max(rows,key=itemgetter('id')),min(rows,key=itemgetter('id')))
#list comprehension  结果 语句
print([n for n in items if n<8])

'''
	2、字符串 和文本
	用于串连接的 a+= ‘123’ 比 join（）要慢，因为每次操作都会创建新串
	避免创建大型的临时结果
	python的代码递归下降解析器：Grammar/Grammar
	python的ast模块  解析器、编译器令人激动的课题。
	
'''
atemp='hello' 'world'#串连接的另一种形式
print(atemp)
s='{name} is {num} message.'
s=s.format(name='Mr qs',num=10)
print(s) 
s=s.replace('is','has')
print(s)
import re
data=b'foo:asd,qwe'
print(re.split(b'[:,]',data))#字节码形式
print(data.decode('ascii'))
#字节串和python中的其他部分不能很好的兼容，需要手动编解码操作，不要用字节串。
'''3、数字、日期和时间
		decimal模块：允许控制计算过程（金融业务）
		python中的科学和工程类函数库都是以numpy为基础的
		random模块中使用梅森旋转算法计算随机数（算法的原理是什么？）（ctrl+y ctrl+z）
		datetime模块 时间换算。pytz模块：时区问题。
'''
a,b=2.2,1.1
print('a+b=3.3:',a+b==3.3,a+b)
number=31415962 #float can not use
print(bin(number),oct(number),hex(number)) #十进制串表示->n进制字符表示
print(format(number,'b'),format(number,'o'),format(number,'x'))
print(int('101110001100101001',2),int('1a3b4c5d6e7d',16))#n进制字符表示->十进制串表示
import math
a,b,c=float('inf'),float('-inf'),float('nan') #nan not a number 不是数字
print(a,b,c,'math:',math.isinf(a),math.isnan(c))
'''4、迭代器和生成器
	迭代（1、与循环相同）2、next（对象）返回对象中的一个元素（不用for）
	（*很重要**）itertools模块（解决复杂的迭代问题） __iter__() 返回迭代器对象，__next__()完成实际的迭代。
	3、生成器：yield语句生产者，for语句消费者。
	reversed（对象）反向迭代。reversed（对象）反向迭代。zip(a,b)创建迭代器，生成元组对（x,y）
	生成器管道：将数据以管道形式进行处理。
'''
'''5、文件和io 
	with open(path,'rt') as f:  #打开文本文件 rb：二进制 xt:文本文件不存在时才进行写操作。
		print('content',file=f) #输出重定向 写到文本里
		f.read()
		f.write(content)
	gzip和bz2模块读写压缩数据
	mmap模块：实现对文件的内存映射操作。
	os模块：文件系统操作相关。
	sys模块：系统调用。
	tempfile模块。11.6
	pickle模块：将对象化为字节流。禁止载入未知数据。不适用于大型数据结构，不作为长期存储格式。
		构建大型应用 使用pickle时 好好参考官方文档（有坑）
'''
import pickle 
f=open('somepickle','wb')
pickle.dump([1,2,3,4],f)
pickle.dump('asdads三分大赛',f)
f.close()
f=open('somepickle','rb')
print('pickle:',pickle.load(f))#load()函数只能在内部使用	
print('pickle:',pickle.load(f))
f.close()

'''6、数据编码与处理（数据长期存储格式）
	CSV模块：heads data（macrosoft excel的CSV编码规则）       14.1
	json模块：同pickle使用方式相同。编码格式：key假设是字符串。  19.2
	xml:     20.4
	python中表达 行数据 的标准方式是采用元组序列。
	orm：（object-ralational mapper）www.sqlalchemy.org(隐藏底层sql)
	编解码有十六进制数组成的原始字符串：使用binascii模块19.8
	统计时间序列问题：pandas库
'''
import json 
data={'name':'acmqs','price':135}
json_str=json.dumps(data)
data1=json.loads(json_str)
print('jason:',json_str,data1)
with open('data.json','w') as f:
	json.dump(data,f)
with open ('data.json','r') as f:
	data2=json.load(f)
	print('json_file:',data2)
'''7、函数
	*a 表示接收多个值放入元组中 
	**b 位于参数列表最后，表示将接受到的参数放入字典 (a=2,b=4) so 可以无序
	本来 A 调用 B 的，结果 B 又调用了 A 告诉它的信息，也就叫 callback 回调函数（存在一个挂起恢复过程）？？？
'''
y=7
sub=lambda x,y=y:x-y  #只用于函数只有一个 return语句时 通常为表达式语句。
def add(x,y=9):
	return x+y
print('lambda:',sub(10))
print('function:',add(1,1))
'''8、类|对象
	特殊方法 语言参考3.3
	用栈和队列避免递归。
	垃圾回收不支持环状数据结构。
'''
class fatherclass:
	def __init__():
		self.x=0
class subclass(fatherclass):
	def __init__(self):
		super.__init__()
	def show():
		print('类演示函数。')
'''9、元编程（减少重复代码）
	wrapper 包装 decorator装饰
	dis模块 查看字节码
	ast模块：抽象语法树。
'''
from abc import ABCMeta,abstractmethod
class yuan(metaclass=ABCMeta):#元类
	@abstractmethod
	def read():
		pass
	@abstractmethod
	def write():
		pass
'''10、包和模块
	包的组织：每个目录下都有一个__init__.py 文件即可。
	python的模块包和导入机制是整个语言最为复杂的部分之一（pep302，importlib）

11、web
	rest风格接口

12、并发
	actor模式
	unix环境高级编程 stevens and rago

13、系统管理   16
	替代shell脚本实现系统任务自动化处理。
'''
import webbrowser
fanhui=webbrowser.open('http://www.baidu.com')
print('webbrowser的返回值：',fanhui)
'''14、测试、异常、调试
	优化的第一原则是不优化，第二原则是:不要优化那些不重要的部分。
		全局范围的代码要比函数中的代码运行慢
		.操作符耗时 from math import sqrt  替代 import math  math.sqrt()  或者 sqt=math.sqrt
		任何时候使用额外的处理层来包装代码时，代码的运行速度就会变慢。
		python内建的数据类型都是c实现的，因此速度更快
		避免不必要的拷贝（拷贝越少越好）
		最先优化算法的复杂度。
		（保证程序能够正常工作比他运行的更快更重要）
		（pypy的jit技术、numba的编译技术）
'''
'''15、C 扩展
'''
input('wait your enter.....')











#!coding=utf-8
import tkinter as tk

class minist_app(tk.Frame):
	def __init__(self,master=None):
		tk.Frame.__init__(self,master)
		self.grid()
		self.createWidgets()
		self.create_entry()
		
	def createWidgets(self):
		#最后的quit没有括号
		
		self.quitButton=tk.Button(self,text='quit',command=self.quit )
		self.quitButton.grid(row=0,column=0)
		
	def createWidgets1(self):
		#修改init（）函数为：self.grid(sticky=tk.N+tk.S+tk.E+tk.W)
		
		top=self.winfo_toplevel()
		top.rowconfigure(0,weight=1)
		top.columnconfigure(0,weight=1)
		self.rowconfigure(0,weight=1)
		self.columnconfigure(0,weight=1)
		self.quit=tk.Button(self,text='quit',command=self.quit )
		self.quit.grid(row=0,column=0,sticky=tk.SE)
		
	def create_entry(self):
		'''单行文本框
		'''
		self.entry=tk.Entry(self,width=10)
		self.entry.grid(row=0,column=1)
		
		self.radiobutton1=tk.Radiobutton(self,text='夏天悄悄过去')
		self.radiobutton1.invoke()
		self.radiobutton1.grid(row=1,column=0)
		self.radiobutton2=tk.Radiobutton(self,relief=tk.FLAT,text='千里冰封雪飘')
		self.radiobutton2.grid(row=1,column=1)
		
		self.scale=tk.Scale(self,width=20,orient=tk.HORIZONTAL)
		self.scale.grid(row=0,column=2)
		
		self.canvas=tk.Canvas(self,width=400,height=100,scrollregion=(0,0,800,200) )
		#画布的两个轴
		self.canvas.grid(row=3,column=0)
		self.scrollbarx=tk.Scrollbar(self,orient=tk.HORIZONTAL,command=self.canvas.xview)
		self.scrollbarx.grid(row=2,column=0,sticky=tk.E+tk.W)
		self.scrollbary=tk.Scrollbar(self,orient=tk.VERTICAL,command=self.canvas.yview)
		self.scrollbary.grid(row=3,column=1,sticky=tk.S+tk.N)
		self.canvas['xscrollcommand']=self.scrollbarx.set
		self.canvas['yscrollcommand']=self.scrollbary.set
		
		self.spinbox=tk.Spinbox(self)
		self.spinbox.grid(row=1,column=2)
		
		self.text=tk.Text(self,width=100,height=100)
		self.text.grid(row=3,column=2)
		
from tkinter import ttk
class ttk_app(ttk.Frame):
	def __init__(self,master=None):
		'''使用ttk模块
		'''
		ttk.Frame.__init__(self,master,width=200,height=200)
		self.grid()
		
		self.button=tk.Button(self,text='no used')
		self.button.grid(row=0,column=0)
		
		self.checkbutton=ttk.Checkbutton(self,text='你是男的？')
		self.checkbutton.grid(row=0,column=1)
		
		self.combobox=ttk.Combobox(self,width=20,height=10)
		self.combobox.grid(row=0,column=2)
		
		self.entry=ttk.Entry(self,width=20)
		self.entry.grid(row=1,column=2)
		
		self.label=ttk.Label(self,text='哇哈哈哈哈哈')
		self.label.grid(row=1,column=0)
		
		self.menub=ttk.Menubutton(self,text='菜单按钮')
		self.menub.grid(row=1,column=1)
		
		self.notebook=ttk.Notebook(self,width=100,height=100)
		self.notebook.grid(row=2,column=0)
		
		self.panedwindow=ttk.Panedwindow(self,height=20,width=20,orient=tk.HORIZONTAL)
		self.panedwindow.grid(row=2,column=1)  #???/
		
		self.progressbar=ttk.Progressbar(self,length=20,orient=tk.HORIZONTAL)
		self.progressbar.grid(row=2,column=2)
		
		
		
		
		
def windows():
	'''窗口设计
	'''
	app=minist_app()
	app.master.title('最简单的应用')
	app.mainloop()
def windows1():
	'''窗口设计2
	'''
	appttk=ttk_app()
	appttk.master.title('ttk application')
	appttk.mainloop()
	

def getinfo():
	'''获取相关信息
	'''
	print('获取相关信息')
	#print(tkFont.families() ) #没有tkFont
	#tk.PhotoImage(filename)
	s=ttk.Style()
	print(s.theme_names())
	print(s.theme_use() )
	b=ttk.Button(None)
	print(b.winfo_class() )
	t=ttk.Treeview(None)
	print(t.winfo_class() )
	
def cross_platform_GUI():
	'''基于TK的跨平台图形界面开发尝试
			你没做过你就不知道他有多坑，还有坑在那？
			2017-12-17 中午 图书馆
	'''
	#windows()
	windows1()
	getinfo()
	
cross_platform_GUI()
#********************************************************************

#UI开发 tkinter
#框架 Frame，标签 Label，按钮Button，单行文本框Entry
#多行文本框Text，复选框Checkbutton，单选框Radiobutton
#事件驱动event-driven。
#批处理文件 .bat   .pyw(无控制台窗口）

#创建类
from tkinter import *
class app(Frame):
    """a GUI applicaton! """
    def __init__(self,master):
        """框架初始化"""
        super(app,self).__init__(master)
        self.grid()
        self.click_b=0
        self.create_widgets()
    def create_widgets(self):
        """添加组件"""
        #widget（窗口小部件window gadget）
        kuang=Frame(root)#放入框架
        kuang.grid()#关联布局
            #********
        self.anniu=Button(kuang)
        self.anniu["text"]="click:0"
        self.anniu["command"]=self.update_count  #bind
        self.anniu.grid(row=2,column=0,sticky=W)
            #*******
        biao1=Label(kuang,text="hao yun ban ni yi sheng标签")
        #12确定位置34确定占用格数5（NSEW）对齐方式。九宫格布局
        biao1.grid(row=0,column=0,rowspan=1,columnspan=2,sticky=N)
            #********
        anniu1=Button(kuang,text="确定",command=self.reveal)
        anniu1.grid(row=1,column=0,columnspan=1,sticky=W)
            #********
        anniu2=Button(kuang)
        anniu2.grid(row=1,column=1,sticky=W)
        anniu2.configure(text="取消")
            #********
        anniu3=Button(kuang)
        anniu3["text"]="继续"
        anniu3.grid(row=1,column=2,sticky=W)
            #********
        self.dan=Entry(kuang)
        self.dan.grid(row=2,column=1,columnspan=2,sticky=W)
            #********
        self.txt=Text(self,width=35,height=5,wrap=WORD)#wrap 换行方式
        self.txt.grid(row=3,column=0,columnspan=3,sticky=W)
    def update_count(self):
        self.click_b += 1
        self.anniu["text"]="clicks:" + str(self.click_b)
    def reveal(self):
        constent=self.dan.get()
        if constent=="good":
            message="it's a  secret."
        else:
            message="go out!"
        self.dan.delete(0,END)     #0行0列 删完
        self.txt.insert(0.0,message)
        #self.txt.delete(0.0,END)
def test_app():
	#主程序
	root=Tk()#窗体创建
	root.title("this is my first UI in py")
	root.geometry("300x300")

	application=app(root)

	root.mainloop()
#test_app()










#！/home/asen/anaconda3/bin/python3.6
#coding=utf-8
'''
	本文档格式为python3
	当前脚本文档   记录pyqt5 学习
	教程来源:
			http://www.cnblogs.com/archisama/p/5503526.html
'''

#           ++++++++++++++              +++++++++++++++++
import sys
from PyQt5.QtWidgets import QMainWindow,QApplication,QFrame
from PyQt5.QtWidgets import QAction,qApp,QMessageBox,QCheckBox
from PyQt5.QtWidgets import QDesktopWidget,QLabel,QPushButton,QProgressBar
from PyQt5.QtWidgets import QHBoxLayout, QVBoxLayout,QGridLayout
from PyQt5.QtWidgets import QLabel, QLineEdit,QTextEdit, QInputDialog
from PyQt5.QtWidgets import QWidget,QLCDNumber, QSlider,QFileDialog,QComboBox
from PyQt5.QtGui import QIcon,QColor,QPixmap
from PyQt5.QtCore import Qt,pyqtSignal, QObject,QBasicTimer

class communicate(QObject):
	closeapp=pyqtSignal()
	
class main_window(QMainWindow):
	def __init__(self):
		super().__init__()
		self.initUI()
		
	def initUI(self):
		self.base_frame()
		#self.my_event()
		#两种窗口定位方式
		#self.setGeometry(50,50,1000,500)  #@1
		self.resize(1000,500);self.center()    #@2
		self.setWindowTitle('UI前端')
		self.show()
	#--------------------
	def my_event(self):
		#自定义事件
		self.c=communicate()
		self.c.closeapp.connect(self.close)  #关联关闭窗口函数
	def mousePressEvent(self,event):
		self.c.closeapp.emit()
	#---------------------
	def btn_clked(self):
			sender=self.sender()
			self.statusBar().showMessage(sender.text()+'被点击.')
	def adder3(self):
		#事件发送:sender
		button1=QPushButton('button : 1',self)
		button1.move(200,100)
		button2=QPushButton('button : 2',self)
		button2.move(200,200)
		button1.clicked.connect(self.btn_clked)
		button2.clicked.connect(self.btn_clked)
		self.statusBar()
	#-------------------------
	def base_frame(self):
		#基本的窗口框架
		exitaction=QAction(QIcon('exit.png'),'exit',self )
		exitaction.setShortcut('Ctrl+Q')
		exitaction.setStatusTip('exit app')
		exitaction.triggered.connect(qApp.quit)
		openfile=QAction(QIcon('open.png'),'open',self)
		openfile.setShortcut('Ctrl+o')
		openfile.triggered.connect(self.showdialog)
		
		self.textedit=QTextEdit()
		self.setCentralWidget(self.textedit)
		
		self.statusBar().showMessage('状态栏')  #状态栏为什么得在菜单栏前面?
		#菜单栏使用格式
		mbar=self.menuBar()
		filem=mbar.addMenu('文件')
		filem.addAction(openfile)
		filem.addAction(exitaction)
		
		viewm=mbar.addMenu('视图')
		taskm=mbar.addMenu('任务')
		aboutm=mbar.addMenu('关于')
		#工具条
		self.toolbar=self.addToolBar('Exit')
		self.toolbar.addAction(exitaction)
	def showdialog(self):
		#打开文件对话框
		fname=QFileDialog.getOpenFileName(self,'打开文件','/home/asen/')
		if fname[0]:
			file=open(fname[0],'r')
			with file:
				data=file.read()
				self.textedit.setText(data)
	#---------------------
	def closeEvent(self,event):
		#不能更改函数名的大小写,应该是预定义接口
		reply=QMessageBox.question(self,'确认窗口','确认退出本应用?',
				QMessageBox.Yes | QMessageBox.No,QMessageBox.No)
		if reply == QMessageBox.Yes:
			event.accept()
		else:
			event.ignore()

	def center(self):
		#将窗口移动到桌面的中央
		qr =self.frameGeometry()
		cp =QDesktopWidget().availableGeometry().center()
		qr.moveCenter(cp)
		self.move(qr.topLeft() )

#         +++++++++++++++                 ++++++++++++++++           ++++++

class layout_used(QWidget):
	#布局类
	def __init__(self):
		super().__init__()
		self.initUI()

	def initUI(self):
		self.setGeometry(50,50,1000,500)
		#self.mygrid_layout()
		#self.mybox_layout()
		self.absolute_layout()
		
		self.setWindowTitle('使用布局')
		self.show()

	def absolute_layout(self):
		#绝对布局
		lb1=QLabel('标签1',self)
		lb1.move(200,200)   # 横轴x,纵轴y
		lb2=QLabel('标签二',self)
		lb2.move(400,300)
		lb3=QLabel('标签3',self)
		lb3.move(400,400)
	def mybox_layout(self):
		#箱式布局  无法显示?
		ok=QPushButton('好的')
		cancel=QPushButton('取消')
		hbox=QHBoxLayout()
		hbox.addStretch(1)
		hbox.addWidget(ok)
		hbox.addWidget(cancel)
		vbox=QVBoxLayout()
		vbox.addStretch(1)
		vbox.addLayout(hbox)
		self.setLayout(vbox)
	def mygrid_layout(self):
		#网格布局:计算器的UI 界面
		grid=QGridLayout()
		self.setLayout(grid)
		names=['clear','back','','quit',
							'7','8','9','/',
							'4','5','6','*',
							'1','2','3','-',
							'0','.','=','+']
		position=[(i,j) for i in range(5) for j in range(4)]
		for p,name in zip(position,names):
			if name == '':
				continue
			btn=QPushButton(name)
			grid.addWidget(btn,*p)

#         +++++++++++++++                 ++++++++++++++++           ++++++

class ponent(QWidget):
	#组件类
	def __init__(self):
		super().__init__()
		self.initUI()
	def initUI(self):
		#self.dialog_used()
		#self.window_get()
		#self.pbar()
		self.xiala()
		self.setGeometry(300, 300, 350, 300)
		self.setWindowTitle('使用组件')   
		self.show()
		
	def xiala(self):
		#下拉列表
		self.lbl = QLabel("Ubuntu", self)
		combo = QComboBox(self)
		combo.addItem("Ubuntu")
		combo.addItem("Mandriva")
		combo.addItem("Fedora")
		combo.addItem("Arch")
		combo.addItem("Gentoo")
		combo.move(50, 50)
		self.lbl.move(50, 150)
		combo.activated[str].connect(self.onactive)
	def onactive(self,text):
		self.lbl.setText(text)
		self.lbl.adjustSize()

	def pbar(self):
		self.pb=QProgressBar(self)
		self.pb.setGeometry(30,40,200,25)
		self.btn = QPushButton('开始', self)
		self.btn.move(40, 80)
		self.btn.clicked.connect(self.doaction)
		self.timer = QBasicTimer()
		self.step=0
	def timerEvent(self,e):
		self.step+=1
		self.pb.setValue(self.step)
		if self.step>=100:
			self.timer.stop()
			self.btn.setText('结束')
	def doaction(self):
		if self.timer.isActive():
			self.btn.setText('开始')
		else:
			self.timer.start(100,self)
			self.btn.setText('结束')

	def window_get(self):
		#小组件
		self.col=QColor(0,0,0)
		self.redbtn=QPushButton('red', self)
		self.redbtn.setCheckable(True)
		self.redbtn.clicked[bool].connect(self.setColor)
		self.greenbtn=QPushButton('green', self)
		self.greenbtn.setCheckable(True)
		self.greenbtn.clicked[bool].connect(self.setColor)
		self.bluebtn=QPushButton('blue', self)
		self.bluebtn.setCheckable(True)
		self.bluebtn.clicked[bool].connect(self.setColor)
		self.square=QFrame(self)
		self.square.setGeometry(50, 20, 100, 100)  #未显示
		self.square.setStyleSheet('qwidget {background-color: %s}' %self.col.name() )
		grid = QGridLayout()
		grid.setSpacing(6)
		grid.addWidget(self.redbtn, 1, 0)
		grid.addWidget(self.greenbtn,1,1)
		grid.addWidget(self.bluebtn,1,2)
		grid.addWidget(self.square,2,0,2,2)
		self.setLayout(grid) 
	def setColor(self,pressed):
		source=self.sender()
		if pressed:
			val=255
		else:
			val=0
		if source.text() == 'red':
			self.col.setRed(val)
		elif source.text() =='green':
			self.col.setGreen(val)
		elif source.text() =='blue':
			self.col.setBlue(val)
		else:
			print('error  at setColor() .')
		self.square.setStyleSheet('qframe {background %s}'%self.col.name() )
		
	def dialog_used(self):
		#对话框,小组件
		self.btn=QPushButton('Dislog',self)
		self.btn.clicked.connect(self.showDialog)
		self.line=QLineEdit(self)
		self.cbox=QCheckBox('作业写完了没?',self)
		self.cbox.stateChanged.connect(self.changeTitle)
		vbox=QVBoxLayout()
		vbox.addWidget(self.line)
		vbox.addWidget(self.btn)
		vbox.addWidget(self.cbox)
		self.setLayout(vbox)
	def showDialog(self):
		text,ok=QInputDialog.getText(self,'输入对话框','请输入内容:')
		if ok:
			self.line.setText(str(text))
	def  changeTitle(self,state):
		if state == Qt.Checked:
			self.setWindowTitle('做完了!!!')
		else:
			self.setWindowTitle('没做完!!!')
	
	def adder2(self):
		lcd=QLCDNumber(self)  #数字显示
		sld=QSlider(Qt.Horizontal,self)  #滑块
		vbox=QVBoxLayout()
		vbox.addWidget(lcd)
		vbox.addWidget(sld)
		self.setLayout(vbox)
		sld.valueChanged.connect(lcd.display)
		#滑块条的valueChanged信号和lcd数字显示的display槽
		
	def keyPressEvent(self,e):
		#重写了esc按键的事件处理功能
		if e.key() == Qt.Key_Escape:
			self.close()
		
	def adder(self):
		#添加组件,标签,单行文本,多行文本 [只能输入英文]
		title = QLabel('题目')
		author = QLabel('作者')
		review = QLabel('文章')
		titleEdit = QLineEdit()
		authorEdit = QLineEdit()
		reviewEdit = QTextEdit()
 
		grid = QGridLayout()
		grid.setSpacing(10)
 
		grid.addWidget(title, 1, 0)
		grid.addWidget(titleEdit, 1, 1)
		grid.addWidget(author, 2, 0)
		grid.addWidget(authorEdit, 2, 1)
 
		grid.addWidget(review, 3, 0)
		grid.addWidget(reviewEdit, 3, 1, 5, 1)
		self.setLayout(grid) 



#         +++++++++++++++                 ++++++++++++++++           ++++++

def main():
	'''  函数文档 '''
	app=QApplication(sys.argv)
	#ex=main_window()
	#ex=layout_used()
	ex=ponent()
	sys.exit(app.exec_() )


if __name__ =='__main__':
	''' 主函数文档'''
	main()
	print('主函数执行结束。')
'''
事件源是状态发生改变的对象。它产生了事件。
事件对象(evnet)封装了事件源中的状态变化。
事件目标是想要被通知的对象
	信号和槽用于对象之间的通信
	
'''











import matplotlib.pyplot as plt
import math
def tanh_show():
	x=[i/1000 for i in range(-1000,1000,1)]
	y=[math.tanh(i) for i in x]
	plt.plot(x,y)
	plt.show()
	
tanh_show()

def relu_show():
	#画出Relu函数
	x=[i/1000 for i in range(-1000,1000,1)]
	def relu(x):
		if x<=0:
			return 0
		else:
			return x
	y=[relu(i) for i in x]
	plt.plot(x,y)
	plt.show()
	
#relu_show()










import nltk
def use_nltk():
    ''' 练习使用NLTK'''
    sentance="this is a question that i know."
            
    takens=nltk.word_tokenize(sentance)
    print('分词结果：',takens)
    
    tag=nltk.pos_tag(takens)
    print('标注结果：',tag[0:10])
    
    entity=nltk.chunk.ne_chunk(tag)
    print('命名实体：',entity)
	
from random import random
def get_random():
    ''' 随机数'''
    print("suijishu:",random())
    
def unicode():
    ''' how to unicode doc
    '''
    word_table=[]
    for num in range(173900):  #1114111
        #print(num,':',chr(num),end=' ')
        word_table.append((num,chr(num)) )
        #break
        
    #存入文件
    with open('unicode_table.txt','w+',encoding='utf-8') as unicode:
        for num in range(173900):
            unicode.write(str(word_table[num-1])+'\t' )
            if num%10== 0:
                unicode.write('\n')
    print(ord('𠣕') )  #验证133333
    
    
def 事物():
    '''
    '''
    print('事物（） 被调用。')
    print(divmod(7,3) )  #(2,1)
	
#import os
#print(os.path.exists('/usr/bin'))
#use_nltk()
#get_random()
#unicode()
#unicode1()
#事物()










'''
    目的：获取相关模块的 帮助信息 并保存到文件中。
    时间：2017-9-5 10:33-11:12
    作者：琼生
'''
import os
import sys
def getdocument1():
    modulename=['os','io','string','re']
    savestdout=sys.stdout
    with open('docu1.txt','w') as file:
        sys.stdout=file
        for module in modulename:
            sentence='(sentence)
            sentenceimport '+module
            #os.system
            print(help(module))
            
    sys.stdout=savestdout
    print('over  this.')
#getdocument()
#第一版：将相关信息全部输入到一个文件里
#确实是按行解释执行。例如 ：16行sentence
#----------------------------------------------
def getdocument2():
    modulename=['os','io','string','re']
    savestdout=sys.stdout
    for module in modulename:
        sentence='import '+module
        sentence    #导入模块   
        filename='doc_'+module+'.txt'
        with open(filename,'w') as file:
            sys.stdout=file
            print(help(module))
    sys.stdout=savestdout
    print('over  this.')
#getdocument2()
#第二版：将每个模块的信息写到不同的文件中
#-----------------------------------------------
def getdocument3(name):
    sentence='import '+name
    sentence    
    filename='doc_'+name+'.txt'
    savestdout=sys.stdout
    with open(filename,'w') as file:
        sys.stdout=file
        print(help(name))
    sys.stdout=savestdout
    print('over  this.')
#getdocument3('re')
#第三版：特定模块文档的生成（函数调用）
#-----------------------------------------------
def getdocument4():
    name=input("请输入模块名：")
    sentence='import '+name
    sentence    
    filename='doc_'+name+'.txt'
    with open(filename,'w') as file:
        savestdout=sys.stdout
        sys.stdout=file
        print(help(name))
        sys.stdout=savestdout
    print('over  this.')
getdocument4()
#第四版：有命令行输入，要查询生成文档的模块
#前几版都是文件流重定向
#------------------------------------------------











#学习使用platform 模块

import platform as plt
print( plt.architecture() )  #位数
print(plt.machine() )  #cpu架构
print( plt.node() )  #机器名
print( plt.platform()  )  #全名
print( plt.processor() ) #处理器架构
print( plt.python_build()  ) 
print( plt.python_compiler() )
print('branch:', plt.python_branch() )
print( plt.python_implementation() )
print( plt.python_version() )   #版本号
print('SCM revision : ', plt.python_revision()  )
print( plt.python_version_tuple()  )
print( plt.release()  )
print( plt.system() ) #平台名称
#print( plt.system_alias() )  system version release 的重命名
print( plt.version() )  #系统版本号
print( plt.uname()  )  #包含了上面的全部信息
print( plt.dist() )  #linux类型 debian red hat
print( plt.linux_distribution() )
print( plt.libc_ver() )  #c库 及其版本


import random as rd
print( rd.seed(100) )  #设置随机数种子
#print( rd.getstate() ) 
print(rd.getrandbits(18) )  #设置随机数位数
print( rd.randrange(1,9,2) )
print( rd.randint(10,20))  #10-20之间的随机数
print(rd.choice([1,3,5,7,9]) )
print( rd.choices([1,2,3,5,7,9],k=3) )
asd=[1,3,5,7,9]
print( 'shuffle:',rd.shuffle(asd) ,asd )
print(rd.sample([1,2,3,5,7,8],k=4) )

print(rd.random() ) #0-1之间的随机数
print(rd.uniform(100,200) )
print(rd.triangular(100,200) )  #对称分布
print( rd.betavariate(3,4)  )  #beta分布
print('exp:', rd.expovariate(3) )  #exp分布
print(rd.gammavariate(2,3) )#gamma分布
print('gauss', rd.gauss(5,2) )  #高斯分布
print( rd.lognormvariate(5,2) )
print('normal:', rd.normalvariate(5,2)  )
print( rd.vonmisesvariate(5,2)  )
print('pareto:', rd.paretovariate(5) )
print( rd.weibullvariate(5,2)  )




#统计模块
import statistics as stt
asd=[1.2,2.3,3.1,3.1,3.1,5.4,6.2,7.3,8.5,9.6]
print('mean:',stt.mean(asd) )
print( '调和均值：',stt.harmonic_mean(asd) )
print('中位数：',stt.median(asd))
print( '小中位',stt.median_low(asd) )
print('大中位数：',stt.median_high(asd))
print('',stt.median_grouped(asd) )  #如何计算的?
print('众数：', stt.mode(asd) )
print('总体标准差：',stt.pstdev(asd) )
print('总体方差：',stt.pvariance(asd) )
print('标准差：',stt.stdev(asd) )
print('方差：',stt.variance(asd) )





from  multiprocessing  import Process,Lock
#标准库中d例子
def func(lock,i):
    lock.acquire()
    try:
        print('该进程号为：',i)
    finally:
        lock.release()
if __name__ =='__main__':
    lk=Lock()
    for num in range(10):
        Process(target=func,args=(lk,num)).start()
        
        
        
from  multiprocessing  import Process,Value,Array,Manager
def fun(num,arr):
    #多进程中的多个值传递
    num.Value=3.14
    for i in arr:
        arr[i]=i+3
def fun1(dict1,list1):
    dict1['bai']=123
    list1.reverse()
        
with Manager() as  manager:
    num=Value('d',0.0001)  #double类型
    arr=Array('i',range(10))  #int型
    dict1=manager.dict()
    list1=manager.list(range(10) )

    #p=Process(target=fun,args=(num,arr))
    p=Process(target=fun1,args=(dict1,list1) )
    p.start()
    p.join()
    
    print(num.value)
    print(arr[:])
    
    print(dict1)
    print(list1)
    
    
from  multiprocessing  import Pool

def add2(x):
    for i in range(100):
        x=x*(i+1)
    for i in range(100):
        x=x/(i+1)
    return x+2

if __name__ =='__main__':
    p=Pool(5)
    result=p.map(add2,range(1000000)) #可以看到cpu占用100%
    print(result[:10])
    
    
import matplotlib.pyplot as plt
plt.plot([1,2,3,4,5,6,7,8,9,10,11],[6.1,-5,9,3,2,6,3,7,5,1,2])
plt.show()







'''  ***********************
	content:re 正则表达式
	auther:qs
	TIME:2017-6-13/22.31    7-11下午
    ***********************'''
import re
import os
def regex():
    '常用函数。'
    line='asdf,fjkl; afed,fghjk;'
    line=re.split(r'[;,\s]\s*',line)
    print(line)
    filename='book.txt'
    url='http:www.python.org'
    print(filename.endswith('.txt'))
    print(url.startswith('http:'))
    filename=os.listdir('.')
    print(filename)
    choice=('http','ftp')  #必须是元组
    url='http://www.python.org'
    print(url.startswith(choice))

#*******************随机选择************************************************

import random
seq=['qwe','asd','zcx']  #可以是列表 字符串
seq1='我是中国人'
print(random.choice(seq))
print(random.choice(seq1))
#************串格式化*****************************
from string import Template
s=Template('$w likes $y.')
s=s.substitute(w='qs',y='mimi')
print('替换后的子串：',s)
d=dict(who='tim')
print(Template('$who libke $what.').safe_substitute(d))
input('enter over！');

'''***************************************
global and nonlocal
python变量的访问顺序：
		当前作用域局部变量
		外部作用域变量
		全剧变量
		python内置变量
'''
_val=2  #全局变量
def value_test():
	#区分global和nonlocal关键字的不同
	
	global _val   # 不改变_val的值，无需global声明
	_val+=1
	print('全局变量_val：',_val)
	
	_v1=3
	def 非本地():
		#nonlocal用来访问外部变量而不是全局变量
		nonlocal _v1  #不改变变量的值 ，无需声明
		_v1+=1
		print('非本地变量_v1：',_v1)
	非本地()
	#两者的报错相同 ：unboundlocalerror
	
#value_test()


def test():
	#	整数除法保留两位
	#方式1：显示装换
	value=round(float(123/34),2)  #保留三位有效数字
	
	#方法2：导入函数
	#from __future__ import division  在文件的开头
	value1=round(123/34,3)
	print(value,value1)
	
	#
	#扫一遍遗漏的知识点 
	# 教程地址：
	#http://www.cnblogs.com/vamei/archive/2012/09/13/2682778.html
	#
	
	#乘方操作符  **
	print('3 的平方是：',3**2)
	
	#获取对象 类型
	aaa=100
	if type(aaa) is int :
		print('aaa的类型是int')
	else:
		print('aaa的类型不是int')
	
	#self代表了根据类定义而创建的对象。
	#参数传递的方式：打包传递
	def func(*name):
		print(type(name) )
		print(name)
	func(1,2,3)
	func('ok',aaa,4,5,76)
	
	#非迭代器的循环使用方式：
	S='aslkdjflks;'
	for i in range(0,len(S),1):
		print(S[i],end='_')
		
	#zip不止合并，还有分解
	ta=[1,2,3]
	tb=[4,5,6]
	ziped=zip(ta,tb)
	print('合并后：',ziped )  
	#为什么用列表推导之后便无法分解？修改了对象？
	#print([i for i in ziped])
	na,nb=zip(*ziped)
	print('分解后：',na,nb)
	
	#使用异常处理
	ra=range(5)
	try:
		for i in range(10):
			print(ra[i] )
	except IndexError:
		print("已经捕获异常。")
	except Exception:
		print('发生异常。')
	else:
		print('没有发生异常。')
	finally:
		print('收尾。')
	
	#面向对象的体现
	print(abs(-2),(-2).__abs__())
	#任何一个有__call__()特殊方法的对象都被当作是函数
	class simple(object):
		def __call__(self,a):  #python中的特殊方法
			print(a+5)
	add5=simple()
	add5(5)
	#list(map(add5,[1,2,3]))  #没有list则不会显示结果
	
	#闭包二探
	state=3  #3是一个对象 state是一个引用
	#右值对象，左值引用。
	def change_state():
		nonlocal state
		print(state + 3)
	change_state()
	change_state()
	
	print(0b1110,0o10,0x2a)  #二进制，八进制，十六进制
	
	#enumerate ：添加索引给对象
	asd=enumerate([2,3,4,5,6,7,8,9,0])
	for index,value in asd:
		print(index,value)
		
	exec("print('执行命令。')")  #双引号
	#模块只导入一次： 多个import this 只执行第一次导入
	#模块在导入的过程中 解释性执行一遍
	
	
	#0填充，总长度6位，小数点后三位
	print("字符串格式化：%06.3f"%3.1415926) 

#----------------------------------------

import pdb
def test_pdb():
	# python的整数区间上下限[-5,256]（linux下）
	#本机windows为：[-5,...] 使用同一个引用
	x=-5
	y=-5
	print(-5,x is y)
	
	#pdb.set_trace()
	
	x=256
	y=256
	print(256,x is y)
	x=-6
	y=-6
	print(-6,x is y)
	x=1099511627776000000111
	y=1099511627776000000111
	print("[2^50+]:",x is y)

#-----------------------------------------

from __future__ import division  #57行
import lib_try1
def test_fanshe():
	#python中的反射
	#需要import 导入的版本
	
	must_str=input("请输入函数名：").strip()
	if hasattr(lib_try1,must_str):
		func=getattr(lib_try1,must_str)  #参数1 必须是模块名字
		func()
		print("反射验证函数结束！")
	else:
		print("未找到 需要调用的函数。")
		
	#python的反射，它的核心本质其实就是利用字符串的形式去对象（模块）
	#	中操作（查找/获取/删除/添加）成员，一种基于字符串的事件驱动！
	
	#无需import 导入的版本
	inp=input("请输入路径：").strip()
	modules, func = inp.split("/")
	obj = __import__("hello."+modules,fromlist=True)  #使用文件夹
	if hasattr(obj, func):
		func = getattr(obj, func)
		func()
	else:
		print("404")
		
#test_fanshe()

#突然发现在作选择的时候好像就有两种方式：
#		1、基于规则的，按条件查找（逐步缩小范围）
#		2、基于枚举的，比较筛选（和集合中的元素逐一比对）

import time
def runtime(function):
	#对相关函数的运行时间进行测试 @runtime
	def wrapper(*args,**kwargs):
		start=time.time()
		result=function(*args,**kwargs)
		end=time.time()
		print(function.__name__,end-start,'s')
		return result
	return wrapper
	
@runtime
def waste():
	result=0
	for i in range(5000):
		for k in range(10000):
			result=i*(k-i)+k
	print(result)
	return result
waste()





def kwargv(myname,*var,**kvar):
	''' *args: 代表可以传入任一参数
		**kwargs:代表 key=args;任意个命名的参数

	'''
	for k,v in kvar.items():
		print(k,' is ',v)
	for val in var:
		print(val)
	print('第一个参数',myname)
	
	
#kwargv('asd',13,True,name='asd',age=13,sex=True)


def return_global(x,y):
	global result,start,end
	start=x+1
	end=y+1
	result=x+y
	#return result   #return 把函数中的值返回给调用的函数
return_global(789,357),print( result,start,end)

def mutable(num,target=[]):
	#参数中的变量是全局变量
	target.append(num)
	return target
def no_used_mutable_type():
	for i in range(10):
		print(i,mutable(i))


#推导式
from pprint import pprint
def tuidaoshi():
	#列表推导
	print([i for i in range(-10,10) if i>0])
	#元组推导
	qwe=(i for i in range(-10,10) if i<0)
	print('生成器对象的第一个值:',next(qwe))  #生成器对像
	#字典推导
	asd={1:4,2:8,3:7,5:6}
	print({v:k for k,v in asd.items()})
	#集合推导
	pprint({val for val in asd.keys()})

#tuidaoshi()
number=0
def get_number():
	global number
	while number<10:
		number+=1 
		yield number
def coroutine_used():
	start=0
	while start <=10:
		start=yield
		print(start)

def example():
	cu=coroutine_used()
	next(cu)
	for i in get_number():
		cu.send(i)

example()


'''
	1 short script debug order : python -m pdb name.py  
		从头开始逐行调试
	2 import pdb  pdb.set_trace()
		c: continue
		w: show context
		a:  parameter 展示参数
		s: step  in function inside
		n: next line in script
	
	@wraps  : 阻止装饰器 修改原函数的名字和docstring
		使用装饰器的场景:入口检查.日志

	自省 introspection:
		运行时判断一个对象的类型的能力
		dir(obj)      type(obj)    id(obj)
		inspect module
	with 使用自定义对象:
		自定义对象中定义 __enter__() 、__exit__()

'''
#python 备忘：星号线前摘自python cookbook：函数
#把函数当类使，速度快一点
import sys
class closeinst:
	def __init__(self,locals=None):
		if locals is None:
			locals=sys._getframe(1).f_locals
		self.__dict__.update(\
			(key,value) for key,value in locals.items() if callable(value))
	def __len__(self):
		return self.__dict__['len']()
		
def stack():
	s=[]
	def put(a):
		s.append(a)
	def get():
		a=s[0]
		s=s[1:]
		return a
	def lens():
		print( len(s))
	return closeinst()
s1=stack()
s1.put(2)
s1.put(5)
s1.put(3)
s1.lens()




#偏特化
def spam(a,b,c,d):
	print(a,b,c,d)
from functools import partial
s1=partial(spam,1,2)
s1(3,4)



#是什么定义了元组（）？  ，？
a=()
d=(1)  #int  ??
b=(1,2)
c=1,2
print(a,b,c,d,type(a),type(b),type(c),type(d))


#运行时绑定
funcs1=[lambda x:x+n for n in range(5)]
#定义时绑定
funcs2=[lambda x,n=n:x+n for n in range(5)]
for f in funcs1:
	print(f(2),end='_')
for f in funcs2:
	print(f(0),end='-')
print()
	
	
	
#默认参数的绑定时间
x=32
def sample(a,b=x):
	print(a,b)
sample(4)
x=23
sample(4)
print('两个数相同时为编译时绑定，不同时为运行时绑定')



#强类型化一个函数：
#	只是方便阅读并没有类型检查等作用
def addtyped(x:int,y:int)->int:
	return x+y
	
one=addtyped(3,4)
two=addtyped('asd','123')
print(one,two,'two正常输出可见没有类型检查')
print(addtyped.__annotations__)
#************************************

def  chose1from3(max_num):
    '''输入数列的最大值：max_num。生成0-n的 序列。
		每隔两个删一个，
		输出最后删除的元素的下标。
	'''
    using_list=list(range(max_num))
    print(using_list)
    #关键在于结束条件
    count,two_limit,count_del=0,0,0
    while True:
        #计数
        if using_list[count] ==-1:  #标记已经删除的值
            pass
        else:
            two_limit+=1
            #删除指定值
            if two_limit==3:#计数初始化 切记！！
                two_limit=0
                using_list[count] =-1
                count_del+=1
                #print(count,using_list) #展示删除过程
        #结束条件，输出结果
        if count_del==len(using_list):
            #return count
            print(count)
            break
        #循环计数
        if count==(len(using_list)-1):
            count=-1  #解决与循环控制的冲突
        #控制循环
        count+=1

#**********************************
#**********************************

import sys
def use_its():
	#python优化
	print('数据结构所占内存大小：字节')
	print('整数', sys.getsizeof(1) )
	print('列表', sys.getsizeof([]))
	print('字典', sys.getsizeof({}))
	print('元组', sys.getsizeof(()))
	print('真值', sys.getsizeof(True))
	print('空串',sys.getsizeof(''))
	#python的易用性带来的 内存开销还真是大！！！
	#解释执行：类型在运行时判断执行。
	#一切皆对象：每一个对象都要引用计数
	#GIL：因为python每执行100个opcode（sys.setcheckinterval()设置）就会尝试线程的切换
	#垃圾回收的时候会有卡顿 （禁用）
	#函数默认参数是在定义的时候求值，而不是调用的时候
	#闭包中用到的变量的值，是在内部函数被调用时查询得到的
	#对象定义了__del__,那么python gc不能进行回收
if  __name__=='__main__':
	#chose1from3(34)
	#test()
	#use_its()
	
	'''
	17-5-13 library
	1、文件可能多了一行（文件末尾有\n） 从而 按行读的时候
		产生一项长度为0的数据。从而访问出错。
	2、找不到错的时候，可以给别人讲讲（尤其是头脑发热、不清楚的时候）
	'''










