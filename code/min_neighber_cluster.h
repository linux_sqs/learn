//
//	随机数生成
//
template<typename T>
T rand_get()
{
	if (typeid(T)==typeid(int)){
		return rand();
	}
	else if(typeid(T)==typeid(float)){
		return rand()*0.1;
	}
	else if (typeid(T) == typeid(double)){
		return rand()*0.001;
	}
}
//
//	生成特征向量
//
template<typename T>
void tenD_generator(T* number,int size)
{
	for (int i = 0; i < size; i++){
		number[i] = rand_get<T>();
	}
}
//
//	显示特征数组
//
template<typename T>
void show_table(T* data_feature,int row,int column)
{
	for (int i = 0; i < row; i++){
		for (int j = 0; j < column; j++){
			std::cout << data_feature[i][j] << '\t';
		}
		std::cout << std::endl;
	}
}
//
//	特征矩阵初始化
//
template<typename T>
void add_value2array(T data_feature[][8], int row, int column)//如何去掉两个10？调用的时候强转(T**)
{
	for (int i = 0; i < row; i++){
		srand(i);//得到不同的随机数序列(只要两次运行的间隔超过1秒    。。。。
		tenD_generator<T>(data_feature[i], column);
	}
}
//
//	最近邻分类器
//		20,20,10:1类、2类、测试集
template<typename T>
double sub_2line(T test_line, T data,int column)
{
	double sum_sub = 0;
	for (int i = 0; i < column;i++){
		sum_sub += abs(test_line[i] - data[i]);
	}
	return sum_sub;
}
double min_arrays(double* sum_sub,int length)
{
	double min = sum_sub[0];
	for (int i = 1; i < length;i++){
		min=(min < sum_sub[i]) ? min : sum_sub[i];
	}
	return min;
}
int find_position(double min, double* sum_sub, int length)
{
	int i = 0;
	for (; i < length; i++){
		if (min == sum_sub[i])break;
	}
	if ((i < length) && (i >= 0)){
		return i;
	}
	else{
		std::cerr <<min<< " 最小值未找到。\n";
		return -1;
	}
}
void show1array(int* array,int length)
{
	for (int i = 0; i < length; i++){
		std::cout << array[i] << ",";
	}
	std::cout << std::endl;
}
template<typename T>
void min_neighber_cluster(T data_feature[][8], int row, int column)
{
	T test_line[10];//测试样本
	double sum_sub[40];
	int result[10];
	for (int i = (row-10); i < row;i++){
		//获得测试样本
		for (int j = 0; j < column;j++){
			test_line[j] = data_feature[i][j];
		}
		for (int k = 0; k < (row - 10);k++){
			//计算最小距离
			sum_sub[k] = sub_2line(test_line, data_feature[k], column);
		}
		//找到最小值
		double mini_num=min_arrays(sum_sub,(row-10));
		//std::cout << mini_num << ' ';
		//找到最小值的位值
		int position = find_position(mini_num, sum_sub, (row - 10));
		//存储分类结果
		result[i-40] = (position < 20) ? 1 : 0;
	}
	//输出分类结果
	show1array(result,10);
}