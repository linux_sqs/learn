## 本页用于程序设计相关的内容整理
---
名称|程序设计语言|备注
:-|:-|:-
[快速排序](/code/quicksort.cpp)|C++|三个版本对比
[C库note](/code/C_lib.h)|C|一些库函数|
[keras,sklearn](/code/keras.py)|python3|CNN,自编码器,SVM|
[C#](/code/Csharp.cs)|C#|C#的尝试|
[C基础:1](/code/c1.cpp)|C|vec,list,enum,char,s|
[C++基础](/code/fundation_of_cpp.h)|C++|文件,宏,递归,|
[C++入门](/code/fundation.c++)|C++|come_from_net
[C++线程了解](/code/多线程.cpp)|C++|
[Function programming](/code/FP.py)|python3|python函数式编程基础|
[遗传算法](/code/heredity.cpp)|C++|相关结果图片(下文)|
[javascript](/code/learn.js)|javascript|js基础|
[python3 lib](/code/lib_try.py)|python3|python库|
[python](/code/some_func.py)|python3|蒙特卡洛pi,utf-8编码,简单加密,巴塞尔问题,分词,数据生成
[最近邻分类器](/code/min_neighber_cluster.h)|C++|
[单链表](/code/single_cycle_list2.h)|C++|
[word2vec](/code/word2vec.c)|C|google的原版C代码
[下载股市信息](/code/download_sh.py)|python|耗时6s左右
[](/code/)||
[](/code/)||
[](/code/)||
[](/code/)||
[](/code/)||


工具链接|工具名称|备注
:-|:-|:-
[Docker](/code/docker.txt)|Docker|
[GDB](/code/gdb_learn.txt)|Gnu debuger|
[unicode](/code/unicode_table.txt)|UTF-8编码表|本文件的格式为:(数值,字符)
[](/code/)||
[](/code/)||
[](/code/)||
[](/code/)||




---

##### 机器生成的数据
```julia
20144268,孟利海,89,52,96,65,82,
20145473,沈建志,43,69,85,61,55,
20145119,赵胜静,39,80,62,51,79,
20143464,孟建瑞,97,60,68,94,99,
20143433,马宁磊,71,63,69,47,52,
20141996,姜胜艳,98,73,39,45,77,
20146333,高梅芳,60,66,92,97,94,
20144351,邵金娜,51,37,55,85,66,
20144561,罗宁宝,87,71,45,63,64,
20149662,魏兴佳,87,89,90,85,64,
20144041,白新飞,68,73,61,90,64,
20141466,施燕超,90,87,82,37,79,
20147242,邵国斌,81,91,89,63,96,
20140621,王琳凤,88,74,64,85,46,
20146149,罗斌祥,94,59,52,51,37,
20144762,李君成,62,66,49,96,77,
20148679,吴莉琳,75,96,49,68,71,
```
##### 遗传算法的结果
![](/code/pict/3k.JPG)
![](/code/pict/10k.JPG)
![](/code/pict/169k.JPG)
![](/code/pict/10m.JPG)


##### 