//  Copyright 2013 Google Inc. All Rights Reserved.
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>

// 文件名的最大长度
#define MAX_STRING 100 
// ?
#define EXP_TABLE_SIZE 1000
// ?
#define MAX_EXP 6
// 最长句子长度
#define MAX_SENTENCE_LENGTH 1000
// 最长霍夫曼编码长度
#define MAX_CODE_LENGTH 40

// 哈希，线性探测，开放定址法，装填系数0.7
const int vocab_hash_size = 30000000;  // Maximum 30 * 0.7 = 21M words in the vocabulary

typedef float real;                    // Precision of float numbers

struct vocab_word 
{
  long long cn; // 单词词频
  int *point; // 霍夫曼树中从根节点到该词的路径，存放路径上每个非叶结点的索引
  char *word, *code, codelen; // 词本身，霍夫曼编码，霍夫曼编码长度
};

// 训练文件路径，输出文件路径
char train_file[MAX_STRING], output_file[MAX_STRING];
// 词表输出文件路径，词表读入文件路径
char save_vocab_file[MAX_STRING], read_vocab_file[MAX_STRING];
// 词表指针
struct vocab_word *vocab;
// 默认参数设置：
// binary = 0，使用文本保存
// cbow = 1，使用cbow模型
// debug_mode = 2，调试模式
// window = 5，滑动窗口大小为5? 在cbow中表示了word vector的最大的sum范围，在skip-gram中表示了max space between words（w1,w2,p(w1 | w2)）
// min_count = 5，过滤不常用词
// num_threads = 12, 线程数，
// min_reduce = 1，ReduceVocab()删除词频小于这个值的词，因为哈希表可以装填的词数是有限的
int binary = 0, cbow = 1, debug_mode = 2, window = 5, min_count = 5, num_threads = 12, min_reduce = 1;
// 词表的Hash存储，下标是词的Hash值，内容是词在词表中的位置
int *vocab_hash;
// 词表最大容量初始为1000，词表大小初始为0，向量维度默认为100
long long vocab_max_size = 1000, vocab_size = 0, layer1_size = 100;
// ?
long long train_words = 0, word_count_actual = 0, iter = 5, file_size = 0, classes = 0;
// 默认学习率为0.025，初始学习率，亚采样的概率参数
real alpha = 0.025, starting_alpha, sample = 1e-3;
// TODO:
// syn0 单词的向量输入，也用于存储最终的vector
// syn1 hs(Hierarchical Softmax)中隐层节点到霍夫曼编码树非叶结点的映射权重
// syn1neg ns(Negative Sampling)中隐层节点到分类问题的映射权重
// expTable 预先存储f函数结果，算法执行中查表
real *syn0, *syn1, *syn1neg, *expTable;
// 用于统计运行时间
clock_t start;

// 默认采用Negative Sampling
// 使用的负例个数默认为5
int hs = 0, negative = 5;
// table_size 静态采样表的规模
// table 采样表
const int table_size = 1e8;
int *table;

// 根据词频生成采样表
void InitUnigramTable() 
{
  int a, i;
  long long train_words_pow = 0;
  real d1, power = 0.75; // 概率与词频的power次方成正比
  table = (int *)malloc(table_size * sizeof(int));
  for (a = 0; a < vocab_size; a++) 
    train_words_pow += pow(vocab[a].cn, power);
  i = 0;
  // 把table里每一维变成pow(cn, 0.75)间隔里对应的i，类似于sample
  d1 = pow(vocab[i].cn, power) / (real)train_words_pow;
  for (a = 0; a < table_size; a++) 
  {
    table[a] = i;
    if (a / (real)table_size > d1) 
    {
      i++;
      d1 += pow(vocab[i].cn, power) / (real)train_words_pow;
    }
    if (i >= vocab_size) 
      i = vocab_size - 1;
  }
}

// Reads a single word from a file, assuming space + tab + EOL to be word boundaries
// 用途：从文件中读入一个词，假设空格、tab和换行是词的边界
// 输入：用于读入的词word和文件fin
// 输出：无
void ReadWord(char *word, FILE *fin) 
{
  int a = 0, ch;
  while (!feof(fin)) 
  {
    // 使用fgetc一个字符一个字符的读入
    ch = fgetc(fin);
    if (ch == 13) continue;
    // 如果是空格、tab或者换行
    if ((ch == ' ') || (ch == '\t') || (ch == '\n')) 
    {
      // 如果之前已经有其它字符读入
      if (a > 0) 
      {
        // 如果是换行，则不读入，而空格和tab则扔掉
        if (ch == '\n') ungetc(ch, fin);
        break;
      }
      // 如果是换行，则转换为</s>，否则就扔掉。（相当于trim掉之前的多个空格或tab）
      if (ch == '\n') 
      {
        strcpy(word, (char *)"</s>");
        return;
      } else continue;
    }
    // 读入字符
    word[a] = ch;
    a++;
    if (a >= MAX_STRING - 1) 
      a--;   // Truncate too long words
  }
  word[a] = 0;
}

// Returns hash value of a word
// 用途：返回一个词word的hash值
// 输入：一个词的char*
// 输出：代表hash值的int
int GetWordHash(char *word) 
{
  unsigned long long a, hash = 0;
  for (a = 0; a < strlen(word); a++) 
    // hash等于当前值乘以257再加上词当前的char值？（可以考察一下hash值的算法）
    hash = hash * 257 + word[a];
  hash = hash % vocab_hash_size;
  return hash;
}

// Returns position of a word in the vocabulary; if the word is not found, returns -1
// 用途：返回词word在词表中的位置(线性探索，开放定址法)
// 输入：词word
// 输出：若存在则返回位置，若不存在则返回-1
int SearchVocab(char *word) {
  unsigned int hash = GetWordHash(word);
  // 由于多个词可能有相同的hash值，因此需要遍历到下一个为-1之前的位置
  while (1) 
  {
    if (vocab_hash[hash] == -1) 
      return -1;
    if (!strcmp(word, vocab[vocab_hash[hash]].word)) 
      return vocab_hash[hash];
    hash = (hash + 1) % vocab_hash_size;
  }
  return -1;
}

// Reads a word and returns its index in the vocabulary
// 用途：从文件读入一个词并且返回它在词表中的位置
// 输入：文件fin
// 输出：词表中的位置。若到文件末尾或者词表中不存在则返回-1
int ReadWordIndex(FILE *fin) 
{
  char word[MAX_STRING];
  ReadWord(word, fin);
  if (feof(fin)) 
    return -1;
  return SearchVocab(word);
}

// Adds a word to the vocabulary
// 用途：将一个词word加入到词表中
// 输入：词word
// 输出：词word在词表中的索引
int AddWordToVocab(char *word) 
{
  // 正整数hash用于计算词word的hash值，length等于词的长度+1(如果超过长度则截断)
  unsigned int hash, length = strlen(word) + 1;
  if (length > MAX_STRING) 
    length = MAX_STRING;
  // 将这个词加入到末尾
  vocab[vocab_size].word = (char *)calloc(length, sizeof(char));
  strcpy(vocab[vocab_size].word, word);
  vocab[vocab_size].cn = 0;
  vocab_size++;
  // Reallocate memory if needed
  // 如果有必要的话就重新分配内存
  if (vocab_size + 2 >= vocab_max_size) 
  {
    // 每次加1000
    vocab_max_size += 1000;
    vocab = (struct vocab_word *)realloc(vocab, vocab_max_size * sizeof(struct vocab_word));
  }
  // 计算得到word的hash值
  hash = GetWordHash(word);
  // 如果这个词的hash值被使用，继续往下找
  while (vocab_hash[hash] != -1) 
    hash = (hash + 1) % vocab_hash_size;
  // 记录最终的hash值，这样可以通过vocab_hash来查找该词
  vocab_hash[hash] = vocab_size - 1;
  return vocab_size - 1;
}

// Used later for sorting by word counts
// 用途：用于比较两个词的词频高低
// 输入：两个词的vocab_word结构
// 输出：词b与词a的差值
int VocabCompare(const void *a, const void *b) 
{
    return ((struct vocab_word *)b)->cn - ((struct vocab_word *)a)->cn;
}

// Sorts the vocabulary by frequency using word counts
// 用途：对词表按照词频高低进行排序
// 输入：无（对vocab进行排序）
// 输出：无
void SortVocab() 
{
  int a, size;
  unsigned int hash;
  // Sort the vocabulary and keep </s> at the first position
  // 使用qsort函数对词表进行排序并且保留</s>排在第一位
  qsort(&vocab[1], vocab_size - 1, sizeof(struct vocab_word), VocabCompare);
  // 重新生成hash映射
  for (a = 0; a < vocab_hash_size; a++) 
    vocab_hash[a] = -1;
  // 大小设为词表大小
  size = vocab_size;
  train_words = 0;
  for (a = 0; a < size; a++) 
  {
    // Words occuring less than min_count times will be discarded from the vocab
    // 如果频度低于min_count，则扔掉
    if ((vocab[a].cn < min_count) && (a != 0)) 
    {
      vocab_size--;
      free(vocab[a].word);
    } 
    else 
    {
      // Hash will be re-computed, as after the sorting it is not actual
       // 重新计算hash值
      hash=GetWordHash(vocab[a].word);
      while (vocab_hash[hash] != -1) hash = (hash + 1) % vocab_hash_size;
      vocab_hash[hash] = a;
      train_words += vocab[a].cn;
    }
  }
  // 重新为词表分配内存大小
  vocab = (struct vocab_word *)realloc(vocab, (vocab_size + 1) * sizeof(struct vocab_word));
  // Allocate memory for the binary tree construction
  // 给霍夫曼编码和路径的词表索引分配空间?
  for (a = 0; a < vocab_size; a++) 
  {
    vocab[a].code = (char *)calloc(MAX_CODE_LENGTH, sizeof(char));
    vocab[a].point = (int *)calloc(MAX_CODE_LENGTH, sizeof(int));
  }
}

// Reduces the vocabulary by removing infrequent tokens
// 用途：通过删除不常见的tokens来减小词表大小
// 输入：无
// 输出：无（直接对全局变量vocab进行操作）
void ReduceVocab() 
{
  int a, b = 0;
  unsigned int hash;
  for (a = 0; a < vocab_size; a++) 
    // 对频度不超过min_reduce的词直接删除
    if (vocab[a].cn > min_reduce) 
    {
      vocab[b].cn = vocab[a].cn;
      vocab[b].word = vocab[a].word;
      b++;
    } 
    else 
      free(vocab[a].word);
  vocab_size = b;
  for (a = 0; a < vocab_hash_size; a++) 
    vocab_hash[a] = -1;
  for (a = 0; a < vocab_size; a++) 
  {
    // Hash will be re-computed, as it is not actual
    hash = GetWordHash(vocab[a].word);
    while (vocab_hash[hash] != -1) 
      hash = (hash + 1) % vocab_hash_size;
    vocab_hash[hash] = a;
  }
  fflush(stdout);
  // 每删除一次，min_reduce加1
  min_reduce++;
}

// Create binary Huffman tree using the word counts
// Frequent words will have short uniqe binary codes
// 用途：使用词的频度来创建二叉Huffman树，使得频度高的词具有更短的二进制编码
void CreateBinaryTree() 
{
  long long a, b, i, min1i, min2i, pos1, pos2, point[MAX_CODE_LENGTH];
  char code[MAX_CODE_LENGTH];
  // count用来存储结点的count
  long long *count = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
  // binary用于记录Huffman树中两个结点哪个结果比较大，大的标记为1
  long long *binary = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
  // parent_node用来存储结点的父结点
  long long *parent_node = (long long *)calloc(vocab_size * 2 + 1, sizeof(long long));
  // 对于count,前n个值初始化为vocab里词的频度（注意这里已经排序），后n个值初始化为1e15(一个极大值)
  for (a = 0; a < vocab_size; a++) 
    count[a] = vocab[a].cn;
  for (a = vocab_size; a < vocab_size * 2; a++) 
    count[a] = 1e15;
  pos1 = vocab_size - 1;
  pos2 = vocab_size;
  // Following algorithm constructs the Huffman tree by adding one node at a time
  for (a = 0; a < vocab_size - 1; a++) 
  {
    // First, find two smallest nodes 'min1, min2'
    if (pos1 >= 0) 
    {
      if (count[pos1] < count[pos2]) 
      {
        min1i = pos1;
        pos1--;
      } 
      else 
      {
        min1i = pos2;
        pos2++;
      }
    } 
    else 
    {
      min1i = pos2;
      pos2++;
    }
    if (pos1 >= 0) 
    {
      if (count[pos1] < count[pos2]) 
      {
        min2i = pos1;
        pos1--;
      } 
      else 
      {
        min2i = pos2;
        pos2++;
      }
    } 
    else 
    {
      min2i = pos2;
      pos2++;
    }
    count[vocab_size + a] = count[min1i] + count[min2i];
    parent_node[min1i] = vocab_size + a;
    parent_node[min2i] = vocab_size + a;
    binary[min2i] = 1;
  }
  // Now assign binary code to each vocabulary word
  for (a = 0; a < vocab_size; a++) 
  {
    b = a;
    i = 0;
    while (1) 
    {
      code[i] = binary[b];
      point[i] = b;
      i++;
      b = parent_node[b];
      if (b == vocab_size * 2 - 2) break;
    }
    vocab[a].codelen = i;
    vocab[a].point[0] = vocab_size - 2;
    for (b = 0; b < i; b++) 
    {
      vocab[a].code[i - b - 1] = code[b];
      vocab[a].point[i - b] = point[b] - vocab_size;
    }
  }
  free(count);
  free(binary);
  free(parent_node);
}

// 用途：从训练文本中得到词表
// 输入：无（训练文本由全局变量指定）
// 输出：无（得到最终的vocab）
void LearnVocabFromTrainFile() {
  char word[MAX_STRING];
  FILE *fin;
  long long a, i;
  for (a = 0; a < vocab_hash_size; a++) 
    vocab_hash[a] = -1;
  fin = fopen(train_file, "rb");
  if (fin == NULL) 
  {
    printf("ERROR: training data file not found!\n");
    exit(1);
  }
  vocab_size = 0;
  // 先将</s>加入到词表中
  AddWordToVocab((char *)"</s>");
  while (1) 
  {
    // 读入一个词
    ReadWord(word, fin);
    if (feof(fin)) 
      break;
    train_words++;
    if ((debug_mode > 1) && (train_words % 100000 == 0)) 
    {
      printf("%lldK%c", train_words / 1000, 13);
      fflush(stdout);
    }
    // 在词表中查找，如果没有则加入，如果有，则增加计数
    i = SearchVocab(word);
    if (i == -1) {
      a = AddWordToVocab(word);
      vocab[a].cn = 1;
    } 
    else 
      vocab[i].cn++;
    // 如果超出装填系数，则减小词表。
    if (vocab_size > vocab_hash_size * 0.7) 
      ReduceVocab();
  }
  // 给词表排序
  SortVocab();
  if (debug_mode > 0) 
  {
    printf("Vocab size: %lld\n", vocab_size);
    printf("Words in train file: %lld\n", train_words);
  }
  // 得到输入文件大小
  file_size = ftell(fin);
  fclose(fin);
}

// 用途：保存词表
// 输入：无（输出至save_vocab_file指定的位置）
// 输出：无
void SaveVocab() 
{
  long long i;
  FILE *fo = fopen(save_vocab_file, "wb");
  for (i = 0; i < vocab_size; i++) 
    // 每一行输出词及词的频率
    fprintf(fo, "%s %lld\n", vocab[i].word, vocab[i].cn);
  fclose(fo);
}

// 用途：从文件中读入词表
// 输入：无（读入文件由全局变量read_vocab_file指定）
// 输出：无
void ReadVocab() 
{
  long long a, i = 0;
  char c;
  char word[MAX_STRING];
  FILE *fin = fopen(read_vocab_file, "rb");
  if (fin == NULL) 
  {
    printf("Vocabulary file not found\n");
    exit(1);
  }
  // 将每一个vocab_hash初始化为-1
  for (a = 0; a < vocab_hash_size; a++) 
    vocab_hash[a] = -1;
  vocab_size = 0;
  while (1) 
  {
    // 读入一个词word
    ReadWord(word, fin);
    if (feof(fin)) break;
    // 把这个词word加入到vocab中，返回在vocab中的位置a
    a = AddWordToVocab(word);
    // 读入词word的频度以及分隔符
    fscanf(fin, "%lld%c", &vocab[a].cn, &c);
    i++;
  }
  // 对词表进行排序
  SortVocab();
  if (debug_mode > 0) 
  {
    printf("Vocab size: %lld\n", vocab_size);
    printf("Words in train file: %lld\n", train_words);
  }
  fin = fopen(train_file, "rb");
  if (fin == NULL) 
  {
    printf("ERROR: training data file not found!\n");
    exit(1);
  }
  fseek(fin, 0, SEEK_END);
  file_size = ftell(fin);
  fclose(fin);
}

// 用途：初始化网络结构
// 输入：无
// 输出：无
void InitNet() 
{
  long long a, b;
  unsigned long long next_random = 1;
  // posix_memalign() 成功时会返回size字节的动态内存，并且这块内存的地址是alignment(这里是128)的倍数
  // syn0 存储的是word vectors
  a = posix_memalign((void **)&syn0, 128, (long long)vocab_size * layer1_size * sizeof(real));
  if (syn0 == NULL) 
  {
    printf("Memory allocation failed\n"); 
    exit(1);
  }
  if (hs) // Hierarchical Softmax
  {
    // hs中，用syn1
    a = posix_memalign((void **)&syn1, 128, (long long)vocab_size * layer1_size * sizeof(real));
    if (syn1 == NULL) 
      {
        printf("Memory allocation failed\n"); 
        exit(1);
      }
    for (a = 0; a < vocab_size; a++) 
      for (b = 0; b < layer1_size; b++)
        syn1[a * layer1_size + b] = 0;
  }
  if (negative>0) // Negative Sampling
  {
    // ns中，用syn1neg
    a = posix_memalign((void **)&syn1neg, 128, (long long)vocab_size * layer1_size * sizeof(real));
    if (syn1neg == NULL) 
      {
        printf("Memory allocation failed\n"); 
        exit(1);
      }
    for (a = 0; a < vocab_size; a++) 
      for (b = 0; b < layer1_size; b++)
        syn1neg[a * layer1_size + b] = 0;
  }
  // 向量随机初始化
  for (a = 0; a < vocab_size; a++) 
    for (b = 0; b < layer1_size; b++) 
    {
      next_random = next_random * (unsigned long long)25214903917 + 11;
      syn0[a * layer1_size + b] = (((next_random & 0xFFFF) / (real)65536) - 0.5) / layer1_size;
    }
  CreateBinaryTree();
}

// 用途：多线程训练模型
// 输入：线程id
// 输出：void*（多线程函数的标准输出，可用pthread_join得到返回值）
void *TrainModelThread(void *id) 
{
  // a, b, d用于循环
  // word记录词的索引
  // last_word记录上一个词的索引
  // sentence_length为句子长度
  // sentence_position为句子位置
  long long a, b, d, cw, word, last_word, sentence_length = 0, sentence_position = 0;
  // word_count记录目前训练的词数
  // last_word_count为上一次记录时训练的词数
  // sen表示句子
  long long word_count = 0, last_word_count = 0, sen[MAX_SENTENCE_LENGTH + 1];
  // TODO:
  // l1 ns中表示word在concatenated word vectors中的起始位置，之后layer1_size是对应的word vector，因为把矩阵拉成长向量了
  // l2 cbow或ns中权重向量的起始位置，之后layer1_size是对应的syn1或syn1neg，因为把矩阵拉成长向量了
  // c 循环中的计数作用
  // target ns中当前的sample
  // label ns中当前sample的label
  long long l1, l2, c, target, label, local_iter = iter;
  // id 线程创建的时候传入，辅助随机数生成
  unsigned long long next_random = (long long)id;
  // f e^x / (1/e^x)，fs中指当前编码为是0（父亲的左子节点为0，右为1）的概率，ns中指label是1的概率
  // g 误差(f与真实值的偏离)与学习速率的乘积
  real f, g;
  clock_t now;
  // neu1是隐含层，neu1e是隐含层误差
  real *neu1 = (real *)calloc(layer1_size, sizeof(real));
  real *neu1e = (real *)calloc(layer1_size, sizeof(real));
  FILE *fi = fopen(train_file, "rb");
  // 将文件内容分配给各个线程
  fseek(fi, file_size / (long long)num_threads * (long long)id, SEEK_SET);
  while (1) 
  {
    // 如果目前记录的词数已经比之前记录的多了10000个词，则输出log
    if (word_count - last_word_count > 10000) 
    {
      // 用word_count_actual记录实际训练的词数 
      word_count_actual += word_count - last_word_count;
      // 更新记录的词数
      last_word_count = word_count;
      // 输出学习率，进度和运行的速度
      if ((debug_mode > 1)) 
      {
        now=clock();
        printf("%cAlpha: %f  Progress: %.2f%%  Words/thread/sec: %.2fk  ", 13, alpha,
         word_count_actual / (real)(iter * train_words + 1) * 100,
         word_count_actual / ((real)(now - start + 1) / (real)CLOCKS_PER_SEC * 1000));
        fflush(stdout);
      }
      // 更新学习率
      alpha = starting_alpha * (1 - word_count_actual / (real)(iter * train_words + 1));
      // 最小学习率不低于初始学习率的万分之一
      if (alpha < starting_alpha * 0.0001) 
        alpha = starting_alpha * 0.0001;
    }
    // 若句子长度为0
    if (sentence_length == 0) 
    {
      while (1) {
        // 读入一个词，得到它的索引
        word = ReadWordIndex(fi);
        // 如果到文件尾，跳出
        if (feof(fi)) 
          break;
        // 如果这个词不在vocab中（例如频度小于min_count或者被reduce），继续读入
        if (word == -1) 
          continue;
        // 记录的词数加1
        word_count++;
        // 如果是换行，跳出
        if (word == 0) 
          break;
        // The subsampling randomly discards frequent words while keeping the ranking same
        // 这里的亚采样是指 Sub-Sampling，Mikolov 在论文指出这种亚采样能够带来 2 到 10 倍的性能提升，并能够提升低频词的表示精度。
        // 低频词被丢弃概率低，高频词被丢弃概率高
        if (sample > 0) 
        {
          real ran = (sqrt(vocab[word].cn / (sample * train_words)) + 1) * (sample * train_words) / vocab[word].cn;
          next_random = next_random * (unsigned long long)25214903917 + 11;
          if (ran < (next_random & 0xFFFF) / (real)65536) 
            continue;
        }
        // 把词加入到sen中
        sen[sentence_length] = word;
        // 增加句子长度
        sentence_length++;
        // 如果达到最大长度，跳出
        if (sentence_length >= MAX_SENTENCE_LENGTH) 
          break;
      }
      sentence_position = 0;
    }
    // 如果到了文件末尾或者线程已经训练完了该训练的文本，跳出
    if (feof(fi) || (word_count > train_words / num_threads)) 
    {
      word_count_actual += word_count - last_word_count;
      local_iter--;
      if (local_iter == 0) break;
      word_count = 0;
      last_word_count = 0;
      sentence_length = 0;
      fseek(fi, file_size / (long long)num_threads * (long long)id, SEEK_SET);
      continue;
    }
    // 取句子中的第一个单词，开始运行BP算法
    word = sen[sentence_position];
    if (word == -1) 
      continue;
    // 隐层节点值和隐层节点误差累计项清零
    for (c = 0; c < layer1_size; c++) neu1[c] = 0;
    for (c = 0; c < layer1_size; c++) neu1e[c] = 0;
    // 随机一个窗口大小
    next_random = next_random * (unsigned long long)25214903917 + 11;
    b = next_random % window;
    // cbow模型
    if (cbow) 
    {  //train the cbow architecture
      // in -> hidden
      cw = 0;
      // 实际上的窗口取的不是window，而是一个随机数%window后的窗口大小
      for (a = b; a < window * 2 + 1 - b; a++) if (a != window) 
      {
        c = sentence_position - window + a;
        // 跳过边界
        if (c < 0) 
          continue;
        if (c >= sentence_length) 
          continue;
        // 记录最后的那个词
        last_word = sen[c];
        // 如果是-1，则下一个词
        if (last_word == -1) 
          continue;
        // 把窗口中的词向量加起来得到neu1
        for (c = 0; c < layer1_size; c++) 
          neu1[c] += syn0[c + last_word * layer1_size];
        cw++;
      }
      if (cw) 
      {
        // TODO:
        for (c = 0; c < layer1_size; c++) 
          neu1[c] /= cw;
        // Hierarchical Softmax
        if (hs) 
          for (d = 0; d < vocab[word].codelen; d++) 
          {
            f = 0;
            // l2 是word在第i层上的父结点 ?
            l2 = vocab[word].point[d] * layer1_size;
            // Propagate hidden -> output
            // 从隐藏层到输出层
            for (c = 0; c < layer1_size; c++) 
              f += neu1[c] * syn1[c + l2];
            // 如果f 比-MAX_EXP小或者比MAX_EXP大，继续，否则f=
            if (f <= -MAX_EXP) 
              continue;
            else if (f >= MAX_EXP) 
              continue;
            else 
              // 从expTable中查找，快速计算
              f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
            // 'g' is the gradient multiplied by the learning rate
            // g 是梯度乘以学习率
            g = (1 - vocab[word].code[d] - f) * alpha;
            // Propagate errors output -> hidden
            // 记录累积误差项
            for (c = 0; c < layer1_size; c++) 
              neu1e[c] += g * syn1[c + l2];
            // Learn weights hidden -> output
            // 更新隐层到霍夫曼树非叶节点的权重
            for (c = 0; c < layer1_size; c++) s
              syn1[c + l2] += g * neu1[c];
          }
        // NEGATIVE SAMPLING
        if (negative > 0) 
          for (d = 0; d < negative + 1; d++) 
          {
            // 采样negative个负例出来，d=0时是正例
            if (d == 0) 
            {
              target = word;
              label = 1;
            } 
            // 随机取得一个负例
            else 
            {
              next_random = next_random * (unsigned long long)25214903917 + 11;
              target = table[(next_random >> 16) % table_size];
              if (target == 0) 
                target = next_random % (vocab_size - 1) + 1;
              if (target == word) 
                continue;
              label = 0;
            }
            // 得到负例所在的层
            l2 = target * layer1_size;
            f = 0;
            // 计算得到概率
            for (c = 0; c < layer1_size; c++) 
              f += neu1[c] * syn1neg[c + l2];
            // 得到梯度
            if (f > MAX_EXP) 
              g = (label - 1) * alpha;
            else if (f < -MAX_EXP) 
              g = (label - 0) * alpha;
            else 
              g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;
            // TODO:
            // 得到传播错误
            for (c = 0; c < layer1_size; c++) 
              neu1e[c] += g * syn1neg[c + l2];
            // 更新传播矩阵
            for (c = 0; c < layer1_size; c++) 
              syn1neg[c + l2] += g * neu1[c];
          }
        // hidden -> in
        // 最后再从隐含层回到输入层
        for (a = b; a < window * 2 + 1 - b; a++) if (a != window) 
        {
          c = sentence_position - window + a;
          if (c < 0) 
            continue;
          if (c >= sentence_length) 
            continue;
          last_word = sen[c];
          if (last_word == -1) 
            continue;
          // 对每一个词都用同样的neu1e更新得到新的输入
          for (c = 0; c < layer1_size; c++) 
            syn0[c + last_word * layer1_size] += neu1e[c];
        }
      }
    } 
    // sip-gram模型
    else 
    {  //train skip-gram
      // 预测非中心的单词（窗口内的单词）
      for (a = b; a < window * 2 + 1 - b; a++) 
        if (a != window) 
        {
          c = sentence_position - window + a;
          if (c < 0) 
            continue;
          if (c >= sentence_length) 
            continue;
          last_word = sen[c];
          if (last_word == -1) 
            continue;
          // l1 是用自己作用隐藏层 ?
          l1 = last_word * layer1_size;
          for (c = 0; c < layer1_size; c++) 
            neu1e[c] = 0;
          // HIERARCHICAL SOFTMAX
          if (hs) 
            for (d = 0; d < vocab[word].codelen; d++) 
            {
              f = 0;
              // l2指定的是word在第d层的父结点，下面类似
              l2 = vocab[word].point[d] * layer1_size;
              // Propagate hidden -> output
              for (c = 0; c < layer1_size; c++) 
                f += syn0[c + l1] * syn1[c + l2];
              if (f <= -MAX_EXP) 
                continue;
              else if (f >= MAX_EXP) 
                continue;
              else 
                f = expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))];
              // 'g' is the gradient multiplied by the learning rate
              g = (1 - vocab[word].code[d] - f) * alpha;
              // Propagate errors output -> hidden
              for (c = 0; c < layer1_size; c++) 
                neu1e[c] += g * syn1[c + l2];
              // Learn weights hidden -> output
              for (c = 0; c < layer1_size; c++) 
                syn1[c + l2] += g * syn0[c + l1];
            }   
          // NEGATIVE SAMPLING
          if (negative > 0) 
            for (d = 0; d < negative + 1; d++) 
            {
              if (d == 0) 
              {
                target = word;
                label = 1;
              } 
              else 
              {
                next_random = next_random * (unsigned long long)25214903917 + 11;
                target = table[(next_random >> 16) % table_size];
                if (target == 0) target = next_random % (vocab_size - 1) + 1;
                if (target == word) continue;
                label = 0;
              }
              // 以自己作为隐含层，以target作为隐含层到输出层的weight的索引
              l2 = target * layer1_size;
              f = 0;
              // 待预测单词的 word vecotr 和 隐层-霍夫曼树非叶节点权重 的内积 ?
              for (c = 0; c < layer1_size; c++) 
                f += syn0[c + l1] * syn1neg[c + l2];
              if (f > MAX_EXP) 
                g = (label - 1) * alpha;
              else if (f < -MAX_EXP) 
                g = (label - 0) * alpha;
              else 
                g = (label - expTable[(int)((f + MAX_EXP) * (EXP_TABLE_SIZE / MAX_EXP / 2))]) * alpha;
              for (c = 0; c < layer1_size; c++) 
                neu1e[c] += g * syn1neg[c + l2];
              for (c = 0; c < layer1_size; c++) 
                syn1neg[c + l2] += g * syn0[c + l1];
            }
          // Learn weights input -> hidden
          for (c = 0; c < layer1_size; c++) syn0[c + l1] += neu1e[c];
        }
    }
    sentence_position++;
    if (sentence_position >= sentence_length) {
      sentence_length = 0;
      continue;
    }
  }
  fclose(fi);
  free(neu1);
  free(neu1e);
  pthread_exit(NULL);
}

void TrainModel() {
  long a, b, c, d;
  FILE *fo;
  pthread_t *pt = (pthread_t *)malloc(num_threads * sizeof(pthread_t));
  printf("Starting training using file %s\n", train_file);
  starting_alpha = alpha;
  if (read_vocab_file[0] != 0) ReadVocab(); else LearnVocabFromTrainFile();
  if (save_vocab_file[0] != 0) SaveVocab();
  if (output_file[0] == 0) return;
  InitNet();
  if (negative > 0) InitUnigramTable();
  start = clock();
  for (a = 0; a < num_threads; a++) pthread_create(&pt[a], NULL, TrainModelThread, (void *)a);
  for (a = 0; a < num_threads; a++) pthread_join(pt[a], NULL);
  fo = fopen(output_file, "wb");
  if (classes == 0) {
    // Save the word vectors
    fprintf(fo, "%lld %lld\n", vocab_size, layer1_size);
    for (a = 0; a < vocab_size; a++) {
      fprintf(fo, "%s ", vocab[a].word);
      if (binary) for (b = 0; b < layer1_size; b++) fwrite(&syn0[a * layer1_size + b], sizeof(real), 1, fo);
      else for (b = 0; b < layer1_size; b++) fprintf(fo, "%lf ", syn0[a * layer1_size + b]);
      fprintf(fo, "\n");
    }
  } else {
    // Run K-means on the word vectors
    int clcn = classes, iter = 10, closeid;
    int *centcn = (int *)malloc(classes * sizeof(int));
    int *cl = (int *)calloc(vocab_size, sizeof(int));
    real closev, x;
    real *cent = (real *)calloc(classes * layer1_size, sizeof(real));
    for (a = 0; a < vocab_size; a++) cl[a] = a % clcn;
    for (a = 0; a < iter; a++) {
      for (b = 0; b < clcn * layer1_size; b++) cent[b] = 0;
      for (b = 0; b < clcn; b++) centcn[b] = 1;
      for (c = 0; c < vocab_size; c++) {
        for (d = 0; d < layer1_size; d++) cent[layer1_size * cl[c] + d] += syn0[c * layer1_size + d];
        centcn[cl[c]]++;
      }
      for (b = 0; b < clcn; b++) {
        closev = 0;
        for (c = 0; c < layer1_size; c++) {
          cent[layer1_size * b + c] /= centcn[b];
          closev += cent[layer1_size * b + c] * cent[layer1_size * b + c];
        }
        closev = sqrt(closev);
        for (c = 0; c < layer1_size; c++) cent[layer1_size * b + c] /= closev;
      }
      for (c = 0; c < vocab_size; c++) {
        closev = -10;
        closeid = 0;
        for (d = 0; d < clcn; d++) {
          x = 0;
          for (b = 0; b < layer1_size; b++) x += cent[layer1_size * d + b] * syn0[c * layer1_size + b];
          if (x > closev) {
            closev = x;
            closeid = d;
          }
        }
        cl[c] = closeid;
      }
    }
    // Save the K-means classes
    for (a = 0; a < vocab_size; a++) fprintf(fo, "%s %d\n", vocab[a].word, cl[a]);
    free(centcn);
    free(cent);
    free(cl);
  }
  fclose(fo);
}

int ArgPos(char *str, int argc, char **argv) {
  int a;
  for (a = 1; a < argc; a++) if (!strcmp(str, argv[a])) {
    if (a == argc - 1) {
      printf("Argument missing for %s\n", str);
      exit(1);
    }
    return a;
  }
  return -1;
}

int main(int argc, char **argv) {
  int i;
  if (argc == 1) {
    printf("WORD VECTOR estimation toolkit v 0.1c\n\n");
    printf("Options:\n");
    printf("Parameters for training:\n");
    printf("\t-train <file>\n");
    printf("\t\tUse text data from <file> to train the model\n");
    printf("\t-output <file>\n");
    printf("\t\tUse <file> to save the resulting word vectors / word clusters\n");
    printf("\t-size <int>\n");
    printf("\t\tSet size of word vectors; default is 100\n");
    printf("\t-window <int>\n");
    printf("\t\tSet max skip length between words; default is 5\n");
    printf("\t-sample <float>\n");
    printf("\t\tSet threshold for occurrence of words. Those that appear with higher frequency in the training data\n");
    printf("\t\twill be randomly down-sampled; default is 1e-3, useful range is (0, 1e-5)\n");
    printf("\t-hs <int>\n");
    printf("\t\tUse Hierarchical Softmax; default is 0 (not used)\n");
    printf("\t-negative <int>\n");
    printf("\t\tNumber of negative examples; default is 5, common values are 3 - 10 (0 = not used)\n");
    printf("\t-threads <int>\n");
    printf("\t\tUse <int> threads (default 12)\n");
    printf("\t-iter <int>\n");
    printf("\t\tRun more training iterations (default 5)\n");
    printf("\t-min-count <int>\n");
    printf("\t\tThis will discard words that appear less than <int> times; default is 5\n");
    printf("\t-alpha <float>\n");
    printf("\t\tSet the starting learning rate; default is 0.025 for skip-gram and 0.05 for CBOW\n");
    printf("\t-classes <int>\n");
    printf("\t\tOutput word classes rather than word vectors; default number of classes is 0 (vectors are written)\n");
    printf("\t-debug <int>\n");
    printf("\t\tSet the debug mode (default = 2 = more info during training)\n");
    printf("\t-binary <int>\n");
    printf("\t\tSave the resulting vectors in binary moded; default is 0 (off)\n");
    printf("\t-save-vocab <file>\n");
    printf("\t\tThe vocabulary will be saved to <file>\n");
    printf("\t-read-vocab <file>\n");
    printf("\t\tThe vocabulary will be read from <file>, not constructed from the training data\n");
    printf("\t-cbow <int>\n");
    printf("\t\tUse the continuous bag of words model; default is 1 (use 0 for skip-gram model)\n");
    printf("\nExamples:\n");
    printf("./word2vec -train data.txt -output vec.txt -size 200 -window 5 -sample 1e-4 -negative 5 -hs 0 -binary 0 -cbow 1 -iter 3\n\n");
    return 0;
  }
  output_file[0] = 0;
  save_vocab_file[0] = 0;
  read_vocab_file[0] = 0;
  if ((i = ArgPos((char *)"-size", argc, argv)) > 0) layer1_size = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-train", argc, argv)) > 0) strcpy(train_file, argv[i + 1]);
  if ((i = ArgPos((char *)"-save-vocab", argc, argv)) > 0) strcpy(save_vocab_file, argv[i + 1]);
  if ((i = ArgPos((char *)"-read-vocab", argc, argv)) > 0) strcpy(read_vocab_file, argv[i + 1]);
  if ((i = ArgPos((char *)"-debug", argc, argv)) > 0) debug_mode = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-binary", argc, argv)) > 0) binary = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-cbow", argc, argv)) > 0) cbow = atoi(argv[i + 1]);
  if (cbow) alpha = 0.05;
  if ((i = ArgPos((char *)"-alpha", argc, argv)) > 0) alpha = atof(argv[i + 1]);
  if ((i = ArgPos((char *)"-output", argc, argv)) > 0) strcpy(output_file, argv[i + 1]);
  if ((i = ArgPos((char *)"-window", argc, argv)) > 0) window = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-sample", argc, argv)) > 0) sample = atof(argv[i + 1]);
  if ((i = ArgPos((char *)"-hs", argc, argv)) > 0) hs = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-negative", argc, argv)) > 0) negative = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-threads", argc, argv)) > 0) num_threads = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-iter", argc, argv)) > 0) iter = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-min-count", argc, argv)) > 0) min_count = atoi(argv[i + 1]);
  if ((i = ArgPos((char *)"-classes", argc, argv)) > 0) classes = atoi(argv[i + 1]);
  vocab = (struct vocab_word *)calloc(vocab_max_size, sizeof(struct vocab_word));
  vocab_hash = (int *)calloc(vocab_hash_size, sizeof(int));
  expTable = (real *)malloc((EXP_TABLE_SIZE + 1) * sizeof(real));
  for (i = 0; i < EXP_TABLE_SIZE; i++) {
    expTable[i] = exp((i / (real)EXP_TABLE_SIZE * 2 - 1) * MAX_EXP); // Precompute the exp() table
    expTable[i] = expTable[i] / (expTable[i] + 1);                   // Precompute f(x) = x / (x + 1)
  }
  TrainModel();
  return 0;
}
