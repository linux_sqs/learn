# 单循环链表
```cpp
/*single cycle list 单循环链表

	第3版
	节点内的泛型变量：由类定义
		由类来进行数据的定义和访问
		由结构体来进行结构化存储
*/

#include<iostream>
using namespace std;
#include<assert.h>
//
//    定义结构节点
//
template<typename T>
struct node_of_list
{
	int size;  // 标记位置
	T   data;  //添加模版项
	struct node_of_list<T>* next;
}; //Templates are not allowed in typedef definitions.
template<typename T>
using node = node_of_list<T>;
template<typename T>
using n_ptr = node_of_list<T>*;
//
//    泛型变量初始化：定义要存储的数据
//
class data_save{
	double price;
	unsigned int weight;
	char* name;
public:
	data_save()
	{
		price = (rand() % 1000)*0.01;
		weight = rand() % 20;
		name = get_name();
	}
	char* get_name()
	{
		char* name="pandas";
		//名字 字符串生成器
		return name;
	}
	void get_price()
	{
		cout << price << '-' << weight << '-' << name << endl;
	}
	/*问题：这时候释放内存
		应该由链表还是类的析构函数呢？？？
	*/
};

//
//     单链表初始化
//
template<typename T>
n_ptr<T> cycle_list()
{
	n_ptr<T> firstnode = new node<T>();
	firstnode->size = 0;
	firstnode->data = data_save();
	firstnode->next = firstnode;
	return firstnode;//头指针
}
template<typename T>
n_ptr<T> cycle_list(int num_of_node)
{
	n_ptr<T> firstnode = cycle_list<T>();
	n_ptr<T> temp_ptr=firstnode;
	if (num_of_node >= 2){
		for (int i = 1; i < num_of_node; i++){
			n_ptr<T> node1 = new node<T>();   // 存在堆上需要delete？？？
			node1->size = i;
			node1->data = data_save();
			node1->next = firstnode;
			//连接
			temp_ptr->next = node1;
			temp_ptr = node1;
		}
	}
	return firstnode;
}
//
//     显示 输出循环链表内容
//
template<typename T>
//定义一行数据的输出
void print(n_ptr<T> &temp_ptr)
{
	cout << temp_ptr->size << ":";
	//变化的数据由类定义所以后输出
	temp_ptr->data.get_price();
}
template<typename T>
void show_cycle_list(n_ptr<T> firstnode)
{
	n_ptr<T> temp_ptr = firstnode->next;
	int count = 1;
	print(firstnode);
	while (temp_ptr != firstnode){
		print(temp_ptr);
		temp_ptr = temp_ptr->next;
		if (count % 10 == 0){
			count = 0;
			cout << endl;
		}
		count++;
	}
}
//
//基本操作：插入
//
template<typename T>
bool insert_front(int num,n_ptr<T> &firstnode)
{
	// 初始化节点
	n_ptr<T> node1 = new node<T>();
	assert(node1!=NULL);
	int number = firstnode->size;
	node1->data = data_save();
	node1->size = --number;//头插的情况下减少1
	// 插入链表中
	n_ptr<T> tempnode = firstnode;//子函数中的指针不用手动释放。
	firstnode = node1;
	firstnode->next = tempnode;
	//更改尾指针
	while (tempnode->next!=firstnode->next)
	{
		tempnode = tempnode->next;
	}
	tempnode->next = firstnode;
	return true;
}
template<typename T>
bool insert_back(int num, n_ptr<T> &firstnode)
{
	//找到尾节点
	n_ptr<T> tempnode=firstnode;
	while (tempnode->next != firstnode){
		// 找到尾节点
		tempnode = tempnode->next;
	}
	//初始化待插入节点
	n_ptr<T> node1 = new node<T>();
	assert(node1 != NULL);
	node1->data = data_save();
	int number = tempnode->size;
	node1->size = ++number;//尾插的情况下加1
	//插入链表中
	tempnode->next = node1;
	node1->next = firstnode;
	return true;
}
//
//基本操作：删除
//
template<typename T>
bool del_list(n_ptr<T> &firstnode)//清空链表
{
	n_ptr<T> tempnode = firstnode,tempnode1;
	while (tempnode->next!=firstnode){
		tempnode1 = tempnode;
		tempnode = tempnode->next;
		delete tempnode1;
	}
	delete tempnode;
	return true;
}
template<typename T>
bool del_node(int value, n_ptr<T> firstnode)//去掉一个节点
{
	//找到要删除节点
	n_ptr<T> tempnode = firstnode;
	while (tempnode->size!=value)
	{
		tempnode = tempnode->next;
		if (tempnode == firstnode){
			break;
		}
	}
	//查找 待删除节点的上一个节点
	while (firstnode->next != tempnode){
		firstnode=firstnode -> next;
	}
	firstnode->next = tempnode->next;
	delete tempnode;
	return true;
}
bool del_list(int condition)//按条件删除
{
	return true;
}
//
//基本操作：修改节点内容
//
bool change_content()
{
	return true;
}
//
//基本操作:查找指定内容。
//
template<typename T>
bool find_content(int value, n_ptr<T> firstnode)
{
	n_ptr<T> tempnode = firstnode;
	while (tempnode->size!=value)
	{
		tempnode = tempnode->next;
		if (tempnode == firstnode){
			break;
		}
	}
	if (tempnode->size == value){
		return true;
	}
	else{
		return false;
	}
	return true;
}
//
//测试循环链表
//
template<typename T>
void test_cycle_list()
{
	//1、初始化数据结构：单链表
	n_ptr<data_save> list = cycle_list<data_save>(100);
	//2、插入数据操作
	srand(199);
	for (int i = 0; i < 5;i++){
		int number = rand();
		insert_front(number,list);
		insert_back(number,list);
	}
	cout << "构建完成"<<endl;
	//3、查找数据操作
	bool isfind=find_content(3,list);
	cout << "正确查找到节点了？" << (isfind==1?"是的":"没有") << endl;
	//4、删除数据操作
	del_node(3,list);
	//5、显示现有的数据
	show_cycle_list(list);
	//6、清空结构体
	bool right_run=del_list(list);
	if (right_run){
		cout << "列表成功删除。" << endl;
	}
	else{
		cout << "列表删除失败。";
	}
}
```
## 二分查找
```cpp
/*
	查找问题
	
	实现二分查找

	2017-12-13 night librarysecondfloor

	Son Jonson
*/
#include<iostream>
using namespace std;
//
//		想申请的内存填入数据
//
void assign_value(long* start,long length)
{
	for (long i = 0; i < length;i++){
		*(start + i) = (i+1);
	}
	//cout << *(start + 9) << endl;
}

//
//		折半查找
//			假设数据由小到大排列
//
bool half_find(long* start,unsigned data_large, long find_one)
{
	long upbound = data_large-1;
	long downbound = 0;
	long tmpbound = 0;

	if (find_one < *(start + downbound) || (find_one > *(start + upbound)) ){
		cout << "上界" <<*(start+upbound)<< "下界" << *(start+downbound)<< "待查值" <<find_one << endl;
		return false;
	}
	unsigned mark = 0;
	while (mark<50){
		tmpbound = long((upbound + downbound) / 2);
		//cout << mark << " : " << downbound << " : " << upbound << " : " << tmpbound << endl;
		if (find_one > *(start + tmpbound) ){
			downbound = tmpbound;
		}
		else if (find_one < *(start + tmpbound) ){
			upbound = tmpbound;
		}
		else if (find_one == *(start + tmpbound)){
			return true;
		}
		else{
			cout << "here is a bug." << endl;
			break;
		}
		mark += 1;
	}
	return false; //没找到
}

//
//
//
#include<cstdlib>
int get_rand()
{
	//生成随机数
	return rand();
}


//
//	测试查找函数
//
void test_half_find()
{
	srand(77);
	unsigned data_large = 100000000;//测试集大小
	unsigned wait_find = 68;
	bool result;
	unsigned test_time = 100000;//测试次数

	long* start = new long[data_large]; //存储测试集

	assign_value(start, data_large);
	for (unsigned i = 0; i < test_time; i++){
		wait_find = get_rand();
		result = half_find(start, data_large, wait_find);
		//cout << wait_find << ((result == true) ? "已经找到了！" : "未找到..") << endl;
	}
	cout << "没有其他输出就是正确执行" << endl;

	delete[] start;
}
```
## limit.h
```c++
/*
	本文件用于测试或者debug
	展示全局控制关系。
*/
void limit_exmple()
{
	puts("limits.h defined  常量：");
	printf("The number of bits in a byte %d\n", CHAR_BIT);

	printf("The minimum value of SIGNED CHAR = %d\n", SCHAR_MIN);
	printf("The maximum value of SIGNED CHAR = %d\n", SCHAR_MAX);
	printf("The maximum value of UNSIGNED CHAR = %d\n", UCHAR_MAX);

	printf("The minimum value of SHORT INT = %d\n", SHRT_MIN);
	printf("The maximum value of SHORT INT = %d\n", SHRT_MAX);

	printf("The minimum value of INT = %d\n", INT_MIN);
	printf("The maximum value of INT = %d\n", INT_MAX);

	printf("The minimum value of CHAR = %d\n", CHAR_MIN);
	printf("The maximum value of CHAR = %d\n", CHAR_MAX);

	printf("The minimum value of LONG = %ld\n", LONG_MIN);
	printf("The maximum value of LONG = %ld\n", LONG_MAX);
}
void float_example()
{
	puts("float.h hava the thing:");
	printf("The maximum value of float = %.10e\n", FLT_MAX);
	printf("The minimum value of float = %.10e\n", FLT_MIN);
	printf("The number of digits in the number = %.10e\n", FLT_MANT_DIG);
	cout << "int size" << sizeof(int) << endl;
}

int main()
{
	//float_example();
	//limit_exmple();
	//test_cycle_list<int>();  //测试单循环链表
	//test_b_tree();
	test_half_find();

	firstapp();
	return 0;
}

```