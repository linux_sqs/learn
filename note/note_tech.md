
# unix 环境高级编程
- unistd.h
- 本书别名:unix系统调用相关介绍及示例展示
##1 基础知识
- 所有操作系统都向它们运行的程序提供服务
- 登录名: /etc/passwd
- 有三个用于进程控制的主要函数: f o r k、e x e c和w a i t p i d
- 信息是通知进程已发生某种条件的一种技术
- 所有的操作系统都提供多种服务的入口点,这些入口点被称为系统调用(system call)
- *库函数通过系统调用执行操作*
## 3 文件io
- 大多数U N I X文件I / O只需用到5个函数:open、read、write、lseek 以及close
- 每个打开文件都有一个与其相关联的“当前文件位移量”可以调用l s e e k显式地定位一个打开文件
- U N I X支持在不同进程间共享打开文件
- a c c e s s函数是按实际用户 I D和实际组 I D进行存取许可权测试的
- u m a s k函数为进程设置文件方式创建屏蔽字,并返回以前的值
##  6,7,8,9,10
- 标准I / O库是由Dennis Ritchie在1 9 7 5年左右编写的
- fgets / gets :每次读一行
- a rg c是命令行参数的数目, a rg v是指向参数的各个指针所构成的数组
- 每个进程都有一个非负整型的唯一进程 I D。因为进程 I D标识符总是唯一的
- 一个现存进程调用 f o r k函数是U N I X内核创建一个新进程的唯一方法
- 当多个进程都企图对共享数据进行某种处理,而最后的结果又取决于进程运行的顺序时,则我们认为这发生了竞态条件( race condition)
- 信号是软件中断。很多比较重要的应用程序都需处理信号
## 高级io
- 非阻塞 I / O、记录锁、系统 V流机制、 I / O多路转接,r e a d v和w r i t e v函数,以及存储映照 I / O(m m a p)
- 进程间通信的多种形式;管道、命名管道( F I F O).消息队列、信号量和共享存储
- 强制锁比建议锁增加了大约 1 5 %的系统开销
- 建议锁比不加锁在总时间上增加了约 1 0 %
---

# 技术图片整理
#### 部分内容来源于网络和电子书
![](pict/bash_shutcut.png)  
![](pict/c_dev.jpg)  
![](pict/dense_model.png)  
![](pict/dir-func.png)  
![](pict/GRU_92.6.png)  
![](pict/gunpg.png)  
![](pict/LSTM_model.png)  
![](pict/nlp.jpeg)  
![](pict/nlp_hist.JPG)  
![](pict/nlpapp.jpeg)  
![](pict/NLTK.png)  
![](pict/python_example.png)  
![](pict/seg_error.JPG)  
![](pict/seg_page.JPG)  
![](pict/seg.JPG)  
![](pict/seg_page3.JPG)  
![](pict/local_prinp.JPG)  
![](pict/mem_add.JPG)  
![](pict/virt_mem.JPG)  

---




























