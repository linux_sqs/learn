# 脚本代码
### 构建判别式模型的python脚本:基于keras/tensorflow
```python3
def build_model():
	#两层宽的网络也可以达到87.3%
	act='relu'
	start=Input(shape=(30,100) )
	x=LSTM(300,activation=act,return_sequences=True )(start)
	x1=Dropout(0.2)(x)  #88.9=20  89.46=30
	
	x=LSTM(200,activation=act,return_sequences=True )(x1)
	x2=Dropout(0.2)(x) #89.29
	
	x=Dense(10,activation=act )(x2)
	x3=Dropout(0.2)(x)
	
	x=Dense(8,activation=act )(x3)
	x4=Dropout(0.2)(x)
	
	x=Dense(4,activation=act )(x4)
	x=Dropout(0.2)(x) #89.55
	
	#x=concat([x1,x2,x3,x4,x])
	x=Flatten()(x2)
	end=Dense(2,activation='softmax')(x)
	model_D=Model(inputs=start,output=end)
	model_D.summary()
	return model_D  
	
def build_model1():
	act='relu'
	start=Input(shape=(30,100) )
	x=Dense(100,activation=act )(start)
	x=Dropout(0.2)(x)
	x=Dense(80,activation=act )(x)
	x=Dropout(0.2)(x)
	x=Dense(60,activation=act )(x)
	x=Dropout(0.2)(x)
	x=Dense(40,activation=act )(x)
	x=Dropout(0.2)(x)
	x=Dense(20,activation=act )(x)
	x=Dropout(0.2)(x)
	x=Dense(10,activation=act )(x)
	x=Dropout(0.2)(x)
	x=Dense(10,activation=act )(x)
	x=Dense(8,activation=act )(x)
	x=Dense(4,activation=act )(x)
	x=Flatten()(x)
	end=Dense(2,activation='softmax')(x)
	model_D=Model(inputs=start,output=end)
	return model_D  #100 epoch 89.23.5
```
### 二分查找
```python3
'''
    折半查找python版
    2017-12-13 night 
    library secondfloor 
    Sun Jonson
    
'''
	
def half_find(test_set,test_value):
    '''折半查找算法
    '''
    start=0
    end=len(test_set)-1
    middle=0
    
    for time in range(50):
        ''' time 为执行次数'''
        middle=int((start+end)/2)
        #print(middle,',',start,',',end)
        if test_value < test_set[middle]:
            end=middle 
        elif test_value >test_set[middle]:
            start=middle 
        elif test_value ==test_set[middle]:
            return True,time 
        elif abs(start-end)==1:
        
            return False,time
        else:
            print("算法出现BUG！")
    return False,50
    
from random import randint
def rand():
    '''产生随机数
    '''
    return randint(20,1000000)
    
def test_half_find():
    '''折半查找的测试函数
    '''
    test_num=11000
    
    test_set=list( range(40000000) )
    for one in range(test_num):
        test_value=rand()
        have,index=half_find(test_set,test_value)
        if have:
            #print(one,"找到了！",test_value,index)
            pass
        else:
            print(one,"没找到！",test_value,index)
    print('测试完成！')
    
test_half_find()
input('回车结束：')
```
### 二维卷积网络
```python3
def build3():
	# 类resnet  78
	inputshape= (40,100,1)
	active='relu'
	start=Input(shape=inputshape )
	x1=conv.Conv2D(filters=1,kernel_size=(2,1),padding='same',activation=active)(start)
	x1=conv.Conv2D(filters=1,kernel_size=(2,1),padding='same',activation=active)(x1)
	x1=conv.Conv2D(filters=1,kernel_size=(2,1),padding='same',activation=active)(x1)
	x1=conv.Conv2D(filters=1,kernel_size=(2,1),padding='same',activation=active)(x1)
	x1=conv.Conv2D(filters=1,kernel_size=(2,1),padding='same',activation=active)(x1)
	x1=average([start,x1])
	
	x2=conv.Conv2D(filters=1,kernel_size=(3,1),padding='same',activation=active)(x1)
	x2=conv.Conv2D(filters=1,kernel_size=(3,1),padding='same',activation=active)(x2)
	x2=conv.Conv2D(filters=1,kernel_size=(3,1),padding='same',activation=active)(x2)
	x2=average([x1,x2])
	
	x3=conv.Conv2D(filters=1,kernel_size=(1,2),padding='same',activation=active)(x2)
	x3=conv.Conv2D(filters=1,kernel_size=(1,2),padding='same',activation=active)(x3)
	x3=conv.Conv2D(filters=1,kernel_size=(1,2),padding='same',activation=active)(x3)
	x3=average([x2,x3])
	
	x4=conv.Conv2D(filters=1,kernel_size=(1,3),padding='same',activation=active)(x3)
	x4=conv.Conv2D(filters=1,kernel_size=(1,3),padding='same',activation=active)(x4)
	x4=conv.Conv2D(filters=1,kernel_size=(1,3),padding='same',activation=active)(x4)
	
	x5=Flatten()(x4)
	end=Dense(4,activation='softmax')(x5)
	model=Model(inputs=start,outputs=end)
	return model
```
### 全连接网络
```python3
'''
	全连接神经网络
		device_count, 告诉tf Session使用CPU数量上限
		
'''
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'
'''
#为了更加充分的利用cpu进行的优化设置:没用
import tensorflow as tf
import keras.backend.tensorflow_backend as KTF
KTF.set_session(tf.Session(config=tf.ConfigProto(device_count={'cpu':1},
	inter_op_parallelism_threads=1,
	intra_op_parallelism_threads=1 ) ) )
'''


from keras.models import Sequential
from keras.layers import Dense,Activation
from keras.layers import Dropout
from keras.layers import Flatten
from keras.utils import to_categorical
from keras.models import load_model
from pickle import load
import numpy as np
np.random.seed(1432)

pickle_file1='rtdata/tdd.bin'
model_file='rtdata/dense_modeltmp.h5'

def read_data(train=True):
	#从文件中顺序获得对象
	with open(pickle_file1,'rb') as data:
		input_train=load(data)
		output_train=load(data)
		input_test=load(data)
		output_test=load(data)
	#对象进行规格化处理
	size=len(input_train)
	input_train=input_train.reshape(size,30,100)
	size1=len(input_test)
	input_test=input_test.reshape(size1,30,100)
	#根据用途选择返回值
	if train is True:
		return input_train,output_train
	else:
		return input_test,output_test
		
		
#--------------------------------+++++++++++++++++++++++++----------------------------
#---------------------------+++++++++++++++++++++++++++------------------------------
def all_connect_net_train():
	''' 尝试用dence 进行分类
			本函数只负责训练模型
	'''
	input_train,output_train=read_data()
	
	model=Sequential()
	model.add(Dense(80,input_shape= (30,100) ))
	model.add(Activation("relu") )
	model.add(Dropout(0.4))  #随机失活20%
	model.add(Dense(60))
	model.add(Activation("relu") )
	model.add(Dropout(0.3))
	model.add(Dense(40))
	model.add(Activation("relu") )
	model.add(Dropout(0.2))
	model.add(Dense(20))
	model.add(Activation("relu") )
	model.add(Dropout(0.1) ) 
	model.add(Dense(10) )
	model.add(Activation("relu") )
	model.add(Dropout(0.1) )
	model.add(Flatten() )
	model.add(Dense(4) )
	model.add( Activation('softmax') )
	
	model.compile(loss='categorical_crossentropy',optimizer='sgd', metrics=['categorical_accuracy','pression','recall'])
	model.fit(input_train, output_train, epochs=100, batch_size=64,verbose=2)
	
	#打印模型概况
	model.summary()
	#保存权重参数:
	model.save(model_file)

#--------------------------------+++++++++++++++++++++++++----------------------------

def net_test():
	#从文件载入参数
	print('测试从文件中载入模型参数')
	input_test,output_test=read_data(False)
	model=load_model(model_file)
	score = model.evaluate(input_test, output_test, batch_size=32,verbose=2)
	print('dev损失：',score[0])
	print('dev准确率：',score[1]*100)
	print('dev召回率:',score[2]*100)
	
	F=2*score[1]*score[2]/(score[1]+score[2])
	print("F-score:",F*100)
#--------------------------------++++++++++++++++++++++
all_connect_net_train()
#net_test()
'''
	axis:辨析
		mean(axis=1),我们将得到按行计算的均值
		drop((name, axis=1),我们实际上删掉了一列
	recall_1:
	"`Tensor` objects are not iterable when eager execution is not "
			TypeError: `Tensor` objects are not iterable when eager execution is not enabled.
 			To iterate over this tensor use `tf.map_fn`
 	recall_2:
 	TypeError: object of type 'Tensor' has no len()
'''
```
### LSTM
```python3
#！ -*- utf8 -*-
'''
	本文档格式为 python3 脚本文档
		2018-4-3: 23:08
'''
import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

from keras.models import Sequential
from keras.layers import Dense,Activation
from keras.layers import Flatten
from keras.layers import Dropout
from keras.layers.recurrent  import LSTM
from keras.layers.recurrent  import GRU
from keras.layers.recurrent  import SimpleRNN
from keras.models import load_model
from pickle import load
import numpy as np
np.random.seed(1432)


pickle_file1='rtdata/tdd.bin'
model_file='rtdata/RNN_model.h5'

#--------------------------------+++++++++++++++++++++++++----------------------------
def read_data(train=True):
	with open(pickle_file1,'rb') as data:
		input_train=load(data)
		output_train=load(data)
		input_test=load(data)
		output_test=load(data)

	size=len(input_train)
	input_train=input_train.reshape(size,30,100)
	size1=len(input_test)
	input_test=input_test.reshape(size1,30,100)

	if train is True:
		return input_train,output_train
	else:
		return input_test,output_test

#--------------------------------+++++++++++++++++++++++++----------------------------

def rnn_train():
	#循环神经网络 使用
	input_train,output_train=read_data()
	model=Sequential()

	model.add(LSTM(60,input_shape=(30,100),return_sequences=True) )
	model.add(Activation("relu") )
	model.add(LSTM(40,return_sequences=True) )
	model.add(Activation("relu") )
	model.add(Dropout(0.3) )
	model.add(LSTM(30,return_sequences=True) )
	model.add(Activation("relu") )
	model.add(Dropout(0.2) )
	model.add(LSTM(20,return_sequences=True) )
	model.add(Activation("relu") )
	model.add(Dropout(0.2) )
	model.add(Flatten() )
	model.add(Dense(4))
	model.add(Activation("softmax") )
	model.compile(loss='categorical_crossentropy',optimizer='adam', metrics=['categorical_accuracy'])
	model.fit(input_train, output_train, epochs=100, batch_size=32,verbose=2)

	model.summary()  #打印模型概况
	model.save(model_file)

#--------------------------------+++++++++++++++++++++++++----------------------------

def rnn_dev():
	#开发集验证模型
	input_test,output_test=read_data(False)
	model = load_model(model_file)
	score = model.evaluate(input_test, output_test, batch_size=32,verbose=2)
	print('测试损失：',score[0])
	print('测试准确率：',score[1])
	print('evaluate RESULT:',score)

#--------------------------------+++++++++++++++++++++++++----------------------------
#rnn_train()
rnn_dev()

'''

Epoch 1/100
 - 22s - loss: 1.1390 - categorical_accuracy: 0.4903
Epoch 2/100
 - 19s - loss: 0.7597 - categorical_accuracy: 0.7029
Epoch 3/100
 - 19s - loss: 0.5335 - categorical_accuracy: 0.8114
Epoch 4/100
 - 19s - loss: 0.4474 - categorical_accuracy: 0.8463
Epoch 5/100
 - 19s - loss: 0.3873 - categorical_accuracy: 0.8743
Epoch 6/100
 - 19s - loss: 0.3473 - categorical_accuracy: 0.8825
Epoch 7/100
 - 19s - loss: 0.3050 - categorical_accuracy: 0.8947
Epoch 8/100
 - 19s - loss: 0.2847 - categorical_accuracy: 0.9050
Epoch 9/100
 - 19s - loss: 0.2577 - categorical_accuracy: 0.9165
Epoch 10/100
 - 19s - loss: 0.2324 - categorical_accuracy: 0.9248

		惊喜来的


 - 19s - loss: 0.0209 - categorical_accuracy: 0.9936
Epoch 90/100
 - 19s - loss: 0.0388 - categorical_accuracy: 0.9897
Epoch 91/100
 - 19s - loss: 0.0195 - categorical_accuracy: 0.9929
Epoch 92/100
 - 19s - loss: 0.0035 - categorical_accuracy: 0.9991
Epoch 93/100
 - 19s - loss: 0.0027 - categorical_accuracy: 0.9991
Epoch 94/100
 - 19s - loss: 0.0024 - categorical_accuracy: 0.9995
Epoch 95/100
 - 19s - loss: 0.0066 - categorical_accuracy: 0.9984
Epoch 96/100
 - 18s - loss: 0.0036 - categorical_accuracy: 0.9993
Epoch 97/100
 - 19s - loss: 0.0279 - categorical_accuracy: 0.9911
Epoch 98/100
 - 19s - loss: 0.0156 - categorical_accuracy: 0.9950
Epoch 99/100
 - 19s - loss: 0.0080 - categorical_accuracy: 0.9975
Epoch 100/100
 - 19s - loss: 0.0027 - categorical_accuracy: 0.9989
'''
```
###  召回率定义
```python3
def categorical_accuracy(y_true, y_pred):
    return K.cast(K.equal(K.argmax(y_true, axis=-1),
                          K.argmax(y_pred, axis=-1)),
                  K.floatx())*100



def recall(y_true,y_pred):
    # version @3   asen add @ 2018-4-5
    T=K.round( K.clip(y_true,0,1) )  #0|1 化
    P=K.round( K.clip(y_pred,0,1))
    equal_sum=K.sum(T*P)  #筛选出真实值和预测值相同的项
    true_sum=K.sum(T )
    R=equal_sum/true_sum
    return R*100
def pression(y_true,y_pred) :
    # this  function is same as categorical_accuracy
    T=K.round( K.clip(y_true,0,1) )
    P=K.round( K.clip(y_pred,0,1))
    equal_sum=K.sum(T*P)
    pred_sum=K.sum(P)
    return (equal_sum/pred_sum)*100
    # if return value 's type is tensor  just use K.mean() to wrapped
    # if return value is a float : do nothing
#K中的类型为 tensor 无法运用python中的迭代
#,categorical_accuracy为总的正确率[keras自带].pression为分类正确率的均值

# @ 2 verion
def pression(y_true,y_pred) :
    # this  function is not same as categorical_accuracy
    T=K.round( K.clip(y_true,0,1) )
    P=K.round( K.clip(y_pred,0,1))
    equal_sum=K.sum(T*P,axis=1)
    pred_sum=K.sum(P,axis=1)
    return K.mean(equal_sum/pred_sum)*100
    #输出向量可能不是one-hot表示
    # if return value 's type is tensor  just use K.mean() to wrapped
    # if return value is a float : do nothing

def pression2(y_true,y_pred):
    #same to categorical_accuracy
    #山重水复疑无路,得来全不费功夫
    T=K.round( K.clip(y_true,0,1) )
    P=K.round( K.clip(y_pred,0,1))
    p2=K.sum(T*P,axis=0) # 按行加和,按列求均值
    p3=K.round(K.clip(p2,0,1))
    return p3*100

# @ 3 version
def pression(y_true,y_pred) :
    # this  function is not  same as categorical_accuracy
    T=K.round( K.clip(y_true,0,1) )
    P=K.round( K.clip(y_pred,0,1))
    equal_sum=K.sum(K.sum(T*P,axis=1) )  #列加和+行加和
    pred_sum=K.sum(K.sum(P,axis=1) )
    return (equal_sum/pred_sum)*100
    # if return value 's type is tensor  just use K.mean() to wrapped
    # if return value is a float : do nothing

def pression2(y_true,y_pred):
    #same to categorical_accuracy ,but by different way
    #山重水复疑无路,得来全不费功夫
    T=K.round( K.clip(y_true,0,1) )
    P=K.round( K.clip(y_pred,0,1))
    p2=K.sum(T*P,axis=0) # 按行加和,按列求均值
    p3=K.round(K.clip(p2,0,1))    #输出向量可能不是one-hot表示:正确率偏高
    return p3*100
```
### 