# java代码
```java
package java1;
//java1为测试类
import java.util.Date;
import java.util.Random;
import java.util.Scanner;
import static java1.Java1.asd.*;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
class Java1 {
    int i;
    int a[]=new int[20];
    int []b=new int[20];
    static String d="just now i know the world!";
    public static void main(String[] args) throws InterruptedException
    {
        mathtext asd = new mathtext();
        //多线程
        Thread myth;
        myth = new Thread()
        {
            @Override
            public void run()
            {
                System.out.println("fitst thread!");
            }
        };
        myth.start();
        Thread.sleep(1);
        System.out.println("my second thread!");
        myth.join();
        /*//串复制函数调用
        int a_1[]= {1,2,3,4};
        int b_1[]={4,5,6,7,8,4,0,98,7,7,6,5};
        System.arraycopy(a_1,0,b_1,0,a_1.length);
        for(int i=0;i<10;i++)
        {
            System.out.println(b_1[i]);
        }*/
        /*
        System.out.println("this is my first netbean program.");
        System.out.println(d);
        */
        /*///////////////////////////蒙特卡罗计算pai
        int n=10000000;
        int m=0;
        double x,y;
        for(int i=0;i<n;i++)
        {
            x=Math.random();
            y=Math.random();
            if(x*x+y*y<=1)
                m++;
        }
        System.out.println("pai="+(double)m/n*4);*///double 作用域
        /*////////////////////////count int type's max.
        //byte short int long 1,2,4,8;
        int max=java.lang.Integer.MAX_VALUE;
        int min=java.lang.Integer.MIN_VALUE;
        System.out.print("int max is"+max+"min is"+min+"\n");
        double fax=java.lang.Float.MAX_VALUE;
        double dmin=java.lang.Double.MAX_VALUE;
        System.out.print("fax"+fax+"max double"+dmin+"\n");
        *//////在代码中插入内容insert key  ///输入串
        /*
        Scanner flowin=new Scanner(System.in);
        String c=flowin.next();
        System.out.println(c.toUpperCase());*/
        /*闰年 判断 异常处理。
        Scanner scan=new Scanner(System.in);
        long year;
        try
        {
            year=scan.nextLong();
            if(year%4==0&&year%100!=0||year%400==0)
            {
                System.out.println(year+"是闰年。");
            }
            else
            {
                System.out.println(year+"不是闰年。");
            }
        }
        catch(Exception e)
        {
            System.out.println("不是有效年份。");
        }
        */
        /*
        Scanner in;
        in =new Scanner(System.in);
        double wendu=in.nextDouble();
        System.out.println("华氏温度："+(wendu*1.8+32));*/
        /*
        System.out.println(Math.tan(Math.PI/4));
        Random free=new Random();
        System.out.println(free.nextInt(9));//0-9
        */
        //math.random可多线程使用。util。random42为的种子，同种同法则同序列。
        /* get the date
        Date date=new Date();
        System.out.print(date.getTime()+"ms\n");
        System.out.println(date.toString());
        System.out.println("账户"+System.getProperty("user.name"));
        System.out.println("当前目录"+System.getProperty("user.dir"));
        System.out.println("home 路径"+System.getProperty("user.home"));
        System.out.println("osname"+System.getProperty("os.name"));
        System.out.println("os version"+System.getProperty("os.version"));
        System.out.println("os架构"+System.getProperty("os.arch"));
        System.out.println("vm's version"+System.getProperty("java.vm.version"));
        System.out.println("vm's 生产商"+System.getProperty("java.vm.vendor"));
        System.out.println("临时文件默认路径"+System.getProperty("java.io.tmpdir"));
        System.out.println("运行环境规范的名称"+System.getProperty("java.specification.name"));
        System.out.println("运行环境的版本"+System.getProperty("java.version"));
        System.out.println("环境生产商"+System.getProperty("java.vendor"));
        System.out.println("安装目录"+System.getProperty("java.home"));
        System.out.println("编译器名称"+System.getProperty("java.compiler"));
        int i=1024*1024;
        Runtime memary;
        memary=Runtime.getRuntime();
        System.out.println("虚拟机全部内存为："+memary.totalMemory()/i+"M");
        System.out.println("可用内存为"+memary.freeMemory()/i+"M");
        memary.gc();//垃圾收集线程；
        System.out.println("可用内存为"+memary.freeMemory()/i+"M");
        /*枚举类型的使用 安类声明，初始化，使用。
        asd a = mon;
        switch(a)
        {
            case today: System.out.println("today!");break;
            case mon: System.out.println("mon!");break;
            case yesterday:break;
            default://注意单词拼写default   &   defult
                System.out.println("null");
               break;
        }
        */
        //new jpr();//无法从静态上下文引用非静态变量this。??????????????
        //new bingxing();//求解素数
    }
    enum asd 
    {
        yesterday,today,mon,tomorrow;
    }
    public class jpr extends JFrame
    {
        JProgressBar progress=new JProgressBar();
        int c=0;
        class task extends java.util.TimerTask
        {
            @Override
            public void run()
            {
                progress.setValue(c++);
            }
        }
        public jpr()
        {
            progress.setStringPainted(true);
            this.getContentPane().add(progress,"North");
            task ask=new task();
            java.util.Timer time=new java.util.Timer();
            time.schedule(ask,100,100);
            this.setSize(500,100);
            this.setVisible(true);
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }
    }
    
}
```
## numpy使用
```python3
# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a script file.using numpy

    2017-5-22 night qs 
"""
import numpy as np
import matplotlib.pyplot as plt

firstone=np.mat('1 2 3;4 5 6;7 8 9')
secondone=np.arange(20)*7      
thirdone=firstone*3

#显示图形
fun=np.poly1d(np.array([1,2,3,4]).astype(float))
x=np.linspace(-10,10,30)
y=fun(x)
plt.plot(x,y)
plt.xlabel('x')
plt.ylabel('y(x)')
plt.show()

#命令行输出
print('转置矩阵：\n',firstone.T)
print('逆矩阵：\n',firstone.I)
print('原矩阵:\n',firstone)
print('secondone:',secondone) 
print('除法：\n',np.divide(thirdone,firstone))
print('模运算：',secondone[1:10]%4,secondone[1:10])
```
## utf-8 编码查看
```python3
'''
    数值所对应的utf-8编码的字符
                琼生
                17-4-17
'''
def view_utf8(max_num,min_num):
    ''' 传入的参数是待输出函数的上下限'''
    outfile=open("int_utf8.txt",'w',encoding='utf-8')
    text='123'
    count_num=min_num
    while count_num<max_num:
        text='0x'+str(format(count_num,'x'))+'\t'+str(count_num)+'\t'+str(format(count_num,'b'))+'\t'+chr(count_num)+'\t\t'
        outfile.writelines(text)
        count_num=count_num+1
        if count_num%10==0:
            outfile.write('\n')
def default():
    max_num=54321
    min_num=0
    view_utf8(max_num,min_num)
def yourchoice():
    firstnum=input('输入最大值: ')
    lastnum=input('输入最小值: ')
    max_num=int(str(firstnum),10)
    min_num=int(str(lastnum),10)
    view_utf8(max_num,min_num)
def main():
    choice=int(input('0:默认，1：手动确定范围：'))
    if choice==0:
        default()
    elif choice==1:
        yourchoice()
    else:
            print('there is no choice of your\'s.')
if __name__=='__main__':
    main()
    
#汉字范围：u4e00~u9fa5 (19968~40869)  转换方式：int('9fa5',16)
```
### 数据生成的初步尝试
```python3
'''17-5-2 吃午饭前 qs
        伪造数据库文件
'''
import os
def get_message():
    #获取基本信息
    large_size=int(input('请输入伪造数据项的条数：'))
    forge_file_name=input('请输入文件名')
    file_path=os.getcwd()+'\\'+forge_file_name
    return large_size,forge_file_name,file_path
import random
# ***       ** *            *  * * * **  **  * * *     * * ** * * *1
def generate_number():
    #生成学号
    number=random.randrange(9999)
    return 20140000+number
# ***       ** *            *  * * * **  **  * * *     * * ** * * *2
def ganerete_name():
    name=''
    #@1 ：完全随机数 （假的明显）生成姓名
    count=0
    while count<=2:
        name+=chr(random.randrange(30000,40000))
        count+=1
    #@2：姓氏枚举（百家姓） 名字：随机选字（常用字）
        xingshi=['赵','钱','孙','李','周']
    return name
# ***       ** *            *  * * * **  **  * * *     * * ** * * *3
def genarate_sex():
    #生成性别
    return random.choice('男女')
def generate_old():
    #生成年龄
    return random.randrange(18,26)
def generate_fenshu():
    #伪造各科成绩
    return random.randrange(36,100)
# ***       ** *            *  * * * **  **  * * *     * * ** * * *4
def generate_address():
    #家庭住址
    #   生成0-30的随机数 
    shengji=['青海','宁夏','海南','香港','澳门','台湾','河北','山东','山西','黑龙江','吉林','辽宁','天津','河南','陕西','甘肃','新疆','西藏','四川','云南','贵州','重庆','湖北','湖南','广西','广东','福建','浙江','上海','北京','安徽','江苏','内蒙古']
    #对应的每个市的地级行政单位
    #shiji=[['西宁']]
    # @1枚举+随机数（覆盖率高）
    cunji=[]#2000个左右村级行政单位 （要不要用枚举）
    #@2 在不完整的数据库中 用随机数选取

    #@3完全虚假的 随机命名 村名（常用字组合，概率组合）
def student_info(data_term):
    #伪造学生信息表
    data_term.append(generate_number())
    data_term.append(ganerete_name())
    data_term.append(genarate_sex())
    data_term.append(generate_old())
    return data_term
def student_score(data_term):
    #学生成绩表
    data_term.append(generate_number())#既是主键又是外键
    data_term.append(generate_fenshu())
    data_term.append(generate_fenshu())
    data_term.append(generate_fenshu())
    data_term.append(generate_fenshu())
    data_term.append(generate_fenshu())
    return data_term
# ***       ** *            *  * * * **  **  * * *     * * ** * * *5
def list_to_string(list_table):
    #列表转化为字符串（用来改变写入文件的形式）
    str1=''
    for iter in list_table:
        str1=str1+str(iter)+','
    return str1
#***       ** *            *  * * * **  **  * * *     * * ** * * *6
def  forge_data():
    #生成格式化的数据库文件 forge：伪造
    large_size,forge_file_name,file_path=get_message()
    with open(file_path,'w+',encoding='utf-8') as file_to_write:
        count=0
        while count<large_size:
            data_term=[] #存储一条待写入的数据项（记录）放在循环内否则会累加
            student_info(data_term)
            #student_score(data_term)
            file_to_write.write(list_to_string(data_term)+'\n')
            count+=1
# ***       ** *            *  * * * **  **  * * *     * * ** * * *7
if  __name__=='__main__':
    forge_data()
```
### 模式识别课程代码
```matlab
%特征提取
path(path,'D:\attface\s1');
feature=zeros(20,6);
for i=1:10
    aimage=imread([num2str(i),'.pgm']);
    T=wenli(aimage);
    feature(i,:)=T;
end

path(path,'D:\attface\s2');
%feature1=zeros(10);
for i=1:10
    aimage1=imread([num2str(i),'.pgm']);
    T1=wenli(aimage1);
    feature(i+10,:)=T1;
end


%分类器2：最近邻分类
feature_distance=zeros(1,20);

result=zeros(1,20);
min_val=zeros(1,20);
for i=1:20
    test_set=feature(i,:);
    for j=1:20
        if i~=j
            feature_distance(j)=sum(abs(feature(j,:)-test_set))/6; 
        else
            feature_distance(j)=+inf;
        end
    end
    min_val(i)=min(feature_distance);
    x=find(feature_distance==min_val(i));
    if i<=10
        if x<=10
            result(i)=1;
        else
            result(i)=0;
        end
    else
        if x<=10
            result(i)=0;
        else
            result(i)=1;
        end
    end
end
right_present=sum(result)/20;

%{
%分类器1
mean=[];
mean1=[];
for i=1:6
    mean(i)=sum(feature(:,i))/9;
    mean1(i)=sum(feature1(:,i))/9;
end
result=[];
test_set=test_set2;
result(1)=sum(test_set-mean)/6;
result(2)=sum(test_set-mean1)/6;
if abs(result(1))<abs(result(2))
    oneclass='是第一类图片的人。';
else
    oneclass='是第二类图片的人';
end
%}



%save feature.txt feature
aimage=imread('1.pgm');
%bimage=rgb2gray(aimage);
%imshow(bimage);
%imshow('1.pgm');
subplot(1,3,1);
imshow(aimage);

subplot(1,3,2);
cimage=imhist(aimage);
imshow(cimage);
axis square;

subplot(1,3,3);
%j=edge(aimage,'sobel');
%j=edge(aimage,'log');
j=edge(aimage,'canny');
%j=edge(aimage,'prewitt');
%j=edge(aimage,'roberts');
imshow(j);

```
### 数据库基本操作
```julia
create table asd.txt num int name char
insert table asd.txt 20143955 阿斯顿 男 2018-5-4
insert table asd.txt 20144389 郝雨歌 女 2000-6-6
insert table asd.txt 20144568 张秋生 男 2048-6-8
insert table asd.txt 20155566 赵丽 女 2361-8-8
delete table asd.txt 20143955
update table asd.txt 20143955 现金晚
select * from asd.txt
	
select s_name,s_age from asd.txt where s_age<21
select s_number,s_name,s_age from asd.txt,score.txt where s_age=18
	
select s_number,math from asd.txt,score.txt where math>90
	
select name,age,score from asd.txt,myscore.txt where score>60 and age<21
	
create table qwe 
create index qwe456
	
add user qs 775966432
open asd use qs qw123214
open asd use qs 775966432
	
drop table qwe
drop index qwe465
	
drop table asd.txt
	
create database zxc
drop database zxc
	
alter table asd.txt add age int computer char tall int
alter table asd.txt drop age 
```
### 表串转换
```python3
'''串与list间的转换。
'''
def list_to_string(list_table):
    #
    str1=''
    for iter in list_table:
        for itr1 in iter:
            str1=str1+str(itr1)+','
    print(str1)
    return str1
def string_to_list(str_table):
    str_list=str_table.split(',')
    print(len(str_list))
    i=0
    asd=[]
    while i< len(str_list)/3:
       asd.append(str_list[:3])
       str_list=str_list[3:]
       i+1  #为什么i=i+1 不对？？
    asd.pop()
    print(asd)
    return asd
    #
temp=[['n1',2,'x'],['n2',1,'c'],['n3',3,'f'],['n4',3,'g']]
str=list_to_string(temp)
lit=string_to_list(str)

'''
str=''.join(list)  引号内为分隔符

list=list(str)
'''
```